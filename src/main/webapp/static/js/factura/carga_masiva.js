$(document).ready(function () {
    $("#fecha").datepicker({language: 'es'});
    $("#etd").datepicker({language: 'es'});
    $("#eta").datepicker({language: 'es'});

    eventos();

    function eventos() {
        $("#cargar").click(function () {
            if (validaCargaArchivo())
            {
                ejecutaCargaArchivo();
            }
        });
    }

    function validaCargaArchivo() {
        var componenteArchivoDeCarga = $("#archivo");
        var msmErrorNulo = "Favor de seleccionar el archivo de carga masiva.";
        var msmErrorFormato = "El formato de archivo aceptado es: <strong>.xlsx</strong>";

        if ($("#numeroOrden").val() == "")
        {
            mensajeError("Favor de ingresar el order");
            return false;
        }

        if (componenteArchivoDeCarga.val() == "") {
            mensajeError(msmErrorNulo);

            return false;
        } else if (componenteArchivoDeCarga.val().lastIndexOf(".xlsx") <= 0) {
            mensajeError(msmErrorFormato);
            return false;
        }

        return true;
    }

    function ejecutaCargaArchivo() {
        var formData = new FormData();
        formData.append('numeroOrden', $("#numeroOrden").val());

        for (var i = 0, len = $("#archivo")[0].files.length; i < len; i++) {
            formData.append('archivo' + i, $("#archivo")[0].files[i]);
        }

        var vURL = "service/cargaArchivo/factura";
        enviarArchivo(vURL, formData);
    }

    function enviarArchivo(ruta, formData) {
        $("#loader").show();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', ruta, true);
        xhr.responseType = 'blob';

        xhr.onload = function (e) {
            $("#loader").hide();
//            if(this.status == 206){
//                mensajeError("Ocurrio un error al cargar la(s) factura(s)");
//            }
            if (this.status == 200) {
                var text = blobToString(this.response);
                cargaResultadosExitosos(JSON.parse(text));
//                mensajeExito("Se cargo el archivo correctamente");
            } else if (this.status == 203) {
                var text = blobToString(this.response);
                cargaResultadosFallidos(JSON.parse(text));
            } else if (this.status == 207) {
                mensajeError("El archivo contiene errores, favor de verificar");
            } else if (this.status == 206) {
                var text = blobToString(this.response);
                mensajeError("Error de archivo " + text);
            } else if (this.status == 409) {
                var text = blobToString(this.response);
                mensajeError("Error de archivo " + text);
            } else {
                var msmErrorFormato = "No se pudo realizar la carga del archivo, por favor intente más tarde.";
                mensajeError(msmErrorFormato);
            }
        };

        xhr.send(formData);

    }

    function descargaArchivoExcel(response)
    {
        var blob = new Blob([response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
        var downloadUrl = URL.createObjectURL(blob);
        var a = document.createElement("a");
        a.href = downloadUrl;
        a.download = $("#archivo").val().replace(/.*(\/|\\)/, '');
        document.body.appendChild(a);
        a.click();
    }

    function blobToString(b) {
        var u, x;
        u = URL.createObjectURL(b);
        x = new XMLHttpRequest();
        x.open('GET', u, false); // although sync, you're not fetching over internet
        x.send();
        URL.revokeObjectURL(u);
        return x.responseText;
    }

    function cargaResultadosExitosos(resultado) {
//        $("#resultadoCarga").html("Factura cargada correctamente").addClass("bg-success").removeClass("bg-danger");

        var cargaFacturaVOListError = new Array(resultado.cargaFacturaVOListError.values());
        var tamanioError = resultado.cargaFacturaVOListError.length;
        
        if (tamanioError > 0) {
            llenaResgistroError(resultado.cargaFacturaVOListError);
        } else {
            llenaRegistroExitoso(resultado.cargaFacturaVOListExitoso);
        }
        
    }


//    function llenaRegistroExitoso(lista) {
//
////        if(lista.length >=1){
////            alert("lista.length" + lista.length);
//            creaDetalleGuardados(lista);
////        }
//        $("#panelResultados").show();
//        mensajeExito("Se cargo el archivo correctamente");
//    }

    function llenaRegistroExitoso(lista) {
        console.log(lista);

        var listaExitosa = lista;
        var tamanio = listaExitosa.length;
        console.log(listaExitosa);
        if (tamanio >= 1) {
            $("#panelResultados").show();
            creaDetalleGuardados(lista);
//                $("#panelResultados").show();
        }
        mensajeExito("Se cargo el archivo correctamente");
    }

    function llenaResgistroError(lista) {
        if (lista.length >= 1) {
            creaDetalleError(lista);
            $("#modalError").modal();
        }
        mensajeExito("Ocurrio un error al cargar la(s) factura(s)");
    }

    function creaDetalleGuardados(lista) {
        var resHTML = "";
        for (var i = 0; i < lista.length; i++) {
            var registro = lista[i];
//            $("#orderRes").html(registro.numeroOrden);
            resHTML += llenaRegistro(registro.datos, registro);
            $("#orderRes").html(registro.numeroOrden);
        }
        $("#tbodyRes").html(resHTML);

        $("#datosRes").html("");

        $("#tablaFactura").DataTable();

//        $("#panelResultados").show();
//        mensajeExito("Se cargo el archivo correctamente");
    }

    function creaDetalleError(lista) {
        var resHTML = "";
        for (var i = 0; i < lista.length; i++) {
            var detalle = lista[i];
            resHTML += llenaErrores(detalle.datos, detalle);
        }
        $("#cuerpoTablaFactura").html(resHTML);
        $("#tablaErr").DataTable();
    }


//    function cargaResultadosFallidos(resultado) {
//        $("#resultadoCarga").html("Factura con errores ").addClass("bg-danger").removeClass("bg-success");
//
//        var fechaFacturaErrorTXT = "";
//        if (resultado.fechaFacturaErrorTXT != null)
//        {
//            fechaFacturaErrorTXT = resultado.fechaFacturaErrorTXT;
//        }
//        $("#dateRes").html(resultado.fechaFactura + " " + fechaFacturaErrorTXT);
//
//        var numeroOrdenErrorTXT = "";
//        if (resultado.numeroOrdenErrorTXT != null)
//        {
//            numeroOrdenErrorTXT = resultado.numeroOrdenErrorTXT;
//        }
//        $("#orderRes").html(resultado.numeroOrden + " " + numeroOrdenErrorTXT);
//
//        var numeroFacturaErrorTXT = "";
//        if (resultado.numeroFacturaErrorTXT != null)
//        {
//            numeroFacturaErrorTXT = resultado.numeroFacturaErrorTXT;
//        }
//        $("#invoiceRes").html(resultado.numeroFactura + " " + numeroFacturaErrorTXT);
//
//        var contenedorErrorTXT = "";
//        if (resultado.contenedorErrorTXT != null)
//        {
//            contenedorErrorTXT = resultado.contenedorErrorTXT;
//        }
//        $("#containerRes").html(resultado.contenedor + " " + contenedorErrorTXT);
//
//        var etdErrorTXT = "";
//        if (resultado.etdErrorTXT != null)
//        {
//            etdErrorTXT = resultado.etdErrorTXT;
//        }
//        $("#etdRes").html(resultado.etd + " " + etdErrorTXT);
//
//        var etaErrorTXT = "";
//        if (resultado.etaErrorTXT != null)
//        {
//            etaErrorTXT = resultado.etaErrorTXT;
//        }
//        $("#etaRes").html(resultado.eta + " " + etaErrorTXT);
//
//        if (resultado.errorListado)
//        {
//            $("#datosRes").html("El archivo no es válido o no contiene información");
//        } else {
//            var resHTML = "";
//            resHTML += llenaRegistroError(resultado.datos);
//
//            $("#tbodyRes").html(resHTML);
//        }
//
//
//
//
//        $("#panelResultados").show();
//    }



    function llenaRegistro(lista, registroGeneral)
    {
        var resHTML = "";
        for (var i = 0; i < lista.length; i++)
        {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.numeroFactura;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.fechaFactura;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.contenedor;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.etd;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.eta;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.sku;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.nombre;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.piezas;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.precio;
            resHTML += "</td>";

            resHTML += "</tr>";
        }

        return resHTML;
    }

    function llenaErrores(lista, registroGeneral) {
        var resHTML = "";
        for (var i = 0; i < 1; i++) {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.nombreArchivo;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.contenedor;
            resHTML += "</td>";
            resHTML += "</tr>";
        }

        return resHTML;
    }


    function llenaRegistroError(lista, tipo)
    {
        var resHTML = "";

        for (var i = 0; i < lista.length; i++)
        {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";


            resHTML += "<td>";
            resHTML += registro.sku;
            resHTML += "</td>";


            resHTML += "<td>";
            resHTML += registro.nombre;
            resHTML += "</td>";

            if (registro.piezasError)
            {
                resHTML += "<td class='bg-danger'>";
                resHTML += registro.piezas + " " + registro.piezasErrorTxt;
                resHTML += "</td>";
            } else {
                resHTML += "<td>";
                resHTML += registro.piezas;
                resHTML += "</td>";
            }

            if (registro.precioError)
            {
                resHTML += "<td class='bg-danger'>";
                resHTML += registro.precio + " " + registro.precioErrorTxt;
                resHTML += "</td>";
            } else {
                resHTML += "<td>";
                resHTML += registro.precio;
                resHTML += "</td>";
            }

            resHTML += "</tr>";
        }

        return resHTML;
    }
});
