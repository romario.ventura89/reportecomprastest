var reporte = "";
var tablaReporte = "";
$(document).ready(function () {

    var cuentaOrdenes = 0;

    $("#fechaInicio").datepicker();
    $("#fechaFin").datepicker();
    eventos();

    function eventos() {
        $("#limpiar").click(function () {
            limpiar();
            $("#botonDescarga").hide();
        });

        $("#buscar").click(function () {
            obtenerReporte();
        });

        $("#descargarReporte").click(function () {
            descargarReporte();
        });
    }


    function obtenerReporte() {
        var path = "service/pedidos/buscar";
        var datos = {
            fechaInicio: $("#fechaInicio").val(),
            fechaFin: $("#fechaFin").val()
        };
        conectaPost(path, datos, function (data) {
            reporte = data;
            llenaReporte();
            $("#botonDescarga").show();
        });
    }

    function limpiar() {
        $("#fechaInicio").val("");
        $("#fechaFin").val("");
        $("#resultado").html("");
    }

    function llenaReporte() {
        var tablaHTML = "";
        tablaHTML += llenaTitulos();
        $("#resultado").html(tablaHTML);
        var cuerpoHTML = "";
        cuerpoHTML += llenaValores();

        $("#cuerpoTabla").html(cuerpoHTML);
        var tamanioColumnas = new Array();
        tamanioColumnas.push({"width": "80px", "targets": 0});
        tamanioColumnas.push({"width": "150px", "targets": 1});



        for (var i = 1; i <= cuentaOrdenes; i++)
        {
            if (cuentaOrdenes <= 5) {
                if (cuentaOrdenes == 1)
                {
                    tamanioColumnas.push({"width": "100%", "targets": i + 1});
                }

                if (cuentaOrdenes == 2)
                {
                    tamanioColumnas.push({"width": "250px", "targets": i + 1});
                }

                if (cuentaOrdenes == 3)
                {
                    tamanioColumnas.push({"width": "175px", "targets": i + 1});
                }

                if (cuentaOrdenes == 4)
                {
                    tamanioColumnas.push({"width": "120px", "targets": i + 1});
                }

                if (cuentaOrdenes == 5)
                {
                    tamanioColumnas.push({"width": "100px", "targets": i + 1});
                }

            } else {
                tamanioColumnas.push({"width": "80px", "targets": i + 1});
            }
        }

        tamanioColumnas.push({"width": "100px", "targets": cuentaOrdenes + 2});
        tamanioColumnas.push({"width": "80px", "targets": cuentaOrdenes + 3});

        var filtros = new Array();


        tablaReporte = crearDatatablePedidos("#reporte", filtros, tamanioColumnas);



    }

    function llenaTitulos() {
        cuentaFacturas = 0;
        cuentaOrdenes = 0;
        var resHTML = "";
        resHTML += "<table id='reporte' class='stripe row-border order-column'><thead>";
        resHTML += "<tr><th style='background:white'>SKU</th>";
        resHTML += "<th style='background:white'>PRODUCT NAME</th>";

        for (var i = 0; i < reporte.ordenes.length; i++)
        {
            var orden = reporte.ordenes[i];
            resHTML += "<th class='text-center'>" + orden.noOrden + "</th>";
            cuentaOrdenes++;
        }

        resHTML += "<th style='background:white'>ORDER MISSING PIECES</th>";
        resHTML += "<th style='background:white'>PIECES CONFIRMED</th>";
        resHTML += "<th style='background:white'>TOTAL INVOICE</th>";
        resHTML += "<th style='background:white'>DIFFERENCE</th>";
        resHTML += "</tr></thead>";
        resHTML += "<tbody id='cuerpoTabla'></tbody></table>";
        return resHTML;
    }

    function llenaValores() {
        var resHTML = "";
        for (var i = 0; i < reporte.pedidos.length; i++)
        {
            var registro = reporte.pedidos[i];

            resHTML += "<tr>";
            resHTML += "<td>" + registro.sku + "</td>";
            resHTML += "<td>" + registro.productName + "</td>";

            for (var j = 0; j < registro.pedidosOrdenes.length; j++)
            {
                var pedidoOrden = registro.pedidosOrdenes[j];
                resHTML += "<td class='text-center'>" + pedidoOrden.piezas + "</td>";
            }



            if (registro.faltantes.length == 0)
            {
                resHTML += "<td class='bg-success text-center'>";
                resHTML += "-";
            } else
            {
                resHTML += "<td class='text-left'>";

                for (var j = 0; j < registro.faltantes.length; j++)
                {
                    var faltante = registro.faltantes[j];
                    var clase = "label-danger";

                    resHTML += '<span class="label ' + clase + '">' + faltante.noOrden + ': ' + faltante.faltantes + '</span>';
                    resHTML += "<br />";
                }
            }

            resHTML += "</td>";

            resHTML += "<td class='text-right'>" + registro.piezasConfirmadas + "</td>";
            resHTML += "<td class='text-right'>" + registro.totalFacturas + "</td>";

            var registrosNoEnviados = registro.noEnviados.replace(",", "");
            registrosNoEnviados = registrosNoEnviados * 1;

            if (registrosNoEnviados < 0) {
                resHTML += "<td class='bg-danger text-right'>";
            } else if (registrosNoEnviados == 0) {
                resHTML += "<td class='bg-success text-right'>";
            } else if (registrosNoEnviados > 0) {
                resHTML += "<td class='bg-info text-right'>";
            } else {
                resHTML += "<td class='text-right'>";
            }


            resHTML += registro.noEnviados + "</td>";
            resHTML += "</tr>";
        }
        return resHTML;
    }

    function descargarReporte()
    {
        leerArchivo("service/pedidos/excel", "REPORTE_CONFIRMACION_DETALLE.xlsx");
    }

});

