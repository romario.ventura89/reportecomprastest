$(document).ready(function ()
{
    eventos();
    function eventos() {
        $("#inicio").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=1");
            loadPage("#menu", "service/navegacion/seleccionaMenu?id=3");
        });

        $("#salir").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=2");
            loadPage("#menu", "service/navegacion/seleccionaMenu?idMenu=0");
        });

    }
});
