$(document).ready(function () {
    $("#fechaConfirmacion").datepicker({language: 'es'});
    eventos();

    function eventos() {
        $("#cargar").click(function () {
            if (validaCargaArchivo())
            {
                ejecutaCargaArchivo(false);
            }
        });

        $("#modalSubir").click(function () {
            if (validaCargaArchivo())
            {
                ejecutaCargaArchivo(true);
            }
        })
    }

    function validaCargaArchivo() {
        var componenteArchivoDeCarga = $("#archivo");
        var msmErrorNulo = "Favor de seleccionar el archivo de carga masiva.";
        var msmErrorFormato = "El formato de archivo aceptado es: <strong>.xlsx</strong>";
        if ($("#fechaConfirmacion").val() == "")
        {
            mensajeError("Favor de seleccionar la fecha de confirmación.");
            return false;
        }

        if (componenteArchivoDeCarga.val() == "") {
            mensajeError(msmErrorNulo);

            return false;
        } else if (componenteArchivoDeCarga.val().lastIndexOf(".xlsx") <= 0) {
            mensajeError(msmErrorFormato);
            return false;
        }

        return true;
    }

    function ejecutaCargaArchivo(remplazar) {

        var formData = new FormData();
        formData.append('fechaConfirmacion', $("#fechaConfirmacion").val());
        formData.append('remplazar', remplazar);

        for (var i = 0, len = $("#archivo")[0].files.length; i < len; i++) {
            formData.append('archivo' + i, $("#archivo")[0].files[i]);
        }

        var vURL = "service/cargaArchivo/confirmacion";
        enviarArchivo(vURL, formData);
    }

    function enviarArchivo(ruta, formData) {
        $("#loader").show();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', ruta, true);
        xhr.responseType = 'blob';

        xhr.onload = function (e) {
            $("#loader").hide();
            if (this.status == 206) {
                mensajeError("La confirmacion fue cargada anteriormente");
            }
            if (this.status == 200) {
                var text = blobToString(this.response);
                cargaResultadosExitosos(JSON.parse(text));
                mensajeExito("Se cargo el archivo correctamente");
            } else if (this.status == 206) {
                var text = blobToString(this.response);
                cargaResultadosExitosos(JSON.parse(text));
                mensajeExito("La confirmacion fue cargada anteriormente");
            } else if (this.status == 203) {
                var text = blobToString(this.response);
                cargaResultadosFallidos(JSON.parse(text));
            } else if (this.status == 205) {
                var text = blobToString(this.response);
            } else if (this.status == 207) {
                mensajeError("El archivo contiene errores, favor de verificar");
            } else if (this.status == 206) {
                var text = blobToString(this.response);
                mensajeError("Error de archivo " + text);
            } else {
                var msmErrorFormato = "No se pudo realizar la carga del archivo, por favor intente más tarde.";
                mensajeError(msmErrorFormato);
            }


        };

        xhr.send(formData);
    }

    function descargaArchivoExcel(response)
    {
        var blob = new Blob([response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
        var downloadUrl = URL.createObjectURL(blob);
        var a = document.createElement("a");
        a.href = downloadUrl;
        a.download = $("#archivo").val().replace(/.*(\/|\\)/, '');
        document.body.appendChild(a);
        a.click();
    }

    function blobToString(b) {
        var u, x;
        u = URL.createObjectURL(b);
        x = new XMLHttpRequest();
        x.open('GET', u, false); // although sync, you're not fetching over internet
        x.send();
        URL.revokeObjectURL(u);
        return x.responseText;
    }

    function cargaResultadosExitosos(resultado) {
        $("#resultadoCarga").html("Confirmación cargada correctamente").addClass("bg-success").removeClass("bg-danger");

//        llenaRegistroExitoso(resultado.cargaConfirmacionVOListExitoso);
//        llenaResgistroError(resultado.cargaConfirmacionVOListError);
        var cargaConfVOListError = new Array(resultado.cargaConfirmacionVOListError.values());
        var tamanioError = resultado.cargaConfirmacionVOListError.length;
        
        if (tamanioError > 0) {
            llenaResgistroError(resultado.cargaConfirmacionVOListError);
        } else {
            llenaRegistroExitoso(resultado.cargaConfirmacionVOListExitoso);
        }

        $("#panelResultados").show();
    }

    function cargaResultadosFallidos(resultado) {
        $("#resultadoCarga").html("Confirmación con errores ").addClass("bg-danger").removeClass("bg-success");

        var textoErrorOrden = "";
        if (resultado.numeroOrdenErrorTxt != null)
        {
            textoErrorOrden = resultado.numeroOrdenErrorTxt;
        }

        $("#orderRes").html(resultado.numeroOrden + " " + textoErrorOrden);

        var textoErrorFecha = "";
        if (resultado.fechaConfirmacionErrorTxt != null)
        {
            textoErrorFecha = resultado.fechaConfirmacionErrorTxt;
        }
        $("#fecRes").html(resultado.fechaConfirmacion + " " + textoErrorFecha);

        if (resultado.errorListado)
        {
            $("#datosRes").html("El archivo no es válido o no contiene información");
        } else {
            var resHTML = "";
            resHTML += llenaRegistroError(resultado.general, "General");
            resHTML += llenaRegistroError(resultado.qianhai, "Qianhai");
            resHTML += llenaRegistroError(resultado.pingshan, "Pingshan");

            $("#tbodyRes").html(resHTML);
        }
        $("#panelResultados").show();
    }

    function llenaRegistroExitoso(lista) {
        var resHTML = "";

        for (var i = 0; i < lista.length; i++){
            var registro = lista[i];
            
            const map = new Map(Object.entries(registro.cargaInformacion));
            
            $("#fecRes").html(registro.fechaConfirmacion);
            var orden = registro.numeroOrden;

            for (const [key, value] of map.entries()) {
                resHTML += llenaRegistro(value, key, orden);
            }
        }
        $("#tbodyRes").html(resHTML);
        $("#datosRes").html("");

        $("#tablaConfirmacion").DataTable();
    }

    function llenaResgistroErrores(lista) {
        var resHTML = "";
        for (var i = 0; i < lista.length; i++)
        {
            var registro = lista[i];
            $("#fecResError").html(registro.fechaConfirmacion);
            var orden = registro.numeroOrden;
            resHTML += llenaRegistro(registro.general, "General", orden);
            resHTML += llenaRegistro(registro.qianhai, "Qianhai", orden);
            resHTML += llenaRegistro(registro.pingshan, "Pingshan", orden);
        }
        $("#tbodyResError").html(resHTML);
        $("#datosResError").html("");

        $("#tablaConfirmacionError").DataTable();

    }
    
    function llenaResgistroError(lista) {
        if (lista.length >= 1) {
            creaDetalleError(lista);
            $("#modalError").modal();
        }
        mensajeExito("Ocurrio un error al cargar la(s) confirmacion(es)");
    }
    
    function creaDetalleError(lista) {
        var resHTML = "";
        for (var i = 0; i < lista.length; i++) {
            var detalle = lista[i];
            resHTML += llenaErrores(detalle.datos, detalle);
        }
        $("#cuerpoTablaFactura").html(resHTML);
        $("#tablaErr").DataTable();
    }
    
     function llenaErrores(lista, registroGeneral) {
        var resHTML = "";
        for (var i = 0; i < 1; i++) {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.numeroOrden;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registroGeneral.numeroOrden;
            resHTML += "</td>";
            resHTML += "</tr>";
        }

        return resHTML;
    }

    function llenaRegistro(lista, tipo, orden)
    {
        var resHTML = "";
        for (var i = 0; i < lista.length; i++)
        {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += orden;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += tipo;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.sku;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.nombre;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.precio;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.cbm;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.piezasSolicitadas;
            resHTML += "</td>";

            resHTML += "<td>";
            if (registro.notOrder) {
                resHTML += "Not Order";
            } else {
                resHTML += registro.piezasEntregadas;
            }
            resHTML += "</td>";

            resHTML += "</tr>";
        }

        return resHTML;
    }

    function llenaRegistroError(lista, tipo)
    {
        var resHTML = "";

        for (var i = 0; i < lista.length; i++)
        {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";


            resHTML += "<td>";
            resHTML += tipo;
            resHTML += "</td>";

            if (registro.skuError)
            {
                resHTML += "<td class='bg-danger'>";
                resHTML += registro.sku + " " + registro.skuErrorTXT;
                resHTML += "</td>";
            } else {
                resHTML += "<td>";
                resHTML += registro.sku;
                resHTML += "</td>";
            }


            resHTML += "<td>";
            resHTML += registro.nombre;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.precio;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.piezasSolicitadas;
            resHTML += "</td>";

            resHTML += "<td>";
            if (registro.notOrder)
            {
                resHTML += "Not Order";
            } else {
                resHTML += registro.piezasEntregadas;
            }
            resHTML += "</td>";

            resHTML += "</tr>";
        }

        return resHTML;
    }
});


