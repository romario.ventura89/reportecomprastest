var datosFactura = "";
var detalleProductos = "";
var facturasSel = "";
var tablaFacturas = "";

$(document).ready(function () {

    $("#fechaConfirmacion").datepicker({language: 'es'});
    $("#eta").datepicker({language: 'es'});
    $("#etd").datepicker({language: 'es'});
    eventos();

    function eventos() {

        $("#limpiar").click(function () {
            limpiar();
        });

        $("#buscar").click(function () {
            buscar();
        });

        $("#aceptarEliminar").click(function () {
            eliminaFactura(facturasSel);
        });

    }

    function buscar() {

        var path = "service/facturasColombia/buscar";
        var datos = {
            numeroOrden: $("#noOrden").val(),
            sku: $("#sku").val(),
            fecha: $("#fechaConfirmacion").val(),
            contenedor: $("#Container").val(),
            factura: $("#factura").val(),
            etd: $("#etd").val(),
            eta: $("#eta").val()
        };

        conectaPost(path, datos, function (data) {
            $("#cuerpoTabla").html("");
            datosFactura = data;
            $("#botonExcel").show();
            $("#panelConfirmacion").show();
            llenarFacturas();
        });

    }


    function llenarFacturas() {
        var resHTML = "";
        for (var i = 0; i < datosFactura.length; i++)
        {
            var factura = datosFactura[i];
            resHTML += "<tr>";

            resHTML += "<td style='text-align: justify;'>";
            resHTML += factura.noFactura;
            resHTML += "</td>";

            resHTML += "<td style='text-align: justify;'>";
            resHTML += factura.numeroOrden;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += factura.contenedor;
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += factura.fecha;
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += factura.etd;
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += factura.eta;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += factura.productos;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += factura.piezas;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += factura.total;
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="detalle-' + i + '"  class="detalle btn btn-danger btn-xs facturas" data-toggle="tooltip" data-placement="left">detail</button>';
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="exc-' + i + '"  class="btn btn-success btn-xs excel"  style="margin-right:5px;" >Excel</button>';
            resHTML += '<button type="button" id="xml-' + i + '"  class="btn btn-info btn-xs xml" style="margin-left:5px;">XML</button>';
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="eli-' + i + '"  class="delete btn btn-danger btn-xs">Eliminar</button>';
            resHTML += "</td>";

        }
        if (tablaFacturas != "")
        {
            tablaFacturas.destroy();
        }
        $("#cuerpoTabla").html(resHTML);

        $(".delete").click(function (e) {
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("eli-", "");
            facturasSel = datosFactura[miId * 1];
            $("#modalEliminar").modal();
            $("#eliminarFacturaTexto").html(facturasSel.noFactura);
            $("#eliminarContenedorTexto").html(facturasSel.contenedor);
        });

        $(".detalle").click(function (e) {
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("detalle-", "");
            facturasSel = datosFactura[miId * 1];
            obtenDetalle(facturasSel.idFactura);
        });

        $(".xml").click(function (e) {
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("xml-", "");
            facturasSel = datosFactura[miId * 1];
            obtenXML(facturasSel.idFactura);
        });

        $(".excel").click(function (e) {
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("exc-", "");
            facturasSel = datosFactura[miId * 1];
            obtenExcel(facturasSel.idFactura);
        });



        $("#encabezadoFacturas").html(llenaEncabezados());

        var filtros = new Array(1, 2, 3, 4, 5);

        tablaFacturas = crearDatatableFacturas("#tablaReporteConfirmacion", filtros);
    }

    function llenaEncabezados() {
        var resHTML = "";
        resHTML += '<tr>';
        resHTML += '<th style="background: white">Invoice</th>';
        resHTML += '<th >Order</th>';
        resHTML += '<th>Container</th>';
        resHTML += '<th>Date</th>';
        resHTML += '<th>ETD</th>';
        resHTML += '<th>ETA</th>';
        resHTML += '<th>Total products</th>';
        resHTML += '<th>Total pieces</th>';
        resHTML += '<th style="width: 20%">Total cost</th>';
        resHTML += '<th style="text-align: center">Details</th>';
        resHTML += '<th style="text-align: center">Downloads</th>';
        resHTML += '<th style="text-align: center">Acción</th>';
        resHTML += '</tr>';
        return resHTML;
    }

    function eliminaFactura(factura) {
        var path = "service/facturasColombia/elimina";
        conectaPost(path, factura, function (data)
        {
            $("#modalEliminar").modal('toggle');
            mensajeExito("Se realizó la eliminación correctamente.");
            buscar();
        });
    }

    function obtenXML(idFactura)
    {
        var path = "service/facturasColombia/xml?idFactura=" + idFactura;
        window.open(path, '_blank');
    }

    function obtenExcel(idFactura) {
        var path = "service/facturasColombia/excel?idFactura=" + idFactura;
        window.open(path, '_blank');
    }

    function obtenDetalle(idFactura)
    {
        var path = "service/facturasColombia/detalle";
        var datos = {
            idFactura: idFactura
        };
        conectaPost(path, datos, function (data)
        {
            detalleProductos = data;

            creaDetalle();
            $("#modalDetalle").modal();
        });
    }

    function creaDetalle() {

        $("#detalleNumOrden").html(facturasSel.numeroOrden);
        $("#detalleContenedor").html(facturasSel.contenedor);
        var resHTML = "";


        for (var i = 0; i < detalleProductos.length; i++) {
            var detalle = detalleProductos[i];
            resHTML += llenaCuarpoTab(detalle);
        }


        $("#cuerpoTablaFactura").html(resHTML);
        $("#tablaFactura").DataTable();

    }



    function llenaCuarpoTab(detalle)
    {
        var resHTML = "";

        resHTML += '<tr>';
        resHTML += '<td>';
        resHTML += detalle.sku;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.descripcion;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.piezas;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.precioUnitario;
        resHTML += '</td>';


        resHTML += '<td>';
        resHTML += detalle.precioTotal;
        resHTML += '</td>';
        resHTML += '</tr>';
        return resHTML;
    }

    function limpiar() {
        $("#noOrden").val("");
        $("#sku").val("");
        $("#fechaConfirmacion").val("");
        $("#Container").val("");
        $("#factura").val("");
        $("#etd").val("");
        $("#eta").val("");
    }
});



