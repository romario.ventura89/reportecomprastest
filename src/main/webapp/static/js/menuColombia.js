$(document).ready(function ()
{
    eventos();

    function eventos() {
        $("#inicio").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=1");
            loadPage("#menu", "service/navegacion/seleccionaMenu?idMenu=3");
        });

        $("#salir").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=2");
            loadPage("#menu", "service/navegacion/seleccionaMenu?idMenu=0");
        });
        
        $("#misConfirmaciones").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=11");
        });

        $("#cargaMasivaConfirmaciones").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=12");
        });
        $("#misFacturas").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=13");
        });
        $("#cargaMasivaFacturas").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=14");
        });
        $("#rConfirmacion").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=15");
        });

        $("#rComodato").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=16");
        });

        $("#rPedidos").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=17");
        });
        
        $("#menuReactivacion").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=18");
        });

        $("#descargaReporte").click(function () {
            leerArchivo("service/reportesGeneralColombia/excel", "REPORTE_GENERAL.xlsx");
        });
    }
});
