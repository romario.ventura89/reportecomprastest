<!--<script>
    $("body").show();
</script>
<h1>Reporte Confirmación</h1>-->
<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.control.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Reporte Confirmación</p>
    </div>
</div>
<div class="row">
    <hr class="rojohr"/>
</div>
<div class="row">
    <div class="col-md-12">

        <div class="container">
            <div class="row">
                <div role="tabpanel" class="tab-pane active" id="catReporte">
                    <fieldset>
                        <div class="col-md-12">
                            <table id="tablaReporteConfirmacion" class="table table-hover display" >
                                <thead>
                                    <tr>
                                        <th style="text-align: right;width: 5%">#</th>
                                        <th>SKU</th>
                                        <th>Product Name</th>
                                        <th>Confirmación</th>
                                        <th>Facturas</th>
                                    </tr>
                                </thead>

                                <tbody id="cuerpoReporteConfirmacion"></tbody>


                            </table>
                        </div>
                    </fieldset>
                </div>
                <div class="panel-footer text-right" id="botonExcel" style="margin-top: 20px">  
                    <button id="descargarReporteConfirmacion" class="btn btn-danger" >Download</button>
                </div>
                <div class="col-md-12 text-center titulo">
                    <span id="sinPedidos"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="static/js/reportes/reporte_confirmacion.js"></script>
<% }%>

