<%@page import="mx.com.miniso.control.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>
<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "../dmz/error.jsp";
</script>
<% } else {%>

<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Inicio</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Seleccione la sociedad:</h3>
    </div>
</div>
<div class="row">
    <hr />
</div>
<div class="row">
    <div class="col-md-12">
        <div id="sociedades"></div>
    </div>
</div>
<script src="static/js/bienvenida.js"></script>
<% }%>
