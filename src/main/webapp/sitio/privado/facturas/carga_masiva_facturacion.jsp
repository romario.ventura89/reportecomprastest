<%-- 
    Document   : carga_masiva
    Created on : 20/03/2018, 07:59:54 PM
    Author     : e5-523
--%>
<script>
    $("body").show();
</script>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Carga Masiva de Factura</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>


<div class="row">
    <form id="cargaMasiva" name="cargamasiva" id="cargamasiva"
          enctype="multipart/form-data" method="post" role="" class="form-horizontal">

        <div class="form-group">

            <label class="col-md-2 text-right">Order:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="numeroOrden" name="numeroOrden" class="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 text-right"  for="archivo">File excel invoice:</label> 
            <div class="col-md-8">
                <input class=" col-md-12"
                       name="archivo" id="archivo" type="file"
                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                       style="padding: 10px;" multiple/>
            </div>
        </div>

        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-danger" id="cargar">Upload</button>
        </div>

    </form>
</div>

<div class="row" id="panelResultados" style="display: none;margin-top: 30px" >
    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <label class="text-right">Resultados</label>
            </div>
            <ul id="breadcrumbs-one" class="nav nav-tabs" role="tablist">
                <li class="active"><a data-toggle="tab" href="#main">Guardado</a></li>
            </ul>
            <div class="tab-content">
                <div id="main" class="tab-pane fade in active">
                    <div class="panel-body">
                        <h1 id="resultadoCarga"></h1>
                        <h3>Order: <span id="orderRes"></span></h3>
                        <h5 id="datosRes"></h5>
                        <table id="tablaFactura" class="table table-hover display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Invoice</th>
                                    <th>Date invoice</th>
                                    <th>Container</th>
                                    <th>ETD</th>
                                    <th>ETA</th>
                                    <th>Product code</th>
                                    <th>Description</th>
                                    <th>Piece</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyRes"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal body" tabindex="-1" role="dialog" id="modalError" >
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Facturas que contienen error</h4>
            </div>
             <div class="modal-body">
                <div class="row">
                    <div  class="col-md-12">
                        <table id="tablaErr" class="table table-hover display" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Archivo</th>
                                    <th>Contenedor</th>
                                </tr>
                            </thead>
                            <tbody id="cuerpoTablaFactura"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<script src="static/js/factura/carga_masiva.js"></script>