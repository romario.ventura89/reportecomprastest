<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.control.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
    String nombre = usuario.getTitulo() + " " + usuario.getNombre() + " " + usuario.getApellidos();
%>
<div class="row">
    <div class="col-md-2 col-xs-3">
        <img src="static/img/logo_pagina.png" alt="Miniso" class="animated bounceIn" />
    </div>
    <div class="col-md-10 col-xs-9">
        <h1 id="tituloDentro">REPORTE DE COMPRAS MINISO Mx</h1>
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-8" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>  
            </div>
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-8"> 
                <ul class="nav navbar-nav " style="width: 100%"> 
                    <li class="navbar-tex">
                        <a href="javascript:;" id="inicio"><i class="glyphicon glyphicon-home"></i> Inicio</a>
                    </li> 
                    <li class="navbar-tex navbar-right">
                        <a href="javascript:;" id="salir"><i class="fa  fa-power-off"></i> Salir</a>
                    </li> 
                    <li class="navbar-tex navbar-right">
                        <a href="javascript:;">Usuario: <span id="nombreUsuario" style="font-weight: bold"><%=nombre%></span></a>
                    </li> 
                </ul> 
            </div>
        </nav>
    </div>

</div>


<script src="static/js/menuGeneral.js"></script>
<% }%>