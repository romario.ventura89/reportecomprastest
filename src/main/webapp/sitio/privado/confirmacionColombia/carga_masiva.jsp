<%@page import="mx.com.miniso.control.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>
<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Carga Masiva de Confirmación</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>


<div class="row">
    <form id="cargaMasiva" name="cargamasiva" id="cargamasiva"
          enctype="multipart/form-data" method="post" role="" class="form-horizontal">

        <div class="form-group">
            <label class="col-md-2 text-right">Fecha de confirmación:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="fechaConfirmacion" name="fechaConfirmacion" class="form-control" readonly="readonly"/>
            </div>

            <%--
            <label class="col-md-2 text-right">Número de orden:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="numeroOrden" name="numeroOrden" class="form-control"/>
            </div>
            --%>

        </div>
        <div class="form-group">
            <label class="col-md-2 text-right"  for="archivo">Archivo de carga masiva:</label> 
            <div class="col-md-8">
                <input class=" col-md-12"
                       name="archivo" id="archivo" type="file"
                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                       style="padding: 10px;" multiple/>
            </div>
        </div>

        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-danger" id="cargar">Upload</button>
        </div>

    </form>
</div>

<div class="row" id="panelResultados" style="display: none;margin-top: 30px" >
    <div class="col-md-12">
        <div class="reg-panel">
            <div class="panel-heading">
                <label class="text-right">Resultados</label>
            </div>
            <ul id="breadcrumbs-one" class="nav nav-tabs" role="tablist">
                <li class="active"><a data-toggle="tab" href="#main">Guardado</a></li>
                <li><a data-toggle="tab" href="#tab1">Error</a></li>
            </ul>
            <div class="tab-content">
                <div id="main" class="tab-pane fade in active">
                    <div class="panel-body">
                        <h1 id="resultadoCarga"></h1>
                        <h3>Fecha de confirmación: <span id="fecRes"></span></h3>
                        <h5 id="datosRes"></h5>
                        <table id="tablaConfirmacion" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order</th>
                                    <th>Type</th>
                                    <th>Product code</th>
                                    <th>Product name</th>
                                    <th>Price</th>
                                    <th>Cbm</th>
                                    <th>Ordered quantity</th>
                                    <th>Final shipped quantity</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyRes">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="tab1" class="tab-pane fade">
                    <div class="panel-body">
                        <h1 id="resultadoCargaError"></h1>
                        <h3>Fecha de confirmación: <span id="fecResError"></span></h3>
                        <h5 id="datosResError"></h5>
                        <table id="tablaConfirmacionError" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order</th>
                                    <th>Type</th>
                                    <th>Product code</th>
                                    <th>Product name</th>
                                    <th>Price</th>
                                    <th>Cbm</th>
                                    <th>Ordered quantity</th>
                                    <th>Final shipped quantity</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyResError">
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
            
<div class="modal body" tabindex="-1" role="dialog" id="modalError" >
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmaciones que contienen error</h4>
            </div>
             <div class="modal-body">
                <div class="row">
                    <div  class="col-md-12">
                        <table id="tablaErr" class="table table-hover display" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Archivo</th>
                                    <th>Contenedor</th>
                                </tr>
                            </thead>
                            <tbody id="cuerpoTablaFactura"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal body" tabindex="-1" role="dialog" id="modalConfirmacion" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Remplazar archivo</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div  class="col-md-12">
                        <h5>¿Seguro que desea remplazar la informacion de la confirmación con el número de orden <span id="numeroContenedorRemplazar"></span>?</h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="">Close</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="modalSubir">Upload</button>
            </div>
        </div>
    </div>
</div>   

<script src="static/js/confirmacionColombia/carga_masiva.js"></script>

<% }%>