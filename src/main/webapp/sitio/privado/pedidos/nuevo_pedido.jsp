<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.control.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>



<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Nuevo pedido</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>

<div class="row">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-md-2 text-right">Fecha de pedido:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="fechaPedido" class="form-control" readonly="readonly"/>
            </div>
        </div>
        <div class="form-group productos" >
            <label class="col-md-2 text-right">SKU:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="sku" class="form-control" required="required" maxlength="30">
            </div>
            <label class="col-md-2 text-right">Cantidad:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="piezas" class="form-control" required="required" maxlength="10"/>
            </div>

            <div class="col-md-3 text-right">
                <button type="button" class="btn btn-danger" id="guardaSKU">Agregar</button>
            </div>
        </div>
    </div>
    
    
    <div id="panelProductos" style="display:none;margin-top: 30px">
        <div class="col-md-12" >
            <table id="tablaControlVentas" class="table table-hover display" >
                <thead>
                    <tr>
                        <th style="text-align: right;width: 5%">#</th>
                        <th>SKU</th>
                        <th>Piezas</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="cuerpoControlVentas">

                </tbody>
            </table>
        </div>

        <div class="panel-footer text-right" id="botonExcel">  
            <a herf="service/descagasExcel/pedido" id="descargarPedido" class="btn btn-danger" target="_blank">Descargar Excel</a>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modalEliminar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmación</h4>
            </div>
            <div class="modal-body">
                <h4>¿Seguro que desea eliminar el producto "<strong id="skuEliminar"></strong>"?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-d" data-dismiss="modal" aria-label="Close">Cancelar</button>
                <button type="button" class="btn btn-danger" id="eliminarProductoPedido">Eliminar</button>
            </div>
        </div>
    </div>
</div>    

<script src="static/js/nuevo_pedido.js"></script>

<% }%>
