package mx.com.miniso.control.util;

import java.io.Serializable;

public class RespuestaValidador implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int codigo;
	private String mensaje;
	private String dato;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getDato() {
		return dato;
	}

	public void setDato(String dato) {
		this.dato = dato;
	}

	@Override
	public String toString() {
		return "RespuestaValidador [codigo=" + codigo + ", mensaje=" + mensaje + ", dato=" + dato + "]";
	}

}
