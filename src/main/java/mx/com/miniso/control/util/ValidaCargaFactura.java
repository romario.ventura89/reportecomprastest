package mx.com.miniso.control.util;

import java.util.List;
import mx.com.miniso.control.ex.ValidacionFormularioException;
import mx.com.miniso.control.service.ConfirmacionService;
import mx.com.miniso.control.service.FacturaService;
import mx.com.miniso.control.vo.CargaFacturaDatosVO;
import mx.com.miniso.control.vo.CargaFacturaVO;
import org.apache.commons.lang3.math.NumberUtils;

public class ValidaCargaFactura extends Validador<CargaFacturaVO> {

    private final FacturaService facturaService;
    private final ConfirmacionService confirmacionService;

    public ValidaCargaFactura(FacturaService facturaService, ConfirmacionService confirmacionService) {
        this.facturaService = facturaService;
        this.confirmacionService = confirmacionService;
    }

    @Override
    public boolean realizarValidacion(CargaFacturaVO cargaVO) throws ValidacionFormularioException {

        boolean error = false;
        error = validaFecha(cargaVO, error);
        error = validaNoOrden(cargaVO, error);
        error = validaNoContendor(cargaVO, error);
        error = validaNoFactura(cargaVO, error);
        error = validaNoEtd(cargaVO, error);
        error = validaEta(cargaVO, error);
        error = validaDatos(cargaVO.getDatos(), error);

        if (cargaVO.getDatos().isEmpty()) {
            cargaVO.setErrorListado(true);
            error = true;
        }

        return error;
    }

    private boolean validaFecha(CargaFacturaVO cargaVO, boolean error) {
        String fecha = cargaVO.getFechaFactura();

        try {
            validaObligatorio(fecha);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setFechaFacturaError(true);
            cargaVO.setFechaFacturaErrorTXT(ex.getMessage());
        }

        try {
            validaFecha(fecha);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setFechaFacturaError(true);
            cargaVO.setFechaFacturaErrorTXT(ex.getMessage());
        }

        return error;

    }

    private boolean validaNoOrden(CargaFacturaVO cargaVO, boolean error) {
        String noOrden = cargaVO.getNumeroOrden();
        try {
            validaObligatorio(noOrden);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setNumeroOrdenError(true);
            cargaVO.setNumeroOrdenErrorTXT(ex.getMessage());
        }

        if (!confirmacionService.existeNoOrden(noOrden)) {
            error = true;
            cargaVO.setNumeroOrdenError(true);
            cargaVO.setNumeroOrdenErrorTXT("No existe el número de orden " + noOrden + " en las confirmaciones.");
        }

        return error;
    }

    private boolean validaDatos(List<CargaFacturaDatosVO> datos, boolean error) {
        for (CargaFacturaDatosVO carga : datos) {
            String precio = carga.getPrecio();
            String piezas = carga.getPiezas();

            try {
                Double.parseDouble(precio);
            } catch (NumberFormatException ex) {
                carga.setPrecioError(true);
                carga.setPrecioErrorTxt("El campo debe ser númerico");
                error = true;
            }

            if (!NumberUtils.isNumber(piezas)) {
                carga.setPiezasError(true);
                carga.setPiezasErrorTxt("El campo debe ser númerico");
                error = true;
            }
        }
        return error;
    }

    private boolean validaNoContendor(CargaFacturaVO cargaVO, boolean error) {
        String contenedor = cargaVO.getContenedor();

        try {
            validaObligatorio(contenedor);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setContenedorError(true);
            cargaVO.setContenedorErrorTXT(ex.getMessage());
        }
        return error;
    }

    private boolean validaNoFactura(CargaFacturaVO cargaVO, boolean error) {
        String factura = cargaVO.getNumeroFactura();

        try {
            validaObligatorio(factura);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setNumeroFacturaError(true);
            cargaVO.setNumeroFacturaErrorTXT(ex.getMessage());
        }

        if (facturaService.existeXCodigo(factura)) {
            error = true;
            cargaVO.setNumeroFacturaError(true);
            cargaVO.setNumeroFacturaErrorTXT("Ya fue ingresado el número de factura " + factura);
        }
        return error;
    }

    private boolean validaNoEtd(CargaFacturaVO cargaVO, boolean error) {
        String fecha = cargaVO.getEtd();

        try {
            validaObligatorio(fecha);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setEtdError(true);
            cargaVO.setEtdErrorTXT(ex.getMessage());
        }

        try {
            validaFecha(fecha);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setEtdError(true);
            cargaVO.setEtdErrorTXT(ex.getMessage());
        }

        return error;
    }

    private boolean validaEta(CargaFacturaVO cargaVO, boolean error) {
        String fecha = cargaVO.getEta();

        try {
            validaObligatorio(fecha);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setEtaError(true);
            cargaVO.setEtaErrorTXT(ex.getMessage());
        }

        try {
            validaFecha(fecha);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setEtaError(true);
            cargaVO.setEtaErrorTXT(ex.getMessage());
        }

        return error;
    }
}
