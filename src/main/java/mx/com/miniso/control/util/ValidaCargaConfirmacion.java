package mx.com.miniso.control.util;

import java.util.List;
import java.util.Map;
import mx.com.miniso.control.ex.ValidacionFormularioException;
import mx.com.miniso.control.service.ConfirmacionService;
import mx.com.miniso.control.vo.CargaConfirmacionDatosVO;
import mx.com.miniso.control.vo.CargaConfirmacionVO;

public class ValidaCargaConfirmacion extends Validador<CargaConfirmacionVO> {

    private final ConfirmacionService confirmacionService;

    public ValidaCargaConfirmacion(ConfirmacionService confirmacionService) {
        this.confirmacionService = confirmacionService;
    }

    @Override
    public boolean realizarValidacion(CargaConfirmacionVO cargaVO) throws ValidacionFormularioException {
        boolean error = false;
        error = validaFecha(cargaVO, error);
        error = validaNoOrden(cargaVO, error);
        for (Map.Entry<String, List<CargaConfirmacionDatosVO>> entry : cargaVO.getCargaInformacion().entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
            error = validaDatos(entry.getValue(), error);

            if (entry.getValue().isEmpty()) {
                cargaVO.setErrorListado(true);
                error = true;
            }
        }

        /*error = validaDatos(cargaVO.getGeneral(), error);
        error = validaDatos(cargaVO.getPingshan(), error);
        error = validaDatos(cargaVO.getQianhai(), error);
        if (cargaVO.getGeneral().isEmpty() && cargaVO.getPingshan().isEmpty() && cargaVO.getQianhai().isEmpty()) {
            cargaVO.setErrorListado(true);
            error = true;
        }*/
        return error;
    }

    private boolean validaFecha(CargaConfirmacionVO cargaVO, boolean error) {
        String fecha = cargaVO.getFechaConfirmacion();

        try {
            validaObligatorio(fecha);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setFechaConfirmacionError(true);
            cargaVO.setFechaConfirmacionErrorTxt(ex.getMessage());
        }

        try {
            validaFecha(fecha);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setFechaConfirmacionError(true);
            cargaVO.setFechaConfirmacionErrorTxt(ex.getMessage());
        }

        return error;

    }

    private boolean validaNoOrden(CargaConfirmacionVO cargaVO, boolean error) {
        String noOrden = cargaVO.getNumeroOrden();
        try {
            validaObligatorio(noOrden);
        } catch (ValidacionFormularioException ex) {
            error = true;
            cargaVO.setNumeroOrdenError(true);
            cargaVO.setNumeroOrdenErrorTxt(ex.getMessage());
        }

        if (confirmacionService.existeNoOrden(noOrden)) {
            error = true;
            cargaVO.setNumeroOrdenError(true);
            cargaVO.setNumeroOrdenErrorTxt("Ya fue ingresado el número de orden " + noOrden);
        }

        return error;
    }

    private boolean validaDatos(List<CargaConfirmacionDatosVO> datos, boolean error) {
        for (CargaConfirmacionDatosVO carga : datos) {
            String sku = carga.getSku();
            try {
                validaObligatorio(sku);
            } catch (ValidacionFormularioException ex) {
                error = true;
                carga.setSkuError(true);
                carga.setSkuErrorTXT(ex.getMessage());
            }
        }
        return error;
    }

}
