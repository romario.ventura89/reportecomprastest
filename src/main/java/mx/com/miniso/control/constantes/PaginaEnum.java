package mx.com.miniso.control.constantes;

public enum PaginaEnum {

    LOGIN(0, "redirect:/sitio/publico/login.jsp"),
    BIENVENIDA(1, "redirect:/sitio/privado/usr/bienvenida.jsp"),
    SALIR(2, "redirect:/service/login/salir"),
    
    //MEXICO CHINA
    CONFIRMACION(3, "redirect:/sitio/privado/confirmacion/mis_confirmaciones.jsp"),
    CARGA_MASIVA_CONFIRMACION(4, "redirect:/sitio/privado/confirmacion/carga_masiva.jsp"),
    FACTURAS(5, "redirect:/sitio/privado/facturas/mis_facturas.jsp"),
    CARGA_MASIVA_FACTURACION(6, "redirect:/sitio/privado/facturas/carga_masiva_facturacion.jsp"),
    REPORTE_CONFIRMACION(7, "redirect:/sitio/privado/reporte/confirmacion.jsp"),
    REPORTE_COMODATO(8, "redirect:/sitio/privado/reporte/comodato.jsp"),
    REPORTE_PEDIDOS(9, "redirect:/sitio/privado/reporte/pedidos.jsp"),
    REACTIVACION_FACTURA(10, "redirect:/sitio/privado/facturas/reactivacion_facturas.jsp"),
    
    //COLOMBIA CHINA
    CONFIRMACION_COLOMBIA(11, "redirect:/sitio/privado/confirmacionColombia/mis_confirmaciones.jsp"),
    CARGA_MASIVA_CONFIRMACION_COLOMBIA(12, "redirect:/sitio/privado/confirmacionColombia/carga_masiva.jsp"),
    FACTURAS_COLOMBIA(13, "redirect:/sitio/privado/facturasColombia/mis_facturas.jsp"),
    CARGA_MASIVA_FACTURACION_COLOMBIA(14, "redirect:/sitio/privado/facturasColombia/carga_masiva_facturacion.jsp"),
    REPORTE_CONFIRMACION_COLOMBIA(15, "redirect:/sitio/privado/reporteColombia/confirmacion.jsp"),
    REPORTE_COMODATO_COLOMBIA(16, "redirect:/sitio/privado/reporteColombia/comodato.jsp"),
    REPORTE_PEDIDOS_COLOMBIA(17, "redirect:/sitio/privado/reporteColombia/pedidos.jsp"),
    REACTIVACION_FACTURA_COLOMBIA(18, "redirect:/sitio/privado/facturasColombia/reactivacion_facturas.jsp")
    
    ;
    
    
    
    int id;
    String path;

    PaginaEnum(int id, String path) {
        this.id = id;
        this.path = path;
    }

    public int getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

}
