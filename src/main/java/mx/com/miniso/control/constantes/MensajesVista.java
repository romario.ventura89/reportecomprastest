
package mx.com.miniso.control.constantes;



public class MensajesVista {
    
    public static final String ERROR_EXCEPCION = "<strong>No se pudo realizar el tr&aacute;mite solicitado:</strong> por favor intente m&aacute;s tarde.";
    public static final String ERROR_ORDER = "<strong>No se pudo realizar la carga:</strong> No existe el n&uacute;mero de orden cargado.";
    
}
