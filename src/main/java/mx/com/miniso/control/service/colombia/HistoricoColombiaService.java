package mx.com.miniso.control.service.colombia;

import mx.com.miniso.control.service.*;
import java.util.List;
import mx.com.miniso.control.dao.FacturaProductoDao;
import mx.com.miniso.control.dao.HistoricoFacturaDao;
import mx.com.miniso.control.dao.colombia.FacturaProductoColombiaDao;
import mx.com.miniso.control.dao.colombia.HistoricoFacturaProductoColombiaDao;
import mx.com.miniso.control.dao.colombia.HistoricoFacturaColombiaDao;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.dto.FacturaProducto;
import mx.com.miniso.control.dto.HistoricoFactura;
import mx.com.miniso.control.dto.HistoricoFacturaProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoricoColombiaService {

    @Autowired
    private HistoricoFacturaColombiaDao historicoFacturaColombiaDao;

    @Autowired
    private HistoricoFacturaProductoColombiaDao historicoFacturaProductoColombiaDao;

    @Autowired
    private FacturaProductoColombiaDao facturaProductoColombiaDao;
    
     public void copiaFacturasAHistorico(Factura factura) {
         HistoricoFactura historicoFactura = new HistoricoFactura(factura);
            historicoFactura = historicoFacturaColombiaDao.guardar(historicoFactura);
            List<FacturaProducto> listaFacturaProducto = facturaProductoColombiaDao.obtenerProductosIdFactura(factura.getIdFactura());
            if (listaFacturaProducto != null) {
                for (FacturaProducto facturaProducto : listaFacturaProducto) {
                    HistoricoFacturaProducto historicoProducto = new HistoricoFacturaProducto(facturaProducto, historicoFactura);
                    historicoFacturaProductoColombiaDao.guardar(historicoProducto);
                }
            }
     }

    public void copiaFacturasAHistorico(List<Factura> facturas) {
        for (Factura factura : facturas) {
            copiaFacturasAHistorico(factura);
        }

    }
}
