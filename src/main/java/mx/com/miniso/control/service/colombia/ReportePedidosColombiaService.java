package mx.com.miniso.control.service.colombia;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.control.dao.colombia.ReportePedidosColombiaDao;
import mx.com.miniso.control.vo.FaltanteOrderVO;
import mx.com.miniso.control.vo.FiltroPedidosVO;
import mx.com.miniso.control.vo.OrderVO;
import mx.com.miniso.control.vo.PedidoOrdenVO;
import mx.com.miniso.control.vo.PedidosVO;
import mx.com.miniso.control.vo.ReportePedidosVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportePedidosColombiaService {

    @Autowired
    private ReportePedidosColombiaDao reportePedidosColombiaDao;

    public ReportePedidosVO buscar(FiltroPedidosVO filtro) {
        ReportePedidosVO reporte = new ReportePedidosVO();
        List<PedidosVO> registros = reportePedidosColombiaDao.obtenerTotales();
        List<OrderVO> ordenes = reportePedidosColombiaDao.obtenerOrdenes();
        List<FaltanteOrderVO> faltantes = reportePedidosColombiaDao.obtenerFaltantes();

        for (OrderVO ordenVO : ordenes) {
            buscaPiezasOrden(registros, ordenVO.getIdComfirmacion());

        }
        agregaFaltantes(registros, faltantes);

        reporte.setOrdenes(ordenes);
        reporte.setPedidos(registros);

        return reporte;
    }

    private void buscaPiezasOrden(List<PedidosVO> registros, String idConfirmacion) {
        List<PedidoOrdenVO> pedidosOrdenes = reportePedidosColombiaDao.obtenPiezasOrden(idConfirmacion);
        for (PedidosVO registro : registros) {
            boolean encontrado = false;
            for (PedidoOrdenVO pedidoOrdenVO : pedidosOrdenes) {
                if (registro.getSku().equals(pedidoOrdenVO.getSku())) {
                    registro.getPedidosOrdenes().add(pedidoOrdenVO);
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                PedidoOrdenVO pedidoOrdenVO = new PedidoOrdenVO();
                pedidoOrdenVO.setSku(registro.getSku());
                pedidoOrdenVO.setPiezas("0");
                pedidoOrdenVO.setIdConfirmacion(idConfirmacion);
                registro.getPedidosOrdenes().add(pedidoOrdenVO);
            }
        }
    }

    private void agregaFaltantes(List<PedidosVO> pedidos, List<FaltanteOrderVO> faltantes) {
        List<FaltanteOrderVO> temFatantes;
        for (PedidosVO pedido : pedidos) {
            temFatantes = new ArrayList<>();
            int totalFacturas = Integer.parseInt(pedido.getTotalFacturas().replace(",", ""));
            int sumando = 0;
            boolean banSKU = false;
            boolean encontrado = false;
            for (FaltanteOrderVO faltante : faltantes) {

                if (pedido.getSku().equals(faltante.getSku())) {
                    sumando += faltante.getConfirmaciones();
                    if (encontrado) {
                        if (faltante.getConfirmaciones() != 0) {
                            faltante.setFaltantes(faltante.getConfirmaciones() * -1);
                            temFatantes.add(faltante);
                        }
                    } else {

                        if (sumando >= totalFacturas) {
                            int dif = sumando - totalFacturas;

                            if (dif != 0) {
                                faltante.setFaltantes(dif * -1);
                                temFatantes.add(faltante);
                                encontrado = true;
                            }
                        }
                    }

                    banSKU = true;
                } else {
                    if (banSKU) {
                        break;
                    }
                }
            }
            pedido.setFaltantes(temFatantes);
        }
    }

}
