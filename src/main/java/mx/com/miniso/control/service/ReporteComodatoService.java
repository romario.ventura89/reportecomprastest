package mx.com.miniso.control.service;

import java.util.List;
import mx.com.miniso.control.dao.ReporteComodatoDao;
import mx.com.miniso.control.vo.ComodatoVO;
import mx.com.miniso.control.vo.ContainerVO;
import mx.com.miniso.control.vo.ContenedorVO;
import mx.com.miniso.control.vo.ReporteComodatoVO;
import mx.com.miniso.control.vo.FiltroComodatoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReporteComodatoService {

    @Autowired
    private ReporteComodatoDao reporteComodatoDao;

    public ReporteComodatoVO buscar(FiltroComodatoVO filtro) {
        ReporteComodatoVO reporte = new ReporteComodatoVO();
        List<ComodatoVO> registros = reporteComodatoDao.obtenerTotales();
        List<ContainerVO> contenedores = reporteComodatoDao.obtenerContenedores();

        for (ComodatoVO comodato : registros) {
            asignaNombreOrden(comodato);
        }
        for (ContainerVO contenedor : contenedores) {
            buscaPiezasFactura(registros, contenedor.getIdFactura());
        }

        reporte.setContenedores(contenedores);
        reporte.setComodatos(registros);
        return reporte;
    }

    private void asignaNombreOrden(ComodatoVO comodato) {
        List<String> numeroOrdenes = reporteComodatoDao.obtnerNumeroOrdenes(comodato.getSku());
        String nombreNumeroOrden = "";
        int k = 0;
        for (String numeroOrden : numeroOrdenes) {
            nombreNumeroOrden += numeroOrden;
            if (k + 1 != numeroOrdenes.size()) {
                nombreNumeroOrden += " / ";
            }
            k++;
        }
        comodato.setNumeroOrden(nombreNumeroOrden);
    }

    private void buscaPiezasFactura(List<ComodatoVO> registros, String idFactura) {
        List<ContenedorVO> contenedores = reporteComodatoDao.obtenPiezas(idFactura);
        for (ComodatoVO registro : registros) {
            boolean encontrado = false;
            for (ContenedorVO contendor : contenedores) {
                if (registro.getSku().equals(contendor.getSku())) {
                    registro.getContenedores().add(contendor);
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                ContenedorVO miContenedor = new ContenedorVO();
                miContenedor.setSku(registro.getSku());
                miContenedor.setPiezas("0");
                miContenedor.setIdFactura(idFactura);
                registro.getContenedores().add(miContenedor);
            }
        }
    }

}
