package mx.com.miniso.control.service;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.control.dao.ConfirmacionDao;
import mx.com.miniso.control.dao.ConfirmacionProductoDao;
import mx.com.miniso.control.dao.FacturaDao;
import mx.com.miniso.control.dao.FacturaProductoDao;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.vo.ConfirmacionDetalleVO;
import mx.com.miniso.control.vo.ConfirmacionesVO;
import mx.com.miniso.control.vo.FiltrosConfirmacionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfirmacionService {

    @Autowired
    private ConfirmacionDao confirmacionDao;

    @Autowired
    private ConfirmacionProductoDao confirmacionProductoDao;

    @Autowired
    private FacturaProductoDao facturaProductoDao;

    @Autowired
    private FacturaDao facturaDao;
    
    @Autowired
    private HistoricoService historicoService;

   

    public List<ConfirmacionesVO> buscarConfirmaciones(FiltrosConfirmacionVO filtrosConfirmacionVO) throws Exception {
        List<ConfirmacionesVO> confirmaciones = new ArrayList<>(0);
        if (filtrosConfirmacionVO == null) {
            return confirmaciones;
        }

        confirmaciones = confirmacionDao.obtenerXParametros(filtrosConfirmacionVO);
        for (ConfirmacionesVO confirmacion : confirmaciones) {
            confirmacion = confirmacionDao.obtenTotalesMisConfirmaciones(confirmacion);
        }

        return confirmaciones;
    }

    public List<ConfirmacionDetalleVO> obtenerDetalle(ConfirmacionesVO confirmacionVO) {
        List<ConfirmacionDetalleVO> detalles = confirmacionDao.obtenDetalleMisConfirmaciones(confirmacionVO);
        return detalles;
    }

    public Confirmacion buscarNumeroOrden(String idConfirmacion) {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setIdConfirmacion(Long.parseLong(idConfirmacion));
        confirmacion = confirmacionDao.obtenerXID(confirmacion);
        return confirmacion;
    }

    public boolean existeNoOrden(String noOrden) {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setNoOrden(noOrden);
        confirmacion = confirmacionDao.obtenerXNumeroOrden(confirmacion);
        return confirmacion != null;
    }

    public void eliminar(ConfirmacionesVO confirmacionVO) {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setIdConfirmacion(Long.parseLong(confirmacionVO.getIdConfirmacion()));
        confirmacion = confirmacionDao.obtenerXID(confirmacion);

        confirmacionProductoDao.eliminaXConfirmacionTipo(confirmacion, confirmacionVO.getTipoOrden());

        int cuentaProductos = confirmacionProductoDao.cuentaProductos(confirmacion);

        if (0 == cuentaProductos) {
            List<Factura> facturas = facturaDao.obtenerXConfirmacion(confirmacion);
            historicoService.copiaFacturasAHistorico(facturas);
            eliminaFacturas(facturas);
            confirmacionDao.borrar(confirmacion);
        }
    }



    private void eliminaFacturas(List<Factura> facturas) {

        for (Factura factura : facturas) {
            facturaProductoDao.eliminaXFactura(factura);
            facturaDao.borrar(factura);
        }

    }

}
