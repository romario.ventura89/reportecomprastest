package mx.com.miniso.control.service.colombia;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.control.dao.colombia.FacturaColombiaDao;
import mx.com.miniso.control.dao.colombia.FacturaProductoColombiaDao;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.vo.FacturaDetalleVO;
import mx.com.miniso.control.vo.FacturaVO;
import mx.com.miniso.control.vo.FiltrosFacturaVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FacturaColombiaService {

    @Autowired
    private FacturaProductoColombiaDao facturaProductoColombiaDao;

    @Autowired
    private FacturaColombiaDao facturaColombiaDao;

    @Autowired
    private HistoricoColombiaService historicoColombiaService;

    public List<FacturaVO> buscarFacturas(FiltrosFacturaVO filtrosFacturaVO) throws Exception {
        List<FacturaVO> facturas = new ArrayList<>(0);
        if (filtrosFacturaVO == null) {
            return facturas;
        }

        facturas = facturaColombiaDao.obtenerXParametros(filtrosFacturaVO);
        for (FacturaVO factura : facturas) {
            factura = facturaColombiaDao.obtenTotalesMisFacturas(factura);
        }

        return facturas;
    }

    public List<FacturaDetalleVO> obtenerDetalle(FacturaVO facturaVO) {
        List<FacturaDetalleVO> detalles = facturaColombiaDao.obtenDetalleMisFacturas(facturaVO);
        return detalles;
    }

    public Factura buscarXId(String idFactura) {
        Factura factura = new Factura();
        factura.setIdFactura(Long.parseLong(idFactura));
        factura = facturaColombiaDao.obtenerXID(factura);
        return factura;
    }

    public void eliminar(FacturaVO facturaVO) {
        Factura factura = new Factura();
        factura.setIdFactura(Long.parseLong(facturaVO.getIdFactura()));
        factura = facturaColombiaDao.obtenerXID(factura);
        historicoColombiaService.copiaFacturasAHistorico(factura);
        facturaProductoColombiaDao.eliminaXFactura(factura);
        facturaColombiaDao.borrar(factura);
    }

    public boolean existeXCodigo(String codigo) {
        Factura factura = new Factura();
        factura.setNoFactura(codigo);
        factura = facturaColombiaDao.obtenerXNo(factura);
        return factura != null;
    }

}
