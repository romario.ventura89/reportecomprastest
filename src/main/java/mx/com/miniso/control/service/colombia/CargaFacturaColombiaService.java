package mx.com.miniso.control.service.colombia;

import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObject;
import com.optimaize.langdetect.text.TextObjectFactory;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import mx.com.miniso.control.dao.colombia.ConfirmacionColombiaDao;
import mx.com.miniso.control.dao.colombia.FacturaColombiaDao;
import mx.com.miniso.control.dao.colombia.FacturaProductoColombiaDao;
import mx.com.miniso.control.dao.colombia.ProductoCompraColombiaDao;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.dto.FacturaProducto;
import mx.com.miniso.control.dto.ProductoCompra;
import mx.com.miniso.control.util.FechaUtil;
import mx.com.miniso.control.vo.CargaFacturaDatosVO;
import mx.com.miniso.control.vo.CargaFacturaVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CargaFacturaColombiaService {

    final static Logger logger = LoggerFactory.getLogger(CargaFacturaColombiaService.class);

    @Autowired
    private ConfirmacionColombiaDao confirmacionColombiaDao;

    @Autowired
    private FacturaColombiaDao facturaColombiaDao;

    @Autowired
    private FacturaProductoColombiaDao facturaProductoColombiaDao;

    @Autowired
    private ProductoCompraColombiaDao productoCompraColombiaDao;
    public static final String MODEL_NO = "Model No.";//3 columnaSKU
    public static final String DESCRIPTION = "DESCRIPTION";//5 columnaNombre
    public static final String PIECE = "Piece";//7 columnaPiezas
    public static final String UNIT_PRICE = "Unit Price";//8 columnaPrecioUnitario
    public static final String TOTAL_PRIECE = "Total Price";//9 columnaPrecioTotal

    public CargaFacturaVO obtenerDatosGenerales(CargaFacturaVO cargaVO, XSSFSheet sheet) throws IOException {
        String invoceDate;
        String invoice;
        String container;
        String etd;
        String eta;

        if (sheet != null) {
            invoceDate = extraValor(buscarContenidoCelda(sheet, "INVOICE DATE:"), ":", 1);
            invoceDate = convertirFecha(invoceDate.replace('.', '/'), "dd/MM/yyyy");
            cargaVO.setFechaFactura(invoceDate);

            //container = extraValor(buscarContenidoCelda(sheet, "CONTAINER:"), ":", 1);
            //cargaVO.setContenedor(container);

            invoice = extraValor(buscarContenidoCelda(sheet, "INVOICE NO:"), ":", 1);
            cargaVO.setNumeroFactura(invoice);

            etd = extraValor(buscarContenidoCelda(sheet, "ETD:"), ":", 1);
            etd = convertirFecha(etd.replace('-', '/'), "dd/MM/yyyy");
            cargaVO.setEtd(etd);

            eta = extraValor(buscarContenidoCelda(sheet, "ETA:"), ":", 1);
            eta = convertirFecha(eta.replace('-', '/'), "dd/MM/yyyy");
            cargaVO.setEta(eta);
        }
        return cargaVO;
    }

    public String convertirFecha(String fecha, String formato) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat simpleDateFormatEnd = new SimpleDateFormat(formato);
        try {
            Date date = simpleDateFormat.parse(fecha);
            fecha = simpleDateFormatEnd.format(date);
            //fecha = simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return fecha;
    }

    public CargaFacturaVO obtenerDatos(CargaFacturaVO cargaVO, MultipartFile archivo) throws IOException {
        Workbook wb = null;
        try {
            wb = new XSSFWorkbook(archivo.getInputStream());
        } catch (IOException e) {
            logger.error("Error al convertir el archivo al formato de Excel");
            throw e;
        }
        XSSFSheet sheet = (XSSFSheet) wb.getSheetAt(0);
        if (sheet != null) {
            cargaVO = obtenerDatosGenerales(cargaVO, sheet);
            cargaVO.setDatos(extraeHoja(sheet));
        }
        return cargaVO;
    }

    private String buscarContenidoCelda(XSSFSheet sheet, String contenidoCelda) {
        String valueCell = "";
        for (Row row : sheet) {
            for (Cell cell : row) {
                if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    valueCell = cell.getRichStringCellValue().getString().trim();
                    int resultado = valueCell.indexOf(contenidoCelda);
                    if (resultado != -1) {
                        return valueCell;
                    } else {
                        valueCell = "";
                    }
                }
            }
        }
        return valueCell;
    }

    public String extraValor(String cadena, String separador, int index) {
        StringTokenizer stringTokenizer = new StringTokenizer(cadena, separador);
        String valor = "";
        int i = 0;
        while (stringTokenizer.hasMoreElements()) {
            valor = stringTokenizer.nextElement().toString();
            if (i == index) {
                break;
            }
            i++;
        }
        return valor;
    }

    private List<CargaFacturaDatosVO> extraeHoja(XSSFSheet sheet) {
        int columnaSKU = 3;
        int columnaNombre = 6;
        int columnaPiezas = 7;
        int columnaPrecioUnitario = 8;
        int columnaPrecioTotal = 9;
        List<CargaFacturaDatosVO> datos = new ArrayList<>(0);
        int i = 0;

        for (Row row : sheet) {
            if (i == 10) {

                // obtiene el nombre de los encabezados
                for (int col = 0; col < row.getLastCellNum(); col++) {
                    Cell cell = row.getCell(col);

                    if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                        // Can't be this cell - it's empty
                        continue;
                    }

                    if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                        String value = cell.getStringCellValue();

                        if (value.contains(MODEL_NO)) {
                            columnaSKU = cell.getAddress().getColumn();
                        } else if (value.contains(DESCRIPTION)) {
                            columnaNombre = cell.getAddress().getColumn();
                        } else if (value.contains(PIECE)) {
                            columnaPiezas = cell.getAddress().getColumn();
                        } else if (value.contains(UNIT_PRICE)) {
                            columnaPrecioUnitario = cell.getAddress().getColumn();
                        } else if (value.contains(TOTAL_PRIECE)) {
                            columnaPrecioTotal = cell.getAddress().getColumn();
                        }
                    }
                }

            } else if (i >= 11) {
                CargaFacturaDatosVO dato = new CargaFacturaDatosVO();
                String valor = formateaValor(row.getCell(columnaSKU));

                if (StringUtils.isEmpty(valor)) {
                    break;
                }

                dato.setSku(valor);
                
                valor = formateaValor(row.getCell(columnaNombre));
                dato.setNombre(valor);

                valor = formateaValor(row.getCell(columnaPiezas));
                valor = valor.replace(",", "");
                dato.setPiezas(valor);

                valor = formateaNumerico(row.getCell(columnaPrecioUnitario));
                dato.setPrecio(valor);

                valor = formateaNumerico(row.getCell(columnaPrecioTotal));
                dato.setPrecioTotal(valor);

                datos.add(dato);
            }
            i++;
        }
        return datos;
    }

    public static String detectLang(String text) {

        //load all languages:
        List<LanguageProfile> languageProfiles = null;
        try {
            languageProfiles = new LanguageProfileReader().readAllBuiltIn();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        //build language detector:
        LanguageDetector languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
                .withProfiles(languageProfiles)
                .build();

        //create a text object factory
        TextObjectFactory textObjectFactory = CommonTextObjectFactories.forDetectingOnLargeText();

        TextObject textObject = textObjectFactory.forText(text);
        com.google.common.base.Optional<LdLocale> lang = languageDetector.detect(textObject);
        LdLocale locale = lang.orNull();
        return locale == null ? null : locale.getLanguage();
    }

    /*
    private List<CargaFacturaDatosVO> extraeHoja(XSSFSheet sheet) {
        int columnaSKU = 3;
        int columnaNombre = 5;
        int columnaPiezas = 7;
        int columnaPrecioUnitario = 8;
        int columnaPrecioTotal = 9;
        List<CargaFacturaDatosVO> datos = new ArrayList<>(0);
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            CargaFacturaDatosVO dato = new CargaFacturaDatosVO();
            String valor = formateaValor(row.getCell(columnaSKU));
            if (StringUtils.isEmpty(valor)) {
                break;
            }
            dato.setSku(valor);

            valor = formateaValor(row.getCell(columnaNombre));
            dato.setNombre(valor);

            valor = formateaValor(row.getCell(columnaPiezas));
            dato.setPiezas(valor);

            valor = formateaValor(row.getCell(columnaPrecioUnitario));
            dato.setPrecio(valor);

            valor = formateaValor(row.getCell(columnaPrecioTotal));
            dato.setPrecioTotal(valor);

            datos.add(dato);

        }
        return datos;
    }*/
    
    
    private String formateaValor(Cell cell) {
        DataFormatter formatter = new DataFormatter();
        String valor = formatter.formatCellValue(cell);
        return valor;
    }

    private String formateaNumerico(Cell cell) {
        DataFormatter formatter = new DataFormatter();
        String valor = formatter.formatCellValue(cell);
        if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
            switch (cell.getCachedFormulaResultType()) {
                case Cell.CELL_TYPE_NUMERIC:
                    valor = String.valueOf(cell.getNumericCellValue());
                    break;
            }
        }
        return valor;
    }

    public Boolean validaOrden(String numeroOrden) {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setNoOrden(numeroOrden);
        confirmacion = confirmacionColombiaDao.obtenerXNumeroOrden(confirmacion);
        if (confirmacion != null) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public void guarda(CargaFacturaVO cargaVO) throws Exception {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setNoOrden(cargaVO.getNumeroOrden());
        confirmacion = confirmacionColombiaDao.obtenerXNumeroOrden(confirmacion);
        if (confirmacion != null) {

        }

        Factura factura = new Factura();
        factura.setFechaRegistro(FechaUtil.creaFechaActualTimestamp());
        factura.setIdConfirmacion(confirmacion.getIdConfirmacion());
        factura.setEstatus('N');
        factura.setFechaFactura(FechaUtil.convierteFechaStringTODate(cargaVO.getFechaFactura()));
        factura.setNoFactura(cargaVO.getNumeroFactura().trim());
        factura.setContenedor(cargaVO.getContenedor().trim());
        factura.setEtd(FechaUtil.convierteFechaStringTODate(cargaVO.getEtd()));
        factura.setEta(FechaUtil.convierteFechaStringTODate(cargaVO.getEta()));
        factura = facturaColombiaDao.guardar(factura);

        guarda(factura, cargaVO.getDatos());

    }

    private void guarda(Factura factura, List<CargaFacturaDatosVO> datos) throws Exception {

        for (CargaFacturaDatosVO dato : datos) {
            FacturaProducto facturaProducto = new FacturaProducto();
            ProductoCompra productoCompra = new ProductoCompra();
            productoCompra.setSku(dato.getSku());

            productoCompra = productoCompraColombiaDao.buscarXSKU(productoCompra);
            if (productoCompra == null) {
                productoCompra = new ProductoCompra();
                productoCompra.setSku(dato.getSku());
                productoCompra.setFechaRegistro(FechaUtil.creaFechaActualTimestamp());
                productoCompra.setDescripcion(dato.getNombre());
                productoCompra = productoCompraColombiaDao.guardar(productoCompra);
            }
            facturaProducto.setIdFactura(factura.getIdFactura());
            facturaProducto.setIdProductoCompra(productoCompra.getIdProductoCompra());

            int piezas = Integer.parseInt(dato.getPiezas());
            double precio = Double.parseDouble(dato.getPrecio());
            double precioTotal = 0;
            if (!NumberUtils.isNumber(dato.getPrecioTotal())) {
                precioTotal = precio * piezas;
            } else {
                precioTotal = Double.parseDouble(dato.getPrecioTotal());
            }
            facturaProducto.setPiezas(piezas);
            facturaProducto.setPrecioUnitario(precio);
            facturaProducto.setPrecioTotal(precioTotal);
            facturaProductoColombiaDao.guardar(facturaProducto);
        }
    }

}
