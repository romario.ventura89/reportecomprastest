package mx.com.miniso.control.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import mx.com.miniso.control.constantes.NombreSheetConfirmacionEnum;
import mx.com.miniso.control.constantes.SociedadMenu;
import mx.com.miniso.control.dao.ConfirmacionDao;
import mx.com.miniso.control.dao.ConfirmacionProductoDao;
import mx.com.miniso.control.dao.ProductoCompraDao;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.dto.ConfirmacionProducto;
import mx.com.miniso.control.dto.ProductoCompra;
import mx.com.miniso.control.ex.ConfirmacionNumeroOrdenExcepcion;
import mx.com.miniso.control.util.FechaUtil;
import mx.com.miniso.control.vo.CargaConfirmacionDatosVO;
import mx.com.miniso.control.vo.CargaConfirmacionVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CargaConfirmacionService {

    final static Logger logger = LoggerFactory.getLogger(CargaConfirmacionService.class);

    public static final String PRODUCT_CODE = "Product code";//0 columnaSKU
    public static final String PRODUCT_NAME = "product name";//2 columnaNombre
    public static final String CARTON_CBM = "Carton CBM";//4 columnaCBM
    public static final String PRICE = "price";//4 columnaPrecio
    public static final String ORDER_QUANTITY = "ordered quantity";//6 columnaPiezasSolicotadas
    public static final String FINAL_SHIPPED_QUANTITY = "final shipped quantity";//7 columnaPiezasEntregadas

    @Autowired
    private ConfirmacionDao confirmacionDao;

    @Autowired
    private ConfirmacionProductoDao confirmacionProductoDao;

    @Autowired
    private ProductoCompraDao productoCompraDao;

    public CargaConfirmacionVO obtenerDatos(CargaConfirmacionVO cargaVO, MultipartFile archivo) throws IOException {
        Workbook wb = null;

        try {
            wb = new XSSFWorkbook(archivo.getInputStream());
        } catch (IOException e) {
            logger.error("Error al convertir el archivo al formato de Excel");
            throw e;
        }

        int hojas = wb.getNumberOfSheets(); //Obtenemos el nmero de hojas que contiene el documento
        System.out.println("Nmero Hojas: " + hojas);

        XSSFSheet sheet;
        Map<String, List<CargaConfirmacionDatosVO>> cargaInformacion = new HashMap<>();
        List<CargaConfirmacionDatosVO> confirmacion;
        String nombreHoja;

        //For que recorre las hojas existentes
        for (int i = 0; i < hojas; i++) {
            nombreHoja = wb.getSheetName(i);

            System.out.println("Hoja #: " + i + " - Nombre: " + nombreHoja); //Muestra la hoja en la estamos y el nombre de esta
            sheet = (XSSFSheet) wb.getSheet(nombreHoja);

            if (sheet != null) {
                confirmacion = extraeHoja(sheet);
                cargaInformacion.put(nombreHoja, confirmacion);
            }

        }

        cargaVO.setCargaInformacion(cargaInformacion);

        /*XSSFSheet sheet = (XSSFSheet) wb.getSheet(NombreSheetConfirmacionEnum.GENERAL.getNombre());
        if (sheet != null) {
            cargaVO.setGeneral(extraeHoja(sheet));
        }

        sheet = (XSSFSheet) wb.getSheet(NombreSheetConfirmacionEnum.PINGSHAN.getNombre());
        if (sheet != null) {
            cargaVO.setPingshan(extraeHoja(sheet));
        }

        sheet = (XSSFSheet) wb.getSheet(NombreSheetConfirmacionEnum.QIANHAI.getNombre());
        if (sheet != null) {
            cargaVO.setQianhai(extraeHoja(sheet));
        }*/
        return cargaVO;
    }

    private List<CargaConfirmacionDatosVO> extraeHoja(XSSFSheet sheet) {
        int columnaSKU = 0;
        int columnaNombre = 2;
        int columnaPrecio = 4;
        int columnaCbm = -1;
        int columnaPiezasSolicitadas = 5;
        int columnaPiezasEntregadas = 6;
        int columnaPrecioTotal = 7;

        List<CargaConfirmacionDatosVO> datos = new ArrayList<>(0);
        int i = 0;
        for (Row row : sheet) {
            if (i == 0) {

                // obtiene el nombre de los encabezados
                for (int col = 0; col < row.getLastCellNum(); col++) {
                    Cell cell = row.getCell(col);

                    if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                        // Can't be this cell - it's empty
                        continue;
                    }

                    if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                        String value = cell.getStringCellValue();

                        if (value.contains(PRODUCT_CODE)) {
                            columnaSKU = cell.getAddress().getColumn();
                        } else if (value.contains(PRODUCT_NAME)) {
                            columnaNombre = cell.getAddress().getColumn();
                        } else if (value.contains(CARTON_CBM)) {
                            columnaCbm = cell.getAddress().getColumn();
                        } else if (value.contains(PRICE)) {
                            columnaPrecio = cell.getAddress().getColumn();
                        } else if (value.contains(ORDER_QUANTITY)) {
                            columnaPiezasSolicitadas = cell.getAddress().getColumn();
                        } else if (value.contains(FINAL_SHIPPED_QUANTITY)) {
                            columnaPiezasEntregadas = cell.getAddress().getColumn();
                        }

                        columnaPrecioTotal = cell.getAddress().getColumn() + 1;
                    }
                }

            } else if (i > 0) {
                CargaConfirmacionDatosVO dato = new CargaConfirmacionDatosVO();

                String valor = formateaValor(row.getCell(columnaSKU));
                if (StringUtils.isEmpty(valor)) {
                    break;
                }
                dato.setSku(valor);

                valor = formateaValor(row.getCell(columnaNombre));
                dato.setNombre(valor);

                valor = formateaValor(row.getCell(columnaPrecio));
                dato.setPrecio(valor);

                if (columnaCbm != -1) {
                    valor = formateaValor(row.getCell(columnaCbm));
                    dato.setCbm(valor);
                }

                valor = formateaValor(row.getCell(columnaPiezasSolicitadas));
                valor = valor.replace(",", "");
                dato.setPiezasSolicitadas(valor);

                valor = formateaValor(row.getCell(columnaPiezasEntregadas));
                valor = valor.replace(",", "");
                dato.setPiezasEntregadas(valor);

                valor = formateaValor(row.getCell(columnaPrecioTotal));
                dato.setPrecioTotal(valor);

                datos.add(dato);
            }

            i++;
        }
        return datos;
    }

    private String formateaValor(Cell cell) {
        DataFormatter formatter = new DataFormatter();
        String valor = formatter.formatCellValue(cell);
        return valor;
    }

    public void guarda(CargaConfirmacionVO cargaVO) throws ConfirmacionNumeroOrdenExcepcion, Exception {
        boolean actualizar = false;
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setNoOrden(cargaVO.getNumeroOrden());
        confirmacion = confirmacionDao.obtenerXNumeroOrden(confirmacion);
        if (confirmacion != null && cargaVO.isRemplazar()) {
            actualizar = true;
        }

        /*int tipoGeneral = 1;
        int tipoQuinhau = 3;
        int tipoPingshen = 2;*/
        if (actualizar && confirmacion != null) {
            confirmacion.setEstatus('A');
            confirmacion.setFechaConfirmacion(FechaUtil.convierteFechaStringTODate(cargaVO.getFechaConfirmacion()));
            confirmacionDao.actualizar(confirmacion);
            confirmacionProductoDao.eliminaXConfirmacion(confirmacion);
        } else {
            confirmacion = new Confirmacion();
            confirmacion.setEstatus('N');
            confirmacion.setFechaRegistro(FechaUtil.creaFechaActualTimestamp());
            confirmacion.setIdSociedad(SociedadMenu.MEXICO_CHINA.getId());
            confirmacion.setNoOrden(cargaVO.getNumeroOrden());
            confirmacion.setFechaConfirmacion(FechaUtil.convierteFechaStringTODate(cargaVO.getFechaConfirmacion()));
            confirmacion = confirmacionDao.guardar(confirmacion);
        }

        for (Map.Entry<String , List<CargaConfirmacionDatosVO>> entry : cargaVO.getCargaInformacion().entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
            guarda(confirmacion, entry.getValue(), entry.getKey());
        }

        /*
        if (!cargaVO.getGeneral().isEmpty()) {
            guarda(confirmacion, cargaVO.getGeneral(), tipoGeneral);
        }

        if (!cargaVO.getQianhai().isEmpty()) {
            guarda(confirmacion, cargaVO.getQianhai(), tipoQuinhau);
        }

        if (!cargaVO.getPingshan().isEmpty()) {
            guarda(confirmacion, cargaVO.getPingshan(), tipoPingshen);
        }*/

    }

    private void guarda(Confirmacion confirmacion, List<CargaConfirmacionDatosVO> datos, String tipoOrder) throws Exception {

        for (CargaConfirmacionDatosVO dato : datos) {
            ConfirmacionProducto cp = new ConfirmacionProducto();
            ProductoCompra productoCompra = new ProductoCompra();
            productoCompra.setSku(dato.getSku());

            productoCompra = productoCompraDao.buscarXSKU(productoCompra);
            if (productoCompra == null) {
                productoCompra = new ProductoCompra();
                productoCompra.setSku(dato.getSku());
                productoCompra.setFechaRegistro(FechaUtil.creaFechaActualTimestamp());
                productoCompra.setDescripcion(dato.getNombre());
                productoCompra = productoCompraDao.guardar(productoCompra);
            }
            cp.setIdConfirmacion(confirmacion.getIdConfirmacion());
            cp.setIdProductoCompra(productoCompra.getIdProductoCompra());

            if (!NumberUtils.isNumber(dato.getPiezasEntregadas())) {
                cp.setNotOrder(true);
                cp.setPiezasEntregadas(0);
                cp.setPrecioTotal(0);
            } else {
                int piezasEntregadas = Integer.parseInt(dato.getPiezasEntregadas());
                double precio = convertDouble(dato.getPrecio());
                double precioTotal = 0;
                if (!NumberUtils.isNumber(dato.getPrecioTotal())) {
                    precioTotal = precio * piezasEntregadas;
                } else {
                    precioTotal = Double.parseDouble(dato.getPrecioTotal());
                }

                cp.setPiezasEntregadas(piezasEntregadas);
                cp.setPrecioUnitario(precio);
                cp.setPrecioTotal(precioTotal);
            }

            if (dato.getCbm() != null) {
                double cbm = convertDouble(dato.getCbm());
                cp.setCbm(cbm);
            }

            int piezasSolicitadas = Integer.parseInt(dato.getPiezasSolicitadas());
            cp.setPiezasSolicitadas(piezasSolicitadas);
            cp.setTipoOrden(tipoOrder);

            confirmacionProductoDao.guardar(cp);
        }
    }

    private Double convertDouble(String valor) {
        String convert = valor.replace(',', '.');
        double value;
        try {
            value = Double.parseDouble(convert);
        } catch (Exception exception) {
            value = 0;
            exception.printStackTrace();
        }
        return value;
    }

    public void comprobarNoOrden(CargaConfirmacionVO cargaVO) throws ConfirmacionNumeroOrdenExcepcion {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setNoOrden(cargaVO.getNumeroOrden());
        confirmacion = confirmacionDao.obtenerXNumeroOrden(confirmacion);
        if (confirmacion != null && !cargaVO.isRemplazar()) {
            throw new ConfirmacionNumeroOrdenExcepcion(confirmacion.getNoOrden());
        }
    }

}
