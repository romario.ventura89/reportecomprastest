package mx.com.miniso.control.service;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.control.dao.FacturaDao;
import mx.com.miniso.control.dao.FacturaProductoDao;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.vo.FacturaDetalleVO;
import mx.com.miniso.control.vo.FacturaVO;
import mx.com.miniso.control.vo.FiltrosFacturaVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FacturaService {

    @Autowired
    private FacturaProductoDao facturaProductoDao;

    @Autowired
    private FacturaDao facturaDao;

    @Autowired
    private HistoricoService historicoService;

    public List<FacturaVO> buscarFacturas(FiltrosFacturaVO filtrosFacturaVO) throws Exception {
        List<FacturaVO> facturas = new ArrayList<>(0);
        if (filtrosFacturaVO == null) {
            return facturas;
        }

        facturas = facturaDao.obtenerXParametros(filtrosFacturaVO);
        for (FacturaVO factura : facturas) {
            factura = facturaDao.obtenTotalesMisFacturas(factura);
        }

        return facturas;
    }

    public List<FacturaDetalleVO> obtenerDetalle(FacturaVO facturaVO) {
        List<FacturaDetalleVO> detalles = facturaDao.obtenDetalleMisFacturas(facturaVO);
        return detalles;
    }

    public Factura buscarXId(String idFactura) {
        Factura factura = new Factura();
        factura.setIdFactura(Long.parseLong(idFactura));
        factura = facturaDao.obtenerXID(factura);
        return factura;
    }

    public void eliminar(FacturaVO facturaVO) {
        Factura factura = new Factura();
        factura.setIdFactura(Long.parseLong(facturaVO.getIdFactura()));
        factura = facturaDao.obtenerXID(factura);
        historicoService.copiaFacturasAHistorico(factura);
        facturaProductoDao.eliminaXFactura(factura);
        facturaDao.borrar(factura);
    }

    public boolean existeXCodigo(String codigo) {
        Factura factura = new Factura();
        factura.setNoFactura(codigo);
        factura = facturaDao.obtenerXNo(factura);
        return factura != null;
    }

}
