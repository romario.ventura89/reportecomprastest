package mx.com.miniso.control.service;

import java.util.List;
import mx.com.miniso.control.dao.FacturaProductoDao;
import mx.com.miniso.control.dao.HistoricoFacturaDao;
import mx.com.miniso.control.dao.HistoricoFacturaProductoDao;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.dto.FacturaProducto;
import mx.com.miniso.control.dto.HistoricoFactura;
import mx.com.miniso.control.dto.HistoricoFacturaProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoricoService {

    @Autowired
    private HistoricoFacturaDao historicoFacturaDao;

    @Autowired
    private HistoricoFacturaProductoDao historicoFacturaProductoDao;

    @Autowired
    private FacturaProductoDao facturaProductoDao;
    
     public void copiaFacturasAHistorico(Factura factura) {
         HistoricoFactura historicoFactura = new HistoricoFactura(factura);
            historicoFactura = historicoFacturaDao.guardar(historicoFactura);
            List<FacturaProducto> listaFacturaProducto = facturaProductoDao.obtenerProductosIdFactura(factura.getIdFactura());
            if (listaFacturaProducto != null) {
                for (FacturaProducto facturaProducto : listaFacturaProducto) {
                    HistoricoFacturaProducto historicoProducto = new HistoricoFacturaProducto(facturaProducto, historicoFactura);
                    historicoFacturaProductoDao.guardar(historicoProducto);
                }
            }
     }

    public void copiaFacturasAHistorico(List<Factura> facturas) {
        for (Factura factura : facturas) {
            copiaFacturasAHistorico(factura);
        }

    }
}
