package mx.com.miniso.control.service.colombia;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.control.dao.colombia.ConfirmacionColombiaDao;
import mx.com.miniso.control.dao.colombia.ConfirmacionProductoColombiaDao;
import mx.com.miniso.control.dao.colombia.FacturaColombiaDao;
import mx.com.miniso.control.dao.colombia.FacturaProductoColombiaDao;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.vo.ConfirmacionDetalleVO;
import mx.com.miniso.control.vo.ConfirmacionesVO;
import mx.com.miniso.control.vo.FiltrosConfirmacionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfirmacionColombiaService {

    @Autowired
    private ConfirmacionColombiaDao confirmacionColombiaDao;

    @Autowired
    private ConfirmacionProductoColombiaDao confirmacionProductoColombiaDao;

    @Autowired
    private FacturaProductoColombiaDao facturaProductoColombiaDao;

    @Autowired
    private FacturaColombiaDao facturaColombiaDao;
    
    @Autowired
    private HistoricoColombiaService historicoColombiaService;

   

    public List<ConfirmacionesVO> buscarConfirmaciones(FiltrosConfirmacionVO filtrosConfirmacionVO) throws Exception {
        List<ConfirmacionesVO> confirmaciones = new ArrayList<>(0);
        if (filtrosConfirmacionVO == null) {
            return confirmaciones;
        }

        confirmaciones = confirmacionColombiaDao.obtenerXParametros(filtrosConfirmacionVO);
        for (ConfirmacionesVO confirmacion : confirmaciones) {
            confirmacion = confirmacionColombiaDao.obtenTotalesMisConfirmaciones(confirmacion);
        }

        return confirmaciones;
    }

    public List<ConfirmacionDetalleVO> obtenerDetalle(ConfirmacionesVO confirmacionVO) {
        List<ConfirmacionDetalleVO> detalles = confirmacionColombiaDao.obtenDetalleMisConfirmaciones(confirmacionVO);
        return detalles;
    }

    public Confirmacion buscarNumeroOrden(String idConfirmacion) {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setIdConfirmacion(Long.parseLong(idConfirmacion));
        confirmacion = confirmacionColombiaDao.obtenerXID(confirmacion);
        return confirmacion;
    }

    public boolean existeNoOrden(String noOrden) {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setNoOrden(noOrden);
        confirmacion = confirmacionColombiaDao.obtenerXNumeroOrden(confirmacion);
        return confirmacion != null;
    }

    public void eliminar(ConfirmacionesVO confirmacionVO) {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setIdConfirmacion(Long.parseLong(confirmacionVO.getIdConfirmacion()));
        confirmacion = confirmacionColombiaDao.obtenerXID(confirmacion);

        confirmacionProductoColombiaDao.eliminaXConfirmacionTipo(confirmacion, confirmacionVO.getTipoOrden());

        int cuentaProductos = confirmacionProductoColombiaDao.cuentaProductos(confirmacion);

        if (0 == cuentaProductos) {
            List<Factura> facturas = facturaColombiaDao.obtenerXConfirmacion(confirmacion);
            historicoColombiaService.copiaFacturasAHistorico(facturas);
            eliminaFacturas(facturas);
            confirmacionColombiaDao.borrar(confirmacion);
        }
    }



    private void eliminaFacturas(List<Factura> facturas) {

        for (Factura factura : facturas) {
            facturaProductoColombiaDao.eliminaXFactura(factura);
            facturaColombiaDao.borrar(factura);
        }

    }

}
