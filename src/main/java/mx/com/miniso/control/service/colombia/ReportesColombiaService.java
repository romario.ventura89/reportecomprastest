
package mx.com.miniso.control.service.colombia;

import java.util.List;
import mx.com.miniso.control.dao.colombia.ReportesColombiaDao;
import mx.com.miniso.control.vo.ReporteConfirmacionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class ReportesColombiaService {
    
    @Autowired
    private ReportesColombiaDao reportesColombiaDao;
    
    public List<ReporteConfirmacionVO> Confirmacion(){
        List<ReporteConfirmacionVO> reporteConfirmacion = reportesColombiaDao.buscarTodos();
        return reporteConfirmacion;
    }
    
    
}
