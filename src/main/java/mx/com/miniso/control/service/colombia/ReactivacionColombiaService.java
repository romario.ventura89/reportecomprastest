package mx.com.miniso.control.service.colombia;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.control.dao.colombia.ConfirmacionColombiaDao;
import mx.com.miniso.control.dao.colombia.FacturaColombiaDao;
import mx.com.miniso.control.dao.colombia.FacturaProductoColombiaDao;
import mx.com.miniso.control.dao.colombia.HistoricoFacturaColombiaDao;
import mx.com.miniso.control.dao.colombia.HistoricoFacturaProductoColombiaDao;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.dto.FacturaProducto;
import mx.com.miniso.control.dto.HistoricoFactura;
import mx.com.miniso.control.dto.HistoricoFacturaProducto;
import mx.com.miniso.control.ex.FacturacionException;
import mx.com.miniso.control.vo.FacturaDetalleVO;
import mx.com.miniso.control.vo.FacturaVO;
import mx.com.miniso.control.vo.FiltrosFacturaVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class ReactivacionColombiaService {

    @Autowired
    private ConfirmacionColombiaDao confirmacionColombiaDao;

    @Autowired
    private FacturaColombiaDao facturaColombiaDao;

    @Autowired
    private FacturaProductoColombiaDao facturaProductoColombiaDao;

    @Autowired
    private HistoricoFacturaColombiaDao historicoFacturaColombiaDao;

    @Autowired
    private HistoricoFacturaProductoColombiaDao historicoFacturaProductoColombiaDao;

    public List<FacturaVO> buscarFacturas(FiltrosFacturaVO filtrosFacturaVO) throws Exception {
        List<FacturaVO> facturas = new ArrayList<>(0);
        if (filtrosFacturaVO == null) {
            return facturas;
        }

        facturas = historicoFacturaColombiaDao.obtenerXParametros(filtrosFacturaVO);
        for (FacturaVO factura : facturas) {
            factura = historicoFacturaColombiaDao.obtenTotalesMisFacturas(factura);
        }

        return facturas;
    }

    public List<FacturaDetalleVO> obtenerDetalle(FacturaVO facturaVO) {
        List<FacturaDetalleVO> detalles = historicoFacturaColombiaDao.obtenDetalleMisFacturas(facturaVO);
        return detalles;
    }

    public HistoricoFactura buscarXId(String id) {
        return historicoFacturaColombiaDao.buscarXId(id);
    }

    public void reactivar(FacturaVO facturaVO) throws FacturacionException {
        validaNumeroOrdenNula(facturaVO);
        Confirmacion confirmacion = validaConfirmacionExistente(facturaVO);
        HistoricoFactura historicoFactura = obtenerHistoricos(facturaVO);
        if (historicoFactura == null) {
            return;
        }

        List<HistoricoFacturaProducto> listaHistoricoProductos = obtenerHistoricoProductos(historicoFactura);
        if (listaHistoricoProductos == null) {
            return;
        }

        Factura factura = guardaFactura(historicoFactura, confirmacion);
        guardaProductos(factura, listaHistoricoProductos);

        eliminaHistoricos(historicoFactura, listaHistoricoProductos);

    }

    private void validaNumeroOrdenNula(FacturaVO factura) throws FacturacionException {
        if (StringUtils.isEmpty(factura.getNumeroOrden())) {
            throw new FacturacionException("Favor de proporcionar el número de orden");
        }
    }

    private Confirmacion validaConfirmacionExistente(FacturaVO factura) throws FacturacionException {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setNoOrden(factura.getNumeroOrden());
        confirmacion = confirmacionColombiaDao.obtenerXNumeroOrden(confirmacion);
        if (confirmacion == null) {
            throw new FacturacionException("El número de orden proporcionado no se encuentra registrado");
        }
        return confirmacion;
    }

    private HistoricoFactura obtenerHistoricos(FacturaVO facturaVO) {
        return historicoFacturaColombiaDao.buscarXId(facturaVO.getIdFactura());
    }

    private List<HistoricoFacturaProducto> obtenerHistoricoProductos(HistoricoFactura historicoFactura) {
        return historicoFacturaProductoColombiaDao.buscarXIdFactura(historicoFactura.getIdFactura());
    }

    private Factura guardaFactura(HistoricoFactura historicoFactura, Confirmacion confirmacion) {
        Factura factura = new Factura(historicoFactura, confirmacion);
        factura = facturaColombiaDao.guardar(factura);
        return factura;
    }

    private void guardaProductos(Factura factura, List<HistoricoFacturaProducto> listaHistoricoProductos) {
        for (HistoricoFacturaProducto historicoProducto : listaHistoricoProductos) {
            FacturaProducto facturaProducto = new FacturaProducto(factura, historicoProducto);
            facturaProductoColombiaDao.guardar(facturaProducto);
        }
    }

    private void eliminaHistoricos(HistoricoFactura historicoFactura, List<HistoricoFacturaProducto> listaHistoricoProductos) {

        for (HistoricoFacturaProducto historicoProducto : listaHistoricoProductos) {
            historicoFacturaProductoColombiaDao.elimina(historicoProducto);
        }
        historicoFacturaColombiaDao.elimina(historicoFactura);
    }

}
