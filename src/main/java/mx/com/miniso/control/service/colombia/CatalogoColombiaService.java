/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miniso.control.service.colombia;

import java.util.List;
import mx.com.miniso.control.constantes.CatalogoEnum;
import mx.com.miniso.control.dao.colombia.SociedadColombiaDao;
import mx.com.miniso.control.dao.colombia.TipoOrdenColombiaDao;
import mx.com.miniso.control.dto.Sociedad;
import mx.com.miniso.control.dto.TipoOrden;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CatalogoColombiaService {
    
    final static Log logger = LogFactory.getLog(CatalogoColombiaService.class);
    
    @Autowired    
    private SociedadColombiaDao sociedadColombiaDao;
    
    @Autowired
    private TipoOrdenColombiaDao tipoOrdenColombiaDao;
    
    private List<Sociedad> sociedades;
    private List<TipoOrden> tipoOrdenes;
    
     public void verificaCatalogo(CatalogoEnum catalogoEnum) {
        if (catalogoEnum.equals(CatalogoEnum.CAT_SOCIEDAD)) {
            if (sociedades == null) {
                sociedades = sociedadColombiaDao.buscarTodos();
            }
        } else if (catalogoEnum.equals(CatalogoEnum.CAT_TIPO_ORDEN)) {
            if (tipoOrdenes == null) {
                tipoOrdenes = tipoOrdenColombiaDao.buscarTodos();
            }
        }
    }
     
     public List<Sociedad> getCatalogoSociedades() {
        verificaCatalogo(CatalogoEnum.CAT_SOCIEDAD);
        return sociedades;
    }
    
     public Sociedad buscarSociedad(Sociedad sociedad) {
        verificaCatalogo(CatalogoEnum.CAT_SOCIEDAD);
        for (Sociedad catSociedadIter : sociedades) {
            if (catSociedadIter.equals(sociedad)) {
                return catSociedadIter;
            }
            
        }
        return null;
    }   
     public Sociedad buscarCatSociedad(long idSociedad) {
        Sociedad sociedad = new Sociedad();
        sociedad.setIdSociedad(idSociedad);
        return buscarSociedad(sociedad);
    }
     
     public List<TipoOrden> getCatalogoTipoOrdenes(){
        verificaCatalogo(CatalogoEnum.CAT_TIPO_ORDEN);
        return tipoOrdenes;
    }
    
     public TipoOrden buscarTipoOrden(TipoOrden tipoOrden) {
        verificaCatalogo(CatalogoEnum.CAT_TIPO_ORDEN);
        for (TipoOrden catTipoOrdenIter : tipoOrdenes) {
            if (catTipoOrdenIter.equals(tipoOrden)) {
                return catTipoOrdenIter;
            }
            
        }
        return null;
    }   
     public TipoOrden buscarCatTipoOrden(long idTipoOrden) {
        TipoOrden tipoOrden = new TipoOrden();
        tipoOrden.setIdTipoOrden(idTipoOrden);
        return buscarTipoOrden(tipoOrden);
    }
     
}
