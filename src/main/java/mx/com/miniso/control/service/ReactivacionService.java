package mx.com.miniso.control.service;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.control.dao.ConfirmacionDao;
import mx.com.miniso.control.dao.FacturaDao;
import mx.com.miniso.control.dao.FacturaProductoDao;
import mx.com.miniso.control.dao.HistoricoFacturaDao;
import mx.com.miniso.control.dao.HistoricoFacturaProductoDao;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.dto.FacturaProducto;
import mx.com.miniso.control.dto.HistoricoFactura;
import mx.com.miniso.control.dto.HistoricoFacturaProducto;
import mx.com.miniso.control.ex.FacturacionException;
import mx.com.miniso.control.vo.FacturaDetalleVO;
import mx.com.miniso.control.vo.FacturaVO;
import mx.com.miniso.control.vo.FiltrosFacturaVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class ReactivacionService {

    @Autowired
    private ConfirmacionDao confirmacionDao;

    @Autowired
    private FacturaDao facturaDao;

    @Autowired
    private FacturaProductoDao facturaProductoDao;

    @Autowired
    private HistoricoFacturaDao historicoFacturaDao;

    @Autowired
    private HistoricoFacturaProductoDao historicoFacturaProductoDao;

    public List<FacturaVO> buscarFacturas(FiltrosFacturaVO filtrosFacturaVO) throws Exception {
        List<FacturaVO> facturas = new ArrayList<>(0);
        if (filtrosFacturaVO == null) {
            return facturas;
        }

        facturas = historicoFacturaDao.obtenerXParametros(filtrosFacturaVO);
        for (FacturaVO factura : facturas) {
            factura = historicoFacturaDao.obtenTotalesMisFacturas(factura);
        }

        return facturas;
    }

    public List<FacturaDetalleVO> obtenerDetalle(FacturaVO facturaVO) {
        List<FacturaDetalleVO> detalles = historicoFacturaDao.obtenDetalleMisFacturas(facturaVO);
        return detalles;
    }

    public HistoricoFactura buscarXId(String id) {
        return historicoFacturaDao.buscarXId(id);
    }

    public void reactivar(FacturaVO facturaVO) throws FacturacionException {
        validaNumeroOrdenNula(facturaVO);
        Confirmacion confirmacion = validaConfirmacionExistente(facturaVO);
        HistoricoFactura historicoFactura = obtenerHistoricos(facturaVO);
        if (historicoFactura == null) {
            return;
        }

        List<HistoricoFacturaProducto> listaHistoricoProductos = obtenerHistoricoProductos(historicoFactura);
        if (listaHistoricoProductos == null) {
            return;
        }

        Factura factura = guardaFactura(historicoFactura, confirmacion);
        guardaProductos(factura, listaHistoricoProductos);

        eliminaHistoricos(historicoFactura, listaHistoricoProductos);

    }

    private void validaNumeroOrdenNula(FacturaVO factura) throws FacturacionException {
        if (StringUtils.isEmpty(factura.getNumeroOrden())) {
            throw new FacturacionException("Favor de proporcionar el número de orden");
        }
    }

    private Confirmacion validaConfirmacionExistente(FacturaVO factura) throws FacturacionException {
        Confirmacion confirmacion = new Confirmacion();
        confirmacion.setNoOrden(factura.getNumeroOrden());
        confirmacion = confirmacionDao.obtenerXNumeroOrden(confirmacion);
        if (confirmacion == null) {
            throw new FacturacionException("El número de orden proporcionado no se encuentra registrado");
        }
        return confirmacion;
    }

    private HistoricoFactura obtenerHistoricos(FacturaVO facturaVO) {
        return historicoFacturaDao.buscarXId(facturaVO.getIdFactura());
    }

    private List<HistoricoFacturaProducto> obtenerHistoricoProductos(HistoricoFactura historicoFactura) {
        return historicoFacturaProductoDao.buscarXIdFactura(historicoFactura.getIdFactura());
    }

    private Factura guardaFactura(HistoricoFactura historicoFactura, Confirmacion confirmacion) {
        Factura factura = new Factura(historicoFactura, confirmacion);
        factura = facturaDao.guardar(factura);
        return factura;
    }

    private void guardaProductos(Factura factura, List<HistoricoFacturaProducto> listaHistoricoProductos) {
        for (HistoricoFacturaProducto historicoProducto : listaHistoricoProductos) {
            FacturaProducto facturaProducto = new FacturaProducto(factura, historicoProducto);
            facturaProductoDao.guardar(facturaProducto);
        }
    }

    private void eliminaHistoricos(HistoricoFactura historicoFactura, List<HistoricoFacturaProducto> listaHistoricoProductos) {

        for (HistoricoFacturaProducto historicoProducto : listaHistoricoProductos) {
            historicoFacturaProductoDao.elimina(historicoProducto);
        }
        historicoFacturaDao.elimina(historicoFactura);
    }

}
