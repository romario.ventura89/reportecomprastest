package mx.com.miniso.control.service;

import mx.com.miniso.control.dao.UsuarioDao;
import mx.com.miniso.control.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
        
    @Autowired
    private UsuarioDao usuarioDao;

    public Usuario obtenerUsuarioXId(Usuario usuario) throws Exception {

        return usuarioDao.obtenerUsuarioXId(usuario);
    }
    
    public Usuario obtenerUsuarioXuserName(Usuario usuario) throws Exception {

        return usuarioDao.obtenerUsuarioXuserName(usuario);
    }
    
    
}
