package mx.com.miniso.control.excel;

import mx.com.miniso.control.vo.ContenedorVO;
import mx.com.miniso.control.vo.FaltanteOrderVO;
import mx.com.miniso.control.vo.OrderVO;
import mx.com.miniso.control.vo.PedidoOrdenVO;
import mx.com.miniso.control.vo.PedidosVO;

import mx.com.miniso.control.vo.ReportePedidosVO;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

public class GeneradorReportePedidos extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorReportePedidos.class);

    private final ReportePedidosVO reportePedido;
    private final String SKU = "SKU";
    private final String PRODUCTNAME ="PRODUCT NAME";
    private final String ORDER_NO = "ORDER NO";
    private final String ORDER_MISSING = "ORDER MISSING PIECES";
    private final String PIECES_CONFIRMED = "PIECES CONFIRMED";
    private final String TOTAL_INVOICE = "TOTAL INVOICE";
    private final String DIFFERENCE = "DIFFERENCE";

    public GeneradorReportePedidos(ReportePedidosVO reportePedido) {
        super("PEDIDOS ");
        this.reportePedido = reportePedido;

    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet generalSheet = reporte.createSheet("PEDIDOS");

        generalSheet = agregaTitulo(generalSheet);
        int indexGeneral = 1;
        for (int j = 0; j < reportePedido.getPedidos().size(); j++) {

            PedidosVO pedido = reportePedido.getPedidos().get(j);
            agregaDatos(pedido, generalSheet, indexGeneral++, j);

        }

        autoSize(generalSheet);
    }

    private void autoSize(SXSSFSheet sheet) {

    }

    private SXSSFSheet agregaTitulo(SXSSFSheet sheet) {

        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(SKU);

        cell = row.createCell(i++);
        cell.setCellValue(PRODUCTNAME);
        
        for (OrderVO order : reportePedido.getOrdenes()) {
            cell = row.createCell(i++);
            cell.setCellValue(order.getNoOrden());
        }

        cell = row.createCell(i++);
        cell.setCellValue(ORDER_MISSING);

        cell = row.createCell(i++);
        cell.setCellValue(PIECES_CONFIRMED);
        cell = row.createCell(i++);
        cell.setCellValue(TOTAL_INVOICE);
        cell = row.createCell(i++);
        cell.setCellValue(DIFFERENCE);
        aplicarAutoSize(sheet, i);
        return sheet;
    }

    @Override
    protected void agregarEncabezados() {

    }

    private void agregaDatos(PedidosVO pedidoVO, SXSSFSheet sheet, int i, int indicePedido) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getSku());

        cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getProductName());
        
        for (PedidoOrdenVO pedidoOrdenVO : pedidoVO.getPedidosOrdenes()) {
            cell = row.createCell(j++);
            cell.setCellValue(pedidoOrdenVO.getPiezas());
        }

        for (ContenedorVO contenedor : pedidoVO.getContenedores()) {
            cell = row.createCell(j++);
            cell.setCellValue(contenedor.getPiezas());
        }

        String textoFaltantes = "-";
        if (pedidoVO.getFaltantes().size() != 0) {
            textoFaltantes = "";
            for (FaltanteOrderVO faltante : pedidoVO.getFaltantes()) {
                textoFaltantes += faltante.getNoOrden() + ": " + faltante.getFaltantes() + " \r\n";
            }
        }

        cell = row.createCell(j++);
        cell.setCellValue(new XSSFRichTextString(textoFaltantes));
        CellStyle style = reporte.createCellStyle();
        style.setWrapText(true);
        cell.setCellStyle(style);

        cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getPiezasConfirmadas());

        cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getTotalFacturas());

        cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getNoEnviados());
        agregaColorDiferencia(pedidoVO.getNoEnviados(), cell);
    }
    private void agregaColorDiferencia(String noEnviados, Cell cell) {
        String valor = noEnviados.replace(",", "");
        if (NumberUtils.isNumber(valor)) {
            CellStyle style = reporte.createCellStyle();
            int diferencia = Integer.parseInt(valor);
            if (diferencia > 0) {
                style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            } else if (diferencia == 0) {
                style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            } else {
                style.setFillForegroundColor(IndexedColors.RED.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            }
             cell.setCellStyle(style);
        }
       
    }

}
