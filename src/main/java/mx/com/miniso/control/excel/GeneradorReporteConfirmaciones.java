package mx.com.miniso.control.excel;

import java.util.List;
import mx.com.miniso.control.vo.ConfirmacionDetalleVO;
import mx.com.miniso.control.vo.ReporteConfirmacionVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class GeneradorReporteConfirmaciones extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorReporteConfirmaciones.class);

    private final List<ReporteConfirmacionVO> detalles;
    private final String SKU = "SKU";
    private final String PRODUCTNAME ="PRODUCT NAME";
    private final String CONFIRMACION = "CONFIRMACIÓN";
    private final String FACTURAS = "FACTURAS";


    public GeneradorReporteConfirmaciones(List<ReporteConfirmacionVO> detalles) {
        super("Confirmacion ");
        this.detalles = detalles;
    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {
        SXSSFSheet generalSheet = reporte.createSheet("Confirmaciones");

        generalSheet = agregaTitulo(generalSheet);
        int indexGeneral = 1;
        for (ReporteConfirmacionVO detalle : detalles) {
                agregaDatos(detalle, generalSheet, indexGeneral++);
        }
        autoSize(generalSheet);
    }

    private void autoSize(SXSSFSheet sheet) {
        
    }

    private SXSSFSheet agregaTitulo(SXSSFSheet sheet) {

        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(SKU);
        
        cell = row.createCell(i++);
        cell.setCellValue(PRODUCTNAME);

        cell = row.createCell(i++);
        cell.setCellValue(CONFIRMACION);

        cell = row.createCell(i++);
        cell.setCellValue(FACTURAS);
        aplicarAutoSize(sheet, i);
        return sheet;
    }

    @Override
    protected void agregarEncabezados() {

    }

    private void agregaDatos(ReporteConfirmacionVO detalle, SXSSFSheet sheet, int i) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(detalle.getSku());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getProductName());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getConfirmacion());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getFacturas());

    }
}
