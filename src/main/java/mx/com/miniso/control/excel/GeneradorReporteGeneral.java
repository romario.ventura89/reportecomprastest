package mx.com.miniso.control.excel;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.com.miniso.control.vo.ComodatoVO;
import mx.com.miniso.control.vo.ContainerVO;
import mx.com.miniso.control.vo.ContenedorVO;
import mx.com.miniso.control.vo.FaltanteOrderVO;
import mx.com.miniso.control.vo.OrderVO;
import mx.com.miniso.control.vo.PedidoOrdenVO;
import mx.com.miniso.control.vo.PedidosVO;

import mx.com.miniso.control.vo.ReporteComodatoVO;
import mx.com.miniso.control.vo.ReporteConfirmacionVO;
import mx.com.miniso.control.vo.ReportePedidosVO;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

public class GeneradorReporteGeneral extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorReporteGeneral.class);

    private final ReporteComodatoVO reporteComodato;
    private final List<ReporteConfirmacionVO> reportesConfirmacion;
    private final ReportePedidosVO reportePedidos;
    private final String SKU = "SKU";
    private final String PRODUCTNAME ="PRODUCT NAME";
    private final String ORDER_NO = "ORDER NO";
    private final String ORDER_MISSING = "ORDER MISSING PIECES";
    private final String PIECES_CONFIRMED = "PIECES CONFIRMED";
    private final String TOTAL_INVOICE = "TOTAL INVOICE";
    private final String DIFFERENCE = "DIFFERENCE";
    private final String CONFIRMACION = "CONFIRMACIÓN";
    private final String FACTURAS = "FACTURAS";

    public GeneradorReporteGeneral(List<ReporteConfirmacionVO> reportesConfirmacion, ReporteComodatoVO reporteComodato, ReportePedidosVO reportePedidos) {
        super("GENERAL ");
        this.reportesConfirmacion = reportesConfirmacion;
        this.reporteComodato = reporteComodato;
        this.reportePedidos = reportePedidos;

    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet sheet = reporte.createSheet("CONFIRMACION GENERAL");
        creaConfirmacion(sheet);

        sheet = reporte.createSheet("COMODATO");
        creaComodato(sheet);

        sheet = reporte.createSheet("CONFIRMACION DETALLADA");

        creaPedidos(sheet);

    }

    @Override
    protected void agregarEncabezados() {

    }

    private void creaConfirmacion(SXSSFSheet sheet) {
        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(SKU);
        
        cell = row.createCell(i++);
        cell.setCellValue(PRODUCTNAME);
        
        cell = row.createCell(i++);
        cell.setCellValue(CONFIRMACION);

        cell = row.createCell(i++);
        cell.setCellValue(FACTURAS);

        aplicarAutoSize(sheet, i);

        int indexGeneral = 1;
        for (ReporteConfirmacionVO detalle : reportesConfirmacion) {
            int j = 0;
            row = sheet.createRow(indexGeneral++);
            cell = row.createCell(j++);
            cell.setCellValue(detalle.getSku());
            
            cell = row.createCell(j++);
            cell.setCellValue(detalle.getProductName());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getConfirmacion());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getFacturas());
        }

    }

    private void creaComodato(SXSSFSheet sheet) {
        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(SKU);
        
        cell = row.createCell(i++);
        cell.setCellValue(PRODUCTNAME);

        cell = row.createCell(i++);
        cell.setCellValue(ORDER_NO);

        cell = row.createCell(i++);
        cell.setCellValue(PIECES_CONFIRMED);

        for (ContainerVO contenedor : reporteComodato.getContenedores()) {
            cell = row.createCell(i++);
            cell.setCellValue(contenedor.getNombreContenedor());
        }
        cell = row.createCell(i++);
        cell.setCellValue(TOTAL_INVOICE);
        cell = row.createCell(i++);
        cell.setCellValue(DIFFERENCE);
        aplicarAutoSize(sheet, i);

        int indexGeneral = 1;
        for (ComodatoVO comodato : reporteComodato.getComodatos()) {

            int j = 0;
            row = sheet.createRow(indexGeneral++);
            cell = row.createCell(j++);
            cell.setCellValue(comodato.getSku());
            
            cell = row.createCell(j++);
            cell.setCellValue(comodato.getProductName());

            cell = row.createCell(j++);
            cell.setCellValue(comodato.getNumeroOrden());

            cell = row.createCell(j++);
            cell.setCellValue(comodato.getPiezasConfirmadas());

            for (ContenedorVO contenedor : comodato.getContenedores()) {
                cell = row.createCell(j++);
                cell.setCellValue(contenedor.getPiezas());
            }

            cell = row.createCell(j++);
            cell.setCellValue(comodato.getTotalFacturas());

            cell = row.createCell(j++);
            cell.setCellValue(comodato.getNoEnviados());
            agregaColorDiferencia(comodato.getNoEnviados(), cell);

        }
    }

    private void creaPedidos(SXSSFSheet sheet) {
        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(SKU);
        
        cell = row.createCell(i++);
        cell.setCellValue(PRODUCTNAME);

        for (OrderVO order : reportePedidos.getOrdenes()) {
            cell = row.createCell(i++);
            cell.setCellValue(order.getNoOrden());

        }

        cell = row.createCell(i++);
        cell.setCellValue(ORDER_MISSING);

        cell = row.createCell(i++);
        cell.setCellValue(PIECES_CONFIRMED);
        cell = row.createCell(i++);
        cell.setCellValue(TOTAL_INVOICE);
        cell = row.createCell(i++);
        cell.setCellValue(DIFFERENCE);

        aplicarAutoSize(sheet, i);
        int indexGeneral = 1;
        for (int j = 0; j < reportePedidos.getPedidos().size(); j++) {

            PedidosVO pedido = reportePedidos.getPedidos().get(j);
            agregaDatosPedido(pedido, sheet, indexGeneral++, j);

        }

    }

    private void agregaDatosPedido(PedidosVO pedidoVO, SXSSFSheet sheet, int i, int indicePedido) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getSku());
        
        cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getProductName());
        
        for (PedidoOrdenVO pedidoOrdenVO : pedidoVO.getPedidosOrdenes()) {
            cell = row.createCell(j++);
            cell.setCellValue(pedidoOrdenVO.getPiezas());
        }

        for (ContenedorVO contenedor : pedidoVO.getContenedores()) {
            cell = row.createCell(j++);
            cell.setCellValue(contenedor.getPiezas());
        }

        String textoFaltantes = "-";
        if (pedidoVO.getFaltantes().size() != 0) {
            textoFaltantes = "";
            for (FaltanteOrderVO faltante : pedidoVO.getFaltantes()) {
                textoFaltantes += faltante.getNoOrden() + ": " + faltante.getFaltantes() + " \r\n";
            }
        }

        cell = row.createCell(j++);
        cell.setCellValue(new XSSFRichTextString(textoFaltantes));
        CellStyle style = reporte.createCellStyle();
        style.setWrapText(true);
        cell.setCellStyle(style);

        cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getPiezasConfirmadas());

        cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getTotalFacturas());

        cell = row.createCell(j++);
        cell.setCellValue(pedidoVO.getNoEnviados());
        agregaColorDiferencia(pedidoVO.getNoEnviados(), cell);
    }
    private void agregaColorDiferencia(String noEnviados, Cell cell) {
        String valor = noEnviados.replace(",", "");
        if (NumberUtils.isNumber(valor)) {
            CellStyle style = reporte.createCellStyle();
            int diferencia = Integer.parseInt(valor);
            if (diferencia > 0) {
                style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            } else if (diferencia == 0) {
                style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            } else {
                style.setFillForegroundColor(IndexedColors.RED.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            }
             cell.setCellStyle(style);
        }
       
    }
}
