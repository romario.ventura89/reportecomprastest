package mx.com.miniso.control.excel;

import java.util.List;
import mx.com.miniso.control.vo.FacturaDetalleVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class GeneradorExcelFactura extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorExcelFactura.class);

    private final List<FacturaDetalleVO> detalles;
    private final String SKU = "Product code";

    private final String DESCRIPCION = "Product name";
    private final String PIEZAS = "pieces";
    private final String PRECIO_UNITARIO = "Unit price";
    private final String PRECIO_TOTAL = "Total price";
    private final String contenedor;

    public GeneradorExcelFactura(String numeroOrden, String contenedor, List<FacturaDetalleVO> detalles) {
        super("Numero Orden " + numeroOrden + "Contenedor " + contenedor);
        this.detalles = detalles;
        this.contenedor = contenedor;
    }

    public GeneradorExcelFactura(String contenedor, List<FacturaDetalleVO> detalles) {
        super("Contenedor " + contenedor);
        this.detalles = detalles;
        this.contenedor = contenedor;
    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet generalSheet = reporte.createSheet(contenedor);
        generalSheet = agregaTitulo(generalSheet);
        int indexGeneral = 1;

        for (FacturaDetalleVO detalle : detalles) {
            agregaDatos(detalle, generalSheet, indexGeneral++);

        }
    }

    private SXSSFSheet agregaTitulo(SXSSFSheet sheet) {

        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(SKU);

        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);

        cell = row.createCell(i++);
        cell.setCellValue(PIEZAS);

        cell = row.createCell(i++);
        cell.setCellValue(PRECIO_UNITARIO);

        cell = row.createCell(i++);
        cell.setCellValue(PRECIO_TOTAL);
        aplicarAutoSize(sheet, i);
        return sheet;
    }

    private void agregaDatos(FacturaDetalleVO detalle, SXSSFSheet sheet, int i) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(detalle.getSku());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getDescripcion());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getPiezas());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getPrecioUnitario());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getPrecioTotal());

    }

    @Override
    protected void agregarEncabezados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
