package mx.com.miniso.control.excel;

import mx.com.miniso.control.vo.ComodatoVO;
import mx.com.miniso.control.vo.ContainerVO;
import mx.com.miniso.control.vo.ContenedorVO;

import mx.com.miniso.control.vo.ReporteComodatoVO;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

public class GeneradorReporteComodato extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorReporteComodato.class);

    private final ReporteComodatoVO reporteComodato;
    private final String SKU = "SKU";

    private final String PRODUCTNAME ="PRODUCT NAME";
    private final String ORDER_NO = "ORDER NO";
    private final String PIECES_CONFIRMED = "PIECES CONFIRMED";
    private final String TOTAL_INVOICE = "TOTAL INVOICE";
    private final String DIFFERENCE = "DIFFERENCE";

    public GeneradorReporteComodato(ReporteComodatoVO reporteComodato) {
        super("Comodato ");
        this.reporteComodato = reporteComodato;

    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet generalSheet = reporte.createSheet("Comodato");

        generalSheet = agregaTitulo(generalSheet);
        int indexGeneral = 1;
        for (ComodatoVO comodato : reporteComodato.getComodatos()) {

            agregaDatos(comodato, generalSheet, indexGeneral++);

        }
        autoSize(generalSheet);
    }

    private void autoSize(SXSSFSheet sheet) {

    }

    private SXSSFSheet agregaTitulo(SXSSFSheet sheet) {

        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(SKU);

        cell = row.createCell(i++);
        cell.setCellValue(PRODUCTNAME);

        cell = row.createCell(i++);
        cell.setCellValue(ORDER_NO);

        cell = row.createCell(i++);
        cell.setCellValue(PIECES_CONFIRMED);

        for (ContainerVO contenedor : reporteComodato.getContenedores()) {
            cell = row.createCell(i++);
            cell.setCellValue(contenedor.getNombreContenedor());
        }
        cell = row.createCell(i++);
        cell.setCellValue(TOTAL_INVOICE);
        cell = row.createCell(i++);
        cell.setCellValue(DIFFERENCE);
        aplicarAutoSize(sheet, i);

        return sheet;
    }

    @Override
    protected void agregarEncabezados() {

    }

    private void agregaDatos(ComodatoVO comodato, SXSSFSheet sheet, int i) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(comodato.getSku());

        cell = row.createCell(j++);
        cell.setCellValue(comodato.getProductName());

        cell = row.createCell(j++);
        cell.setCellValue(comodato.getNumeroOrden());

        cell = row.createCell(j++);
        cell.setCellValue(comodato.getPiezasConfirmadas());

        for (ContenedorVO contenedor : comodato.getContenedores()) {
            cell = row.createCell(j++);
            cell.setCellValue(contenedor.getPiezas());
        }

        cell = row.createCell(j++);
        cell.setCellValue(comodato.getTotalFacturas());

        cell = row.createCell(j++);
        cell.setCellValue(comodato.getNoEnviados());
        agregaColorDiferencia(comodato.getNoEnviados(), cell);

    }

    private void agregaColorDiferencia(String noEnviados, Cell cell) {
        String valor = noEnviados.replace(",", "");
        if (NumberUtils.isNumber(valor)) {
            CellStyle style = reporte.createCellStyle();
            int diferencia = Integer.parseInt(valor);
            if (diferencia > 0) {
                style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            } else if (diferencia == 0) {
                style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            } else {
                style.setFillForegroundColor(IndexedColors.RED.getIndex());
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            }
             cell.setCellStyle(style);
        }
       
    }
}
