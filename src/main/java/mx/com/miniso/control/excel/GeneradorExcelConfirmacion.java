package mx.com.miniso.control.excel;

import java.util.List;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.vo.ConfirmacionDetalleVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class GeneradorExcelConfirmacion extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorExcelConfirmacion.class);

    private final List<ConfirmacionDetalleVO> detalles;
    private Confirmacion confirmacion;
    private String tipoOrden;
    private final String SKU = "Product code";
    private final String DESCRIPCION = "product name";
    private final String CBM = "carton cbm";
    private final String PRECIO = "price";
    private final String SOLICITUDES = "ordered quantity";
    private final String ENTREGADAS = "final shipped quantity";
    private final String TOTAL = "Total price";

    public GeneradorExcelConfirmacion(Confirmacion confirmacion, List<ConfirmacionDetalleVO> detalles, String tipoOrden) {
        super("Confirmacion " + confirmacion.toString());
        this.detalles = detalles;
        this.confirmacion = confirmacion;
        this.tipoOrden = tipoOrden;
    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet generalSheet = null;
        SXSSFSheet qianhaiSheet = null;
        SXSSFSheet pingshanSheet = null;

        int indexGeneral = 1;
        int indexPingshan = 1;
        int indexQianhai = 1;

        boolean paginaGeneralCreada = false;
        boolean paginaPingshanCreada = false;
        boolean paginaQianhaiCreada = false;

        for (ConfirmacionDetalleVO detalle : detalles) {

            if (!paginaGeneralCreada) {
                generalSheet = reporte.createSheet(tipoOrden);
                generalSheet = agregaTitulo(generalSheet);
                paginaGeneralCreada = true;
            }

            agregaDatos(detalle, generalSheet, indexGeneral++);

            /*if (detalle.isGeneral()) {
                if (!paginaGeneralCreada) {
                    generalSheet = reporte.createSheet("GENERAL");
                    generalSheet = agregaTitulo(generalSheet);
                    paginaGeneralCreada = true;
                }
                agregaDatos(detalle, generalSheet, indexGeneral++);
            } else if (detalle.isPingshan()) {
                if (!paginaPingshanCreada) {
                    pingshanSheet = reporte.createSheet("PINGSHAN");
                    pingshanSheet = agregaTitulo(pingshanSheet);
                    paginaPingshanCreada = true;
                }
                agregaDatos(detalle, pingshanSheet, indexPingshan++);
            } else if (detalle.isQianhai()) {
                if (!paginaQianhaiCreada) {
                    qianhaiSheet = reporte.createSheet("QIANHAI");
                    qianhaiSheet = agregaTitulo(qianhaiSheet);
                    paginaQianhaiCreada = true;
                }
                agregaDatos(detalle, qianhaiSheet, indexQianhai++);
            }*/
        }
        /*autoSize(generalSheet);
        autoSize(qianhaiSheet);
        autoSize(pingshanSheet);*/
    }

    private void autoSize(SXSSFSheet sheet) {

    }

    private SXSSFSheet agregaTitulo(SXSSFSheet sheet) {

        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(SKU);

        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);

        cell = row.createCell(i++);
        cell.setCellValue(CBM);

        cell = row.createCell(i++);
        cell.setCellValue(PRECIO);

        cell = row.createCell(i++);
        cell.setCellValue(SOLICITUDES);

        cell = row.createCell(i++);
        cell.setCellValue(ENTREGADAS);

        cell = row.createCell(i++);
        cell.setCellValue(TOTAL);
        aplicarAutoSize(sheet, i);
        return sheet;
    }

    @Override
    protected void agregarEncabezados() {

    }

    private void agregaDatos(ConfirmacionDetalleVO detalle, SXSSFSheet sheet, int i) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(detalle.getSku());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getDescripcion());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getCartoncbm());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getPrecioUnitario());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getPiezasSolicitadas());

        cell = row.createCell(j++);
        if (detalle.isNotOrder()) {
            cell.setCellValue("Not order");
        } else {
            cell.setCellValue(detalle.getPiezasEntregadas());
        }

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getPrecioTotal());

    }
}
