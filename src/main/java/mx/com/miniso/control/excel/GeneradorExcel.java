package mx.com.miniso.control.excel;

import mx.com.miniso.control.util.FechaUtil;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public abstract class GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorExcel.class);

    private final String ENCABEZADO_HTML = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private final String MENSAJE_ERROR_IOEXCEPTION = "No se realizo la obtencion del reporte por las siguientes causas:";

    private final String subtituloReporte;
    protected SXSSFWorkbook reporte;

    private final List<String> encabezados;

    protected boolean aplicarAutosizeDespuesReporte;

    protected abstract void crearReporte(SXSSFWorkbook reporte);
    
   

    protected abstract void agregarEncabezados();

    public GeneradorExcel(String subtituloReporte) {
        this.subtituloReporte = subtituloReporte;
        reporte = new SXSSFWorkbook();
        reporte.setCompressTempFiles(true);
        encabezados = new ArrayList<>(0);
    }

    public final byte[] obtenerArchivo() throws IOException {
        LOGGER.info("Prepara archivo");

        crearReporte(reporte);
        LOGGER.info("Datos agregados");

        LOGGER.info("Area de Impresión");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            reporte.write(bos);
        } finally {
            bos.close();
        }
        return bos.toByteArray();

    }

    protected void agregaValorCelda(SXSSFRow row, Object valor, int indice) {
        if (valor instanceof Integer) {
            agregaValorCelda(row, (Integer) valor, indice);
            return;
        }
        if (valor instanceof String) {
            agregaValorCelda(row, (String) valor, indice);
            return;
        }
        if (valor instanceof Long) {
            agregaValorCelda(row, (Long) valor, indice);
            return;
        }
        if (valor instanceof Number) {
            agregaValorCelda(row, (Number) valor, indice);
            return;
        }
        if (valor instanceof BigDecimal) {
            agregaValorCelda(row, (BigDecimal) valor, indice);
            return;
        }

        if (valor instanceof Date) {
            agregaValorCelda(row, (Date) valor, indice);
        }

    }

    protected void agregaValorCelda(SXSSFRow row, Integer valor, int indice) {
        SXSSFCell cell = row.createCell(indice);
        if (valor != null) {
            cell.setCellValue(valor);
        }
    }

    protected void agregaValorCelda(SXSSFRow row, Long valor, int indice) {
        SXSSFCell cell = row.createCell(indice);
        if (valor != null) {
            cell.setCellValue(valor);
        }
    }

    protected void agregaValorCelda(SXSSFRow row, String valor, int indice) {
        SXSSFCell cell = row.createCell(indice);
        if (valor != null) {
            cell.setCellValue(valor.trim());
        }

    }

    protected void agregaValorCelda(SXSSFRow row, Number valor, int indice) {
        SXSSFCell cell = row.createCell(indice);
        if (valor != null) {
            cell.setCellValue(valor.longValue());
        }
    }

    protected void agregaValorCelda(SXSSFRow row, BigDecimal valor, int indice) {
        SXSSFCell cell = row.createCell(indice);
        if (valor != null) {
            cell.setCellValue(valor.toString());
        }
    }

    protected void agregaValorCelda(SXSSFRow row, Date valor, int indice) {
        SXSSFCell cell = row.createCell(indice);
        if (valor != null) {
            cell.setCellValue(FechaUtil.formateaFecha(valor));
        }
    }

    private void agregarImagen(String nombreImagen, int col, double porcentageTamanioImg) {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        String path = externalContext.getRealPath("images");
        InputStream is = null;
        try {
            is = new FileInputStream(path + "/" + nombreImagen);
            byte[] bytes = IOUtils.toByteArray(is);
            int pictureIdx = reporte.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);

            CreationHelper helper = reporte.getCreationHelper();
            SXSSFSheet sheet = reporte.getSheetAt(0);
            Drawing drawing = sheet.createDrawingPatriarch();
            ClientAnchor anchor = helper.createClientAnchor();
            anchor.setAnchorType(ClientAnchor.AnchorType.DONT_MOVE_AND_RESIZE);

            anchor.setCol1(col);
            anchor.setRow1(0);

            drawing.createPicture(anchor, pictureIdx).resize(porcentageTamanioImg);

        } catch (IOException ex) {
            LOGGER.error("Sucedió un error al agregar la imagen " + nombreImagen + " al reporte", ex);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    LOGGER.error("Sucedió un error al cerrar el inputstream", ex);
                }
            }
        }

    }

    protected void aplicarValorEncabezados(String encabezado) {
        encabezados.add(encabezado);
    }

    private void crearEncabezados() {
        SXSSFSheet sheet = reporte.getSheetAt(0);
        SXSSFRow row = sheet.createRow(2);

        for (int i = 0; i < encabezados.size(); i++) {
            SXSSFCell cell = row.createCell(i);
            cell.setCellValue(encabezados.get(i));
            aplicarEstilosEncabezados(cell);
        }
    }

    protected void aplicarEstilosEncabezados(SXSSFCell cell) {
        CellStyle style = reporte.createCellStyle();

        style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());

        Font font = reporte.createFont();
        font.setFontName("SoberanaTitular");
        font.setFontHeightInPoints((short) 10);
        font.setBold(true);
        font.setColor(IndexedColors.BLACK.getIndex());
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
        cell.setCellStyle(style);
    }

    protected void aplicarMargenImpresion() {
        SXSSFSheet sheet = reporte.getSheetAt(0);

        reporte.setPrintArea(0, 0, encabezados.size(), 0, sheet.getPhysicalNumberOfRows());

        sheet.getPrintSetup().setFitWidth((short) 1);
        sheet.getPrintSetup().setFitHeight((short) 0);
        sheet.setAutobreaks(true);

        sheet.getPrintSetup().setFooterMargin(0.25);
        sheet.getPrintSetup().setHeaderMargin(0.25);
        sheet.setMargin(SXSSFSheet.LeftMargin, 0.25);
        sheet.setMargin(SXSSFSheet.RightMargin, 0.25);
        sheet.setMargin(SXSSFSheet.TopMargin, 0.25);
        sheet.setMargin(SXSSFSheet.BottomMargin, 0.5);
        sheet.getPrintSetup().setScale((short) 70);
        sheet.getPrintSetup().setLandscape(true);
        sheet.setDisplayGridlines(false);
        sheet.setPrintGridlines(false);
    }

    protected void aplicarAutoSize(SXSSFSheet sheet, int numeroColumnas) {
        sheet.trackAllColumnsForAutoSizing();
        for (int j = 0; j < numeroColumnas; j++) {
            sheet.autoSizeColumn(j);
        }
    }
}
