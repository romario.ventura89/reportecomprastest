package mx.com.miniso.control.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "facturaProducto")
public class FacturaProducto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idFacturaProducto")
    private long idFacturaProducto;

    @Column(name = "piezas")
    private int piezas;

    @Column(name = "precioUnitario")
    private double precioUnitario;

    @Column(name = "precioTotal")
    private double precioTotal;

    @Column(name = "idFactura")
    private long idFactura;

    @Column(name = "idProductoCompra")
    private long idProductoCompra;

    public FacturaProducto() {

    }

    public FacturaProducto(Factura factura, HistoricoFacturaProducto historicoProducto) {
        if (factura != null && historicoProducto != null) {
            this.piezas = historicoProducto.getPiezas();
            this.precioUnitario = historicoProducto.getPrecioUnitario();
            this.precioTotal = historicoProducto.getPrecioTotal();
            this.idFactura = factura.getIdFactura();
            this.idProductoCompra = historicoProducto.getIdProductoCompra();
        }
    }

    public long getIdFacturaProducto() {
        return idFacturaProducto;
    }

    public void setIdFacturaProducto(long idFacturaProducto) {
        this.idFacturaProducto = idFacturaProducto;
    }

    public int getPiezas() {
        return piezas;
    }

    public void setPiezas(int piezas) {
        this.piezas = piezas;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public long getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(long idFactura) {
        this.idFactura = idFactura;
    }

    public long getIdProductoCompra() {
        return idProductoCompra;
    }

    public void setIdProductoCompra(long idProductoCompra) {
        this.idProductoCompra = idProductoCompra;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (int) (this.idFacturaProducto ^ (this.idFacturaProducto >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FacturaProducto other = (FacturaProducto) obj;
        if (this.idFacturaProducto != other.idFacturaProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FacturaProducto{" + "idFacturaProducto=" + idFacturaProducto + ", piezas=" + piezas + ", precioUnitario=" + precioUnitario + ", precioTotal=" + precioTotal + ", idFactura=" + idFactura + ", idProductoCompra=" + idProductoCompra + '}';
    }

}
