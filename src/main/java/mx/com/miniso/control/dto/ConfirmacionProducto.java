package mx.com.miniso.control.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "confirmacionProducto")
public class ConfirmacionProducto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idConfirmacionProducto")
    private long idConfirmacionProducto;

    @Column(name = "piezasEntregadas")
    private int piezasEntregadas;

    @Column(name = "piezasSolicitadas")
    private int piezasSolicitadas;

    @Column(name = "precioUnitario")
    private double precioUnitario;

    @Column(name = "precioTotal")
    private double precioTotal;
    
    @Column(name = "cbm")
    private double cbm;

    @Column(name = "notOrder")
    private boolean notOrder;

    @Column(name = "idConfirmacion")
    private long idConfirmacion;

    @Column(name = "idProductoCompra")
    private long idProductoCompra;

    @Column(name = "tipoOrden")
    private String tipoOrden;

    public long getIdConfirmacionProducto() {
        return idConfirmacionProducto;
    }

    public void setIdConfirmacionProducto(long idConfirmacionProducto) {
        this.idConfirmacionProducto = idConfirmacionProducto;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public int getPiezasEntregadas() {
        return piezasEntregadas;
    }

    public void setPiezasEntregadas(int piezasEntregadas) {
        this.piezasEntregadas = piezasEntregadas;
    }

    public int getPiezasSolicitadas() {
        return piezasSolicitadas;
    }

    public void setPiezasSolicitadas(int piezasSolicitadas) {
        this.piezasSolicitadas = piezasSolicitadas;
    }

    public boolean isNotOrder() {
        return notOrder;
    }

    public void setNotOrder(boolean notOrder) {
        this.notOrder = notOrder;
    }

    public long getIdConfirmacion() {
        return idConfirmacion;
    }

    public void setIdConfirmacion(long idConfirmacion) {
        this.idConfirmacion = idConfirmacion;
    }

    public long getIdProductoCompra() {
        return idProductoCompra;
    }

    public void setIdProductoCompra(long idProductoCompra) {
        this.idProductoCompra = idProductoCompra;
    }

    public String getTipoOrden() {
        return tipoOrden;
    }

    public void setTipoOrden(String tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    public double getCbm() {
        return cbm;
    }

    public void setCbm(double cbm) {
        this.cbm = cbm;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (this.idConfirmacionProducto ^ (this.idConfirmacionProducto >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConfirmacionProducto other = (ConfirmacionProducto) obj;
        if (this.idConfirmacionProducto != other.idConfirmacionProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ConfirmacionProducto{" + "idConfirmacionProducto=" + idConfirmacionProducto + ", piezasEntregadas=" + piezasEntregadas + ", piezasSolicitadas=" + piezasSolicitadas + ", precioUnitario=" + precioUnitario + ", precioTotal=" + precioTotal + ", cbm=" + cbm + ", notOrder=" + notOrder + ", idConfirmacion=" + idConfirmacion + ", idProductoCompra=" + idProductoCompra + ", idTipoOrden=" + tipoOrden + '}';
    }
    
}
