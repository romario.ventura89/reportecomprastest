package mx.com.miniso.control.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "confirmacion")
public class Confirmacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idConfirmacion")
    private long idConfirmacion;

    @Column(name = "fechaRegistro")
    private Timestamp fechaRegistro;

    @Column(name = "estatus")
    private char estatus;

    @Column(name = "noOrden")
    private String noOrden;

    @Column(name = "idSociedad")
    private long idSociedad;



    @Column(name = "fechaConfirmacion")
    private Date fechaConfirmacion;

    public long getIdConfirmacion() {
        return idConfirmacion;
    }

    public void setIdConfirmacion(long idConfirmacion) {
        this.idConfirmacion = idConfirmacion;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public char getEstatus() {
        return estatus;
    }

    public void setEstatus(char estatus) {
        this.estatus = estatus;
    }

    public String getNoOrden() {
        return noOrden;
    }

    public void setNoOrden(String noOrden) {
        this.noOrden = noOrden;
    }

    public long getIdSociedad() {
        return idSociedad;
    }

    public void setIdSociedad(long idSociedad) {
        this.idSociedad = idSociedad;
    }

    public Date getFechaConfirmacion() {
        return fechaConfirmacion;
    }

    public void setFechaConfirmacion(Date fechaConfirmacion) {
        this.fechaConfirmacion = fechaConfirmacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.idConfirmacion ^ (this.idConfirmacion >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Confirmacion other = (Confirmacion) obj;
        if (this.idConfirmacion != other.idConfirmacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Confirmacion{" + "idConfirmacion=" + idConfirmacion + ", fechaRegistro=" + fechaRegistro + ", estatus=" + estatus + ", noOrden=" + noOrden + ", idSociedad=" + idSociedad + ", fechaConfirmacion=" + fechaConfirmacion + '}';
    }

  

}
