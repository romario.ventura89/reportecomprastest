package mx.com.miniso.control.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "respuestaProducto")
public class RespuestaProducto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idRespuestaProducto")
    private long idRespuestaProducto;

    @Column(name = "storeCode")
    private String storeCode;

    @Column(name = "item")
    private String item;

    @Column(name = "carton")
    private String carton;

    @Column(name = "piezas")
    private int piezas;

    @Column(name = "precioUnitario")
    private double precioUnitario;

    @Column(name = "idProductoCompra")
    private long idProductoCompra;

    @Column(name = "idRespuesta")
    private long idRespuesta;

    public long getIdRespuestaProducto() {
        return idRespuestaProducto;
    }

    public void setIdRespuestaProducto(long idRespuestaProducto) {
        this.idRespuestaProducto = idRespuestaProducto;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getCarton() {
        return carton;
    }

    public void setCarton(String carton) {
        this.carton = carton;
    }

    public int getPiezas() {
        return piezas;
    }

    public void setPiezas(int piezas) {
        this.piezas = piezas;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public long getIdProductoCompra() {
        return idProductoCompra;
    }

    public void setIdProductoCompra(long idProductoCompra) {
        this.idProductoCompra = idProductoCompra;
    }

    public long getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(long idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (this.idRespuestaProducto ^ (this.idRespuestaProducto >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RespuestaProducto other = (RespuestaProducto) obj;
        if (this.idRespuestaProducto != other.idRespuestaProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RespuestaProducto{" + "idRespuestaProducto=" + idRespuestaProducto + ", storeCode=" + storeCode + ", item=" + item + ", carton=" + carton + ", piezas=" + piezas + ", precioUnitario=" + precioUnitario + ", idProductoCompra=" + idProductoCompra + ", idRespuesta=" + idRespuesta + '}';
    }

}
