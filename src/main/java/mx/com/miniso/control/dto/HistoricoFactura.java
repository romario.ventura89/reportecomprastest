package mx.com.miniso.control.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "historicoFactura")
public class HistoricoFactura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idFactura")
    private long idFactura;

    @Column(name = "fechaRegistro")
    private Timestamp fechaRegistro;

    @Column(name = "fechaFactura")
    private Date fechaFactura;

    @Column(name = "estatus")
    private char estatus;

    @Column(name = "contenedor")
    private String contenedor;

    @Column(name = "noFactura")
    private String noFactura;

    @Column(name = "etd")
    private Date etd;

    @Column(name = "eta")
    private Date eta;

    public HistoricoFactura() {

    }

    public HistoricoFactura(Factura factura) {
        if (factura != null) {
            this.fechaRegistro = factura.getFechaRegistro();
            this.fechaFactura = factura.getFechaFactura();
            this.estatus = factura.getEstatus();
            this.contenedor = factura.getContenedor();
            this.noFactura = factura.getNoFactura();
            this.etd = factura.getEtd();
            this.eta = factura.getEta();
        }

    }

    public long getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(long idFactura) {
        this.idFactura = idFactura;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public char isEstatus() {
        return estatus;
    }

    public void setEstatus(char estatus) {
        this.estatus = estatus;
    }

    public String getContenedor() {
        return contenedor;
    }

    public void setContenedor(String contenedor) {
        this.contenedor = contenedor;
    }

    public String getNoFactura() {
        return noFactura;
    }

    public void setNoFactura(String noFactura) {
        this.noFactura = noFactura;
    }

    public Date getEtd() {
        return etd;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public Date getEta() {
        return eta;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public char getEstatus() {
        return estatus;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (int) (this.idFactura ^ (this.idFactura >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HistoricoFactura other = (HistoricoFactura) obj;
        if (this.idFactura != other.idFactura) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HistoricoFactura{" + "idFactura=" + idFactura + ", fechaRegistro=" + fechaRegistro + ", fechaFactura=" + fechaFactura + ", estatus=" + estatus + ", contenedor=" + contenedor + ", noFactura=" + noFactura + ", etd=" + etd + ", eta=" + eta + '}';
    }

}
