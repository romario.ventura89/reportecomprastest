package mx.com.miniso.control.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "sesion")
public class Sesion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idSesion")
    private long idSesion;

    @Column(name = "idUsuario")
    private long idUsuario;

    @Column(name = "idNavegacion")
    private long idNavegacion;

    public long getIdSesion() {
        return idSesion;
    }

    public void setIdSesion(long idSesion) {
        this.idSesion = idSesion;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getIdNavegacion() {
        return idNavegacion;
    }

    public void setIdNavegacion(long idNavegacion) {
        this.idNavegacion = idNavegacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.idSesion ^ (this.idSesion >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sesion other = (Sesion) obj;
        if (this.idSesion != other.idSesion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Sesion{" + "idSesion=" + idSesion + ", idUsuario=" + idUsuario + ", idNavegacion=" + idNavegacion + '}';
    }

}
