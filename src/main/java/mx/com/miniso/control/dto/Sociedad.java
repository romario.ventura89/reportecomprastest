/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miniso.control.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "sociedad")
public class Sociedad implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "idSociedad")
    private long idSociedad;

    @Column(name = "descripcion")
    private String descripcion;

    public long getIdSociedad() {
        return idSociedad;
    }

    public void setIdSociedad(long idSociedad) {
        this.idSociedad = idSociedad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (int) (this.idSociedad ^ (this.idSociedad >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sociedad other = (Sociedad) obj;
        if (this.idSociedad != other.idSociedad) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Sociedad{" + "idSociedad=" + idSociedad + ", descripcion=" + descripcion + '}';
    }
    
}
