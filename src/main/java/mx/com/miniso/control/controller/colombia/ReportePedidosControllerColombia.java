
package mx.com.miniso.control.controller.colombia;

import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.miniso.control.constantes.Constantes;
import mx.com.miniso.control.constantes.MensajesVista;
import mx.com.miniso.control.excel.GeneradorExcel;
import mx.com.miniso.control.excel.GeneradorReportePedidos;
import mx.com.miniso.control.service.colombia.ReportePedidosColombiaService;
import mx.com.miniso.control.vo.FiltroPedidosVO;
import mx.com.miniso.control.vo.ReportePedidosVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/pedidosColombia")
public class ReportePedidosControllerColombia {
    
    final static Log logger = LogFactory.getLog(ReportePedidosControllerColombia.class);

    @Autowired
    private ReportePedidosColombiaService reportePedidosColombiaService;

    @RequestMapping(value = "/buscar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody

    public ResponseEntity<?> buscar(@RequestBody FiltroPedidosVO filtroPedidosVO, HttpServletRequest request) {

        ReportePedidosVO reporte;
        try {
            reporte = reportePedidosColombiaService.buscar(filtroPedidosVO);
            request.getSession().setAttribute(Constantes.SESION_REPORTE, reporte);
            return new ResponseEntity<>(reporte, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/excel", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(HttpServletRequest request, HttpServletResponse response) {
        try {
            ReportePedidosVO reporte = (ReportePedidosVO) request.getSession().getAttribute(Constantes.SESION_REPORTE);
            GeneradorExcel generador = new GeneradorReportePedidos(reporte);

            response.addHeader("Content-disposition", "attachment;filename="
                    + "REPORTE_PEDIDOS.xlsx");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            OutputStream output = response.getOutputStream();
            output.write(generador.obtenerArchivo());
            output.close();

        } catch (Exception ex) {
            logger.error("Error al generar el archivo reporte de confirmacion", ex);
        }

    }
    
}
