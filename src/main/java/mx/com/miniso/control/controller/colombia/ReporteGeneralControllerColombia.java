package mx.com.miniso.control.controller.colombia;

import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.miniso.control.excel.GeneradorExcel;
import mx.com.miniso.control.excel.GeneradorReporteGeneral;
import mx.com.miniso.control.service.colombia.ReporteComodatoColombiaService;
import mx.com.miniso.control.service.colombia.ReportePedidosColombiaService;
import mx.com.miniso.control.service.colombia.ReportesColombiaService;
import mx.com.miniso.control.vo.FiltroComodatoVO;
import mx.com.miniso.control.vo.FiltroPedidosVO;
import mx.com.miniso.control.vo.ReporteComodatoVO;
import mx.com.miniso.control.vo.ReporteConfirmacionVO;
import mx.com.miniso.control.vo.ReportePedidosVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/reportesGeneralColombia")
public class ReporteGeneralControllerColombia {

    final static Log logger = LogFactory.getLog(ReporteGeneralControllerColombia.class);

    @Autowired
    private ReporteComodatoColombiaService reporteComodatoColombiaService;

    @Autowired
    private ReportesColombiaService reportesColombiaService;

    @Autowired
    private ReportePedidosColombiaService reportePedidosColombiaService;

    @RequestMapping(value = "/excel", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ReporteConfirmacionVO> reportesConfirmacion = reportesColombiaService.Confirmacion();
            ReporteComodatoVO reporteComodato = reporteComodatoColombiaService.buscar(new FiltroComodatoVO());
            ReportePedidosVO reportePedidos = reportePedidosColombiaService.buscar(new FiltroPedidosVO());
            response.addHeader("Content-disposition", "attachment;filename="
                    + "REPORTE_GENERAL.xlsx");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            GeneradorExcel generador = new GeneradorReporteGeneral(reportesConfirmacion, reporteComodato, reportePedidos);
            OutputStream output = response.getOutputStream();
            output.write(generador.obtenerArchivo());
            output.close();
        } catch (Exception ex) {
            logger.error("Error al generar el archivo reporte de confirmacion", ex);
        }

    }

}
