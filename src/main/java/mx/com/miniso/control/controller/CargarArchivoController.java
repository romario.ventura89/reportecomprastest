package mx.com.miniso.control.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import mx.com.miniso.control.constantes.MensajesVista;
import mx.com.miniso.control.ex.ConfirmacionNumeroOrdenExcepcion;
import mx.com.miniso.control.service.CargaConfirmacionService;
import mx.com.miniso.control.service.CargaFacturaService;
import mx.com.miniso.control.service.ConfirmacionService;
import mx.com.miniso.control.service.FacturaService;
import mx.com.miniso.control.service.colombia.CargaConfirmacionColombiaService;
import mx.com.miniso.control.service.colombia.CargaFacturaColombiaService;
import mx.com.miniso.control.service.colombia.ConfirmacionColombiaService;
import mx.com.miniso.control.service.colombia.FacturaColombiaService;
import mx.com.miniso.control.util.ValidaCargaConfirmacion;
import mx.com.miniso.control.util.ValidaCargaFactura;
import mx.com.miniso.control.util.Validador;
import mx.com.miniso.control.vo.CargaConfirmacionVO;
import mx.com.miniso.control.vo.CargaFacturaVO;
import mx.com.miniso.control.vo.TabCargaConfirmacionVO;
import mx.com.miniso.control.vo.TabCargaFacturaVO;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/cargaArchivo")
public class CargarArchivoController {

    final static Logger logger = LoggerFactory.getLogger(CargarArchivoController.class);

    @Autowired
    private CargaConfirmacionService cargaConfirmacionService;

    @Autowired
    private CargaFacturaService cargaFacturaService;

    @Autowired
    private ConfirmacionService confirmacionService;

    @Autowired
    private FacturaService facturaService;

    @RequestMapping(value = "/confirmacion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> confirmacion(MultipartHttpServletRequest request) {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {

                List<CargaConfirmacionVO> cargaConfirmacionVOList = new ArrayList();
                CargaConfirmacionVO cargaConfirmacionVO;

                Map<String, MultipartFile> fileMap = request.getFileMap();
                String nombreArchivo = "";

                for (MultipartFile archivo : fileMap.values()) {
                    cargaConfirmacionVO = new CargaConfirmacionVO();

                    nombreArchivo = archivo.getOriginalFilename();
                    nombreArchivo = nombreArchivo.substring(0, nombreArchivo.lastIndexOf("."));
                    StringTokenizer stringTokenizer = new StringTokenizer(nombreArchivo, " ");
                    String valor = "";
                    int i = 0;
                    while (stringTokenizer.hasMoreElements()) {
                        valor = stringTokenizer.nextElement().toString();
                        if (i == 1) {
                            cargaConfirmacionVO.setNumeroOrden(nombreArchivo);
                            break;
                        }
                        i++;
                    }

                    cargaConfirmacionVO.setFechaConfirmacion(request.getParameter("fechaConfirmacion"));
                    cargaConfirmacionVO = cargaConfirmacionService.obtenerDatos(cargaConfirmacionVO, archivo);
                    cargaConfirmacionVOList.add(cargaConfirmacionVO);
                }

                Validador validador = new ValidaCargaConfirmacion(confirmacionService);

                List<CargaConfirmacionVO> cargaConfirmacionVOListGuarda = new ArrayList();
                List<CargaConfirmacionVO> cargaConfirmacionVOListNoUtorizado = new ArrayList();

                for (CargaConfirmacionVO cargaVO : cargaConfirmacionVOList) {
                    if (validador.realizarValidacion(cargaVO)) {
                        cargaConfirmacionVOListNoUtorizado.add(cargaVO);
                    } else {
                        cargaConfirmacionVOListGuarda.add(cargaVO);
                    }
                }

                if (cargaConfirmacionVOListNoUtorizado.size() <= 0) {
                    for (CargaConfirmacionVO cargaVO : cargaConfirmacionVOList) {
                        cargaConfirmacionService.guarda(cargaVO);
                    }
                }

                TabCargaConfirmacionVO tabCargaConfirmacionVO = new TabCargaConfirmacionVO();
                tabCargaConfirmacionVO.setCargaConfirmacionVOListError(cargaConfirmacionVOListNoUtorizado);
                tabCargaConfirmacionVO.setCargaConfirmacionVOListExitoso(cargaConfirmacionVOListGuarda);

                ResponseEntity response = new ResponseEntity<>(tabCargaConfirmacionVO, HttpStatus.OK);
                return response;
            }
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo de pedido", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (IOException e) {
            logger.error("Error al cargar el archivo de pedido", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo de pedido", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/factura", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> factura(MultipartHttpServletRequest request) {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {

                List<CargaFacturaVO> cargaFacturaVOList = new ArrayList();
                String numeroOrden = request.getParameter("numeroOrden");
                CargaFacturaVO cargaFacturaVO;
                Boolean estatus = true;

                cargaFacturaVO = new CargaFacturaVO();
                cargaFacturaVO.setNumeroOrden(numeroOrden);

                estatus = cargaFacturaService.validaOrden(numeroOrden);
                if (estatus) {
                    Map<String, MultipartFile> fileMap = request.getFileMap();
                    String nombreArchivo = "";

                    for (MultipartFile archivo : fileMap.values()) {
                        cargaFacturaVO = new CargaFacturaVO();
                        cargaFacturaVO.setNombreArchivo(archivo.getOriginalFilename());
                        cargaFacturaVO.setNumeroOrden(numeroOrden);

                        cargaFacturaVO = cargaFacturaService.obtenerDatos(cargaFacturaVO, archivo);

                        nombreArchivo = archivo.getOriginalFilename();
                        nombreArchivo = obtenercadena(nombreArchivo, " ", 2);
                        cargaFacturaVO.setContenedor(nombreArchivo);

                        cargaFacturaVOList.add(cargaFacturaVO);
                    }

                    List<CargaFacturaVO> cargaFacturaVOListGuarda = new ArrayList();
                    List<CargaFacturaVO> cargaFacturaVOListNoUtorizado = new ArrayList();

                    Validador validador = new ValidaCargaFactura(facturaService, confirmacionService);

                    for (CargaFacturaVO cargaVO : cargaFacturaVOList) {
                        if (validador.realizarValidacion(cargaVO)) {
                            cargaFacturaVOListNoUtorizado.add(cargaVO);
                        } else {
                            cargaFacturaVOListGuarda.add(cargaVO);
                        }
                    }

                    if (cargaFacturaVOListNoUtorizado.size() <= 0) {
                        for (CargaFacturaVO cargaVO : cargaFacturaVOList) {
                            cargaFacturaService.guarda(cargaVO);
                        }
                    }

                    TabCargaFacturaVO tabCargaFacturaVO = new TabCargaFacturaVO();

                    tabCargaFacturaVO.setCargaFacturaVOListError(cargaFacturaVOListNoUtorizado);
                    tabCargaFacturaVO.setCargaFacturaVOListExitoso(cargaFacturaVOListGuarda);

                    return new ResponseEntity<>(tabCargaFacturaVO, HttpStatus.OK);

                } else {
                    cargaFacturaVO.setNumeroOrdenError(true);
                    cargaFacturaVO.setNumeroOrdenErrorTxt("No existe el número de orden " + numeroOrden);

                    logger.error("No existe el número de orden " + numeroOrden, cargaFacturaVO);
                    return new ResponseEntity<String>(MensajesVista.ERROR_ORDER, HttpStatus.CONFLICT);
                }
            }
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo de pedido", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo de factura", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
    }

    public String obtenercadena(String nombreArchivo, String caracter, int indice) {
        StringTokenizer stringTokenizer = new StringTokenizer(nombreArchivo, caracter);
        String valor = "";
        int i = 0;
        while (stringTokenizer.hasMoreElements()) {
            valor = stringTokenizer.nextElement().toString();
            if (i == indice) {
                break;
            }
            i++;
        }
        return valor;
    }

}
