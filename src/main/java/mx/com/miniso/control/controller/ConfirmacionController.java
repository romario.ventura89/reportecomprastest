package mx.com.miniso.control.controller;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import mx.com.miniso.control.constantes.MensajesVista;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.excel.GeneradorExcel;
import mx.com.miniso.control.excel.GeneradorExcelConfirmacion;
import mx.com.miniso.control.service.ConfirmacionService;
import mx.com.miniso.control.service.colombia.ConfirmacionColombiaService;
import mx.com.miniso.control.util.FechaUtil;
import mx.com.miniso.control.vo.ConfirmacionDetalleVO;
import mx.com.miniso.control.vo.ConfirmacionXMLVO;
import mx.com.miniso.control.vo.ConfirmacionesVO;
import mx.com.miniso.control.vo.FiltrosConfirmacionVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/confirmacion")
public class ConfirmacionController {

    final static Log logger = LogFactory.getLog(ConfirmacionController.class);

    @Autowired
    private ConfirmacionService confirmacionService;

    @RequestMapping(value = "/buscar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> buscar(@RequestBody FiltrosConfirmacionVO filtrosConfirmacionVO) {

        List<ConfirmacionesVO> confirmacionesVO;
        try {
            confirmacionesVO = confirmacionService.buscarConfirmaciones(filtrosConfirmacionVO);
            return new ResponseEntity<>(confirmacionesVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/detalle", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> detalle(@RequestBody ConfirmacionesVO confirmacionVO) {

        try {
            List<ConfirmacionDetalleVO> detalles = confirmacionService.obtenerDetalle(confirmacionVO);
            return new ResponseEntity<>(detalles, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/eliminar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> eliminar(@RequestBody ConfirmacionesVO confirmacionVO) {

        try {
            confirmacionService.eliminar(confirmacionVO);
            return new ResponseEntity<>(confirmacionVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/excel", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(@RequestParam(name = "idConfirmacion") String idConfirmacion, @RequestParam(name = "tipoOrden") String tipoOrden, HttpServletResponse response) {
        try {
            ConfirmacionesVO confirmacionVO = new ConfirmacionesVO();
            confirmacionVO.setIdConfirmacion(idConfirmacion);
            confirmacionVO.setTipoOrden(tipoOrden);

            Confirmacion confirmacion = confirmacionService.buscarNumeroOrden(idConfirmacion);

            List<ConfirmacionDetalleVO> detalles = confirmacionService.obtenerDetalle(confirmacionVO);

            GeneradorExcel generador = new GeneradorExcelConfirmacion(confirmacion, detalles, confirmacionVO.getTipoOrden());
            response.addHeader("Content-disposition", "attachment;filename="
                    + FechaUtil.formateaFechaArchivo(confirmacion.getFechaConfirmacion())
                    + "_"
                    + confirmacion.getNoOrden()
                    + "_CONFIRMED.xlsx");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            OutputStream output = response.getOutputStream();
            output.write(generador.obtenerArchivo());
            output.close();
        } catch (Exception ex) {
            logger.error("Error al procesar el archivo", ex);
        }

    }

    @RequestMapping(value = "/xml", method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public void xml(@RequestParam(name = "idConfirmacion") String idConfirmacion, @RequestParam(name = "tipoOrden") String tipoOrden, HttpServletResponse response) {

        try {
            ConfirmacionesVO confirmacionVO = new ConfirmacionesVO();
            confirmacionVO.setIdConfirmacion(idConfirmacion);
            confirmacionVO.setTipoOrden(tipoOrden);
            Confirmacion confirmacion = confirmacionService.buscarNumeroOrden(idConfirmacion);
            List<ConfirmacionDetalleVO> detalles = confirmacionService.obtenerDetalle(confirmacionVO);
            ConfirmacionXMLVO confirmacionXML = new ConfirmacionXMLVO();
            confirmacionXML.setDetalles(detalles);
            confirmacionXML.setNumeroOrden(confirmacion.getNoOrden());
            confirmacionXML.setFecha(FechaUtil.formateaFecha(confirmacion.getFechaConfirmacion()));
            JAXBContext context = JAXBContext.newInstance(ConfirmacionXMLVO.class);

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.marshal(confirmacionXML, baos);

            response.addHeader("Content-disposition", "attachment;filename="
                    + FechaUtil.formateaFechaArchivo(confirmacion.getFechaConfirmacion())
                    + "_"
                    + confirmacion.getNoOrden()
                    + "_CONFIRMED.xml");
            response.setContentType("application/xml");

            OutputStream output = response.getOutputStream();
            output.write(baos.toByteArray());
            output.close();
        } catch (Exception ex) {
            logger.error("Falló la carga del xml", ex);

        }
    }

}
