package mx.com.miniso.control.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import mx.com.miniso.control.constantes.MensajesVista;
import mx.com.miniso.control.dto.Sociedad;
import mx.com.miniso.control.dto.Usuario;
import mx.com.miniso.control.ex.ValidacionFormularioException;
import mx.com.miniso.control.service.BienvenidaService;
import mx.com.miniso.control.service.CatalogoService;
import mx.com.miniso.control.service.LoginService;
import mx.com.miniso.control.vo.LoginVO;
import mx.com.miniso.control.vo.SociedadVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/bienvenida")
public class BienvenidaController {

    final static Log logger = LogFactory.getLog(BienvenidaController.class);

    @Autowired
    private BienvenidaService bienvenidaService;

    @RequestMapping(value = "/obtenerSociedades", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> obtenerSociedades() {
        try {
            List<SociedadVO> sociedades = bienvenidaService.getCatalogoSociedades();
            return new ResponseEntity<>(sociedades, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error("Error al obtener la sociedades", ex);
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }
}
