package mx.com.miniso.control.controller;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.miniso.control.constantes.Constantes;
import mx.com.miniso.control.excel.GeneradorExcel;
import mx.com.miniso.control.excel.GeneradorReporteConfirmaciones;
import mx.com.miniso.control.service.ReporteComodatoService;
import mx.com.miniso.control.service.ReportesService;


import mx.com.miniso.control.vo.ReporteConfirmacionVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/reportes")
public class ReportesController {

    final static Log logger = LogFactory.getLog(ReportesController.class);

    @Autowired
    private ReportesService reportesService;
    
    @Autowired
    private ReporteComodatoService reporteComodatoService;

    @RequestMapping(value = "/confirmacion", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> confirmacion() {
        List<ReporteConfirmacionVO> reporteConfirmacionVO = reportesService.Confirmacion();
        return new ResponseEntity<>(reporteConfirmacionVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/excelConfirmacionSesion", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> excelConfirmacionSesion(@RequestBody List<ReporteConfirmacionVO> reporteConfirmacionVO, HttpServletRequest request) {
        request.getSession().setAttribute(Constantes.SESION_REPORTE, reporteConfirmacionVO);
        reporteConfirmacionVO = new ArrayList<>();
        return new ResponseEntity<>(reporteConfirmacionVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/excelConfirmacion", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ReporteConfirmacionVO> reporteConfirmacionVO = (List<ReporteConfirmacionVO>) request.getSession().getAttribute(Constantes.SESION_REPORTE);
            GeneradorExcel generador = new GeneradorReporteConfirmaciones(reporteConfirmacionVO);

            response.addHeader("Content-disposition", "attachment;filename="
                    + "REPORTE_CONFIRMACION.xlsx");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            OutputStream output = response.getOutputStream();
            output.write(generador.obtenerArchivo());
            output.close();

        } catch (Exception ex) {
            logger.error("Error al generar el archivo reporte de confirmacion", ex);
        }

    }
}
