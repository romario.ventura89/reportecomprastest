package mx.com.miniso.control.controller;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import mx.com.miniso.control.constantes.MensajesVista;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.excel.GeneradorExcel;
import mx.com.miniso.control.excel.GeneradorExcelFactura;
import mx.com.miniso.control.service.ConfirmacionService;
import mx.com.miniso.control.service.FacturaService;
import mx.com.miniso.control.util.FechaUtil;
import mx.com.miniso.control.vo.FacturaDetalleVO;
import mx.com.miniso.control.vo.FacturaVO;
import mx.com.miniso.control.vo.FacturaXMLVO;
import mx.com.miniso.control.vo.FiltrosFacturaVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/facturas")
public class FacturasController {

    final static Log logger = LogFactory.getLog(FacturasController.class);

    @Autowired
    private ConfirmacionService confirmacionService;

    @Autowired
    private FacturaService facturaService;

    @RequestMapping(value = "/buscar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> buscar(@RequestBody FiltrosFacturaVO filtrosFacturaVO) {

        List<FacturaVO> facturas;
        try {
            facturas = facturaService.buscarFacturas(filtrosFacturaVO);
            return new ResponseEntity<>(facturas, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error("Sucedió un error al obtener las facturas ", ex);
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/detalle", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> detalle(@RequestBody FacturaVO facturaVO) {

        try {
            List<FacturaDetalleVO> detalles = facturaService.obtenerDetalle(facturaVO);
            return new ResponseEntity<>(detalles, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/elimina", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> elimina(@RequestBody FacturaVO facturaVO) {

        try {
            facturaService.eliminar(facturaVO);
            return new ResponseEntity<>(facturaVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/excel", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(@RequestParam(name = "idFactura") String id, HttpServletResponse response) {

        try {
            FacturaVO facturaVO = new FacturaVO();
            facturaVO.setIdFactura(id);

            Factura factura = facturaService.buscarXId(id);
            String contenedor = factura.getContenedor();
            Confirmacion confirmacion = confirmacionService.buscarNumeroOrden(String.valueOf(factura.getIdConfirmacion()));

            List<FacturaDetalleVO> detalles = facturaService.obtenerDetalle(facturaVO);

            GeneradorExcel generador = new GeneradorExcelFactura(confirmacion.getNoOrden(), contenedor, detalles);
            response.addHeader("Content-disposition", "attachment;filename="
                    + FechaUtil.formateaFechaArchivo(factura.getFechaFactura())
                    + "_"
                    + factura.getNoFactura()
                    + "_INVOICE.xlsx");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            OutputStream output = response.getOutputStream();
            output.write(generador.obtenerArchivo());
            output.close();

        } catch (Exception ex) {
            logger.error("Error al procesar el archivo", ex);
        }

    }

    @RequestMapping(value = "/xml", method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public void xml(@RequestParam(name = "idFactura") String id, HttpServletResponse response) {

        try {
            FacturaVO facturaVO = new FacturaVO();
            facturaVO.setIdFactura(id);

            Factura factura = facturaService.buscarXId(id);
            String contenedor = factura.getContenedor();
            Confirmacion confirmacion = confirmacionService.buscarNumeroOrden(String.valueOf(factura.getIdConfirmacion()));
            List<FacturaDetalleVO> detalles = facturaService.obtenerDetalle(facturaVO);
            FacturaXMLVO facturaXML = new FacturaXMLVO();
            facturaXML.setNumeroOrden(confirmacion.getNoOrden());
            facturaXML.setContenedor(contenedor);
            facturaXML.setFecha(FechaUtil.formateaFecha(factura.getFechaFactura()));
            facturaXML.setDetalles(detalles);
            facturaXML.setInvoice(factura.getNoFactura());
            facturaXML.setEta(FechaUtil.formateaFecha(factura.getEta()));
            facturaXML.setEtd(FechaUtil.formateaFecha(factura.getEtd()));
            JAXBContext context = JAXBContext.newInstance(FacturaXMLVO.class);

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.marshal(facturaXML, baos);

            response.addHeader("Content-disposition", "attachment;filename="
                    + FechaUtil.formateaFechaArchivo(factura.getFechaFactura())
                    + "_"
                    + factura.getNoFactura()
                    + "_INVOICE.xml");
            response.setContentType("application/xml");

            OutputStream output = response.getOutputStream();
            output.write(baos.toByteArray());
            output.close();
        } catch (Exception ex) {
            logger.error("Falló la carga del xml", ex);
        }

    }
}
