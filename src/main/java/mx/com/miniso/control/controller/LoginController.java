package mx.com.miniso.control.controller;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpServletRequest;
import mx.com.miniso.control.constantes.Constantes;
import mx.com.miniso.control.constantes.MensajesVista;
import mx.com.miniso.control.constantes.PaginaEnum;
import mx.com.miniso.control.dto.Usuario;
import mx.com.miniso.control.ex.ValidacionFormularioException;
import mx.com.miniso.control.service.LoginService;
import mx.com.miniso.control.vo.LoginVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/login")
public class LoginController {

    final static Log logger = LogFactory.getLog(LoginController.class);

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/entraLogin", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> entraLogin(@RequestBody LoginVO login, HttpServletRequest request) {
        request.getSession().removeAttribute(Constantes.SESION_USUARIO);
        request.getSession().removeAttribute(Constantes.SESION_OBJETO);
        try {
            if (login == null) {
                logger.error("El valor del usuario es nulo.");
                return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
            }

            Usuario usuario = new Usuario();

            usuario.setUserName(login.getUsuario());
            usuario.setPassword(login.getPassword());

            usuario = loginService.obtenerUsuarioXuserName(usuario);
            /*
            String passwordMd5 = MD5(usuario.getPassword()); 
            usuario.setPassword(passwordMd5);
             */

            if (usuario == null) {
                return new ResponseEntity<>("Tu usuario no es válido", HttpStatus.CONFLICT);

            }
            if (!usuario.isEstatus()) {
                return new ResponseEntity<>("El usuario solicitado no se encuentra activo.", HttpStatus.CONFLICT);
            }
            if (usuario.getPassword().equals(login.getPassword())) {
                usuario.setPassword("********");
                request.getSession().setAttribute("USUARIO", usuario);
            } else {
                return new ResponseEntity<>("Tu contraseña no es válida", HttpStatus.CONFLICT);
            }

        } catch (Exception ex) {
            logger.error("Error al obtener la información de Login", ex);
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(login, HttpStatus.OK);
    }

    @RequestMapping(value = "/redirectRol", method = RequestMethod.GET)
    public String redirectRol(HttpServletRequest request) {

        int usuarioChina = 1;
        int usuarioMexico = 2;

        Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
        if (usuario == null) {
            return "redirect:/index.html";
        }

        if (usuario.getRol().getIdRol() == usuarioChina) {

            return "redirect:/sitio/privado/usr/bienvenida.jsp";

        }

        if (usuario.getRol().getIdRol() == usuarioMexico) {
            return "redirect:/sitio/privado/usr/bienvenida.jsp";
        }

        return "";
    }

    @RequestMapping(value = "/menuRol", method = RequestMethod.GET)
    public String menuRol(HttpServletRequest request) {

        int usuarioChina = 1;
        int usuarioMexico = 2;

        Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");

        if (usuario.getRol().getIdRol() == usuarioChina) {
            return "redirect:/sitio/privado/menus/menuChina.jsp";
        }

        if (usuario.getRol().getIdRol() == usuarioMexico) {
            return "redirect:/sitio/privado/menus/menuMexico.jsp";
        }

        return "redirect:/index.html";
    }

    /*
    public static String MD5(String text)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        byte[] md5hash = new byte[32];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        md5hash = md.digest();
        return convertToHex(md5hash);
    }

    private static String convertToHex(byte[] data) { 
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    }

    //remover el objeto de sesión, opción salir
    @RequestMapping(value = "/salir", method = RequestMethod.GET)
    public String salir(HttpServletRequest request) {
        request.getSession().removeAttribute(Constantes.SESION_USUARIO);
        request.getSession().removeAttribute(Constantes.SESION_OBJETO);
        return PaginaEnum.LOGIN.getPath();
    }
     */
}
