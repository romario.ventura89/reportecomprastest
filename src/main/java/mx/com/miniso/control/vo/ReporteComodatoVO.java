
package mx.com.miniso.control.vo;

import java.io.Serializable;
import java.util.List;


public class ReporteComodatoVO implements Serializable{
    private List<ComodatoVO> comodatos;
    private List<ContainerVO> contenedores;

    public List<ComodatoVO> getComodatos() {
        return comodatos;
    }

    public void setComodatos(List<ComodatoVO> comodatos) {
        this.comodatos = comodatos;
    }

    public List<ContainerVO> getContenedores() {
        return contenedores;
    }

    public void setContenedores(List<ContainerVO> contenedores) {
        this.contenedores = contenedores;
    }
    
    
    
}
