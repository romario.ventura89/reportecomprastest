package mx.com.miniso.control.vo;

import java.io.Serializable;

public class FaltanteOrderVO implements Serializable {

    private String sku;
    private String noOrden;
    private int confirmaciones;
    private int faltantes;
    private String faltantesTXT;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getNoOrden() {
        return noOrden;
    }

    public void setNoOrden(String noOrden) {
        this.noOrden = noOrden;
    }

    public int getConfirmaciones() {
        return confirmaciones;
    }

    public void setConfirmaciones(int confirmaciones) {
        this.confirmaciones = confirmaciones;
    }

    public int getFaltantes() {
        return faltantes;
    }

    public void setFaltantes(int faltantes) {
        this.faltantes = faltantes;
    }

    public String getFaltantesTXT() {
        return faltantesTXT;
    }

    public void setFaltantesTXT(String faltantesTXT) {
        this.faltantesTXT = faltantesTXT;
    }

}
