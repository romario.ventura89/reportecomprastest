package mx.com.miniso.control.vo;

import java.io.Serializable;

public class CargaFacturaDatosVO implements Serializable {

    private String sku;
    private String nombre;
    private String piezas;
    private String precio;
    private String precioTotal;

    private String piezasErrorTxt;
    private String precioErrorTxt;

    private boolean piezasError;
    private boolean precioError;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPiezas() {
        return piezas;
    }

    public void setPiezas(String piezas) {
        this.piezas = piezas;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(String precioTotal) {
        this.precioTotal = precioTotal;
    }

    public String getPiezasErrorTxt() {
        return piezasErrorTxt;
    }

    public void setPiezasErrorTxt(String piezasErrorTxt) {
        this.piezasErrorTxt = piezasErrorTxt;
    }

    public String getPrecioErrorTxt() {
        return precioErrorTxt;
    }

    public void setPrecioErrorTxt(String precioErrorTxt) {
        this.precioErrorTxt = precioErrorTxt;
    }

    public boolean isPiezasError() {
        return piezasError;
    }

    public void setPiezasError(boolean piezasError) {
        this.piezasError = piezasError;
    }

    public boolean isPrecioError() {
        return precioError;
    }

    public void setPrecioError(boolean precioError) {
        this.precioError = precioError;
    }

    @Override
    public String toString() {
        return "CargaFacturaDatosVO{" + "sku=" + sku + ", nombre=" + nombre + ", piezas=" + piezas + ", precio=" + precio + ", precioTotal=" + precioTotal + ", piezasErrorTxt=" + piezasErrorTxt + ", precioErrorTxt=" + precioErrorTxt + ", piezasError=" + piezasError + ", precioError=" + precioError + '}';
    }
}
