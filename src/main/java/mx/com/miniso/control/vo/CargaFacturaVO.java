package mx.com.miniso.control.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CargaFacturaVO implements Serializable {
    private String nombreArchivo;
    private String fechaFactura;
    private String numeroOrden;
    private String numeroFactura;
    private String contenedor;
    private String etd;
    private String eta;

    private boolean remplazar;

    private List<CargaFacturaDatosVO> datos;
    private boolean error;

    private String fechaFacturaErrorTXT;
    private String numeroOrdenErrorTXT;
    private String numeroFacturaErrorTXT;
    private String contenedorErrorTXT;
    private String etdErrorTXT;
    private String etaErrorTXT;
    
    

    private boolean fechaFacturaError;
    private boolean numeroOrdenError;
    private String numeroOrdenErrorTxt;

    
    
    private boolean numeroFacturaError;
    private boolean contenedorError;
    private boolean etdError;
    private boolean etaError;

    private boolean errorListado;

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public CargaFacturaVO() {
        this.datos = new ArrayList<>(0);
    }

    public String getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getContenedor() {
        return contenedor;
    }

    public void setContenedor(String contenedor) {
        this.contenedor = contenedor;
    }

    public List<CargaFacturaDatosVO> getDatos() {
        return datos;
    }

    public void setDatos(List<CargaFacturaDatosVO> datos) {
        this.datos = datos;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isRemplazar() {
        return remplazar;
    }

    public void setRemplazar(boolean remplazar) {
        this.remplazar = remplazar;
    }

    public String getFechaFacturaErrorTXT() {
        return fechaFacturaErrorTXT;
    }

    public void setFechaFacturaErrorTXT(String fechaFacturaErrorTXT) {
        this.fechaFacturaErrorTXT = fechaFacturaErrorTXT;
    }

    public String getNumeroOrdenErrorTXT() {
        return numeroOrdenErrorTXT;
    }

    public void setNumeroOrdenErrorTXT(String numeroOrdenErrorTXT) {
        this.numeroOrdenErrorTXT = numeroOrdenErrorTXT;
    }

    public String getNumeroFacturaErrorTXT() {
        return numeroFacturaErrorTXT;
    }

    public void setNumeroFacturaErrorTXT(String numeroFacturaErrorTXT) {
        this.numeroFacturaErrorTXT = numeroFacturaErrorTXT;
    }

    public String getContenedorErrorTXT() {
        return contenedorErrorTXT;
    }

    public void setContenedorErrorTXT(String contenedorErrorTXT) {
        this.contenedorErrorTXT = contenedorErrorTXT;
    }

    public String getEtdErrorTXT() {
        return etdErrorTXT;
    }

    public void setEtdErrorTXT(String etdErrorTXT) {
        this.etdErrorTXT = etdErrorTXT;
    }

    public String getEtaErrorTXT() {
        return etaErrorTXT;
    }

    public void setEtaErrorTXT(String etaErrorTXT) {
        this.etaErrorTXT = etaErrorTXT;
    }

    public boolean isFechaFacturaError() {
        return fechaFacturaError;
    }

    public void setFechaFacturaError(boolean fechaFacturaError) {
        this.fechaFacturaError = fechaFacturaError;
    }

    public boolean isNumeroOrdenError() {
        return numeroOrdenError;
    }

    public void setNumeroOrdenError(boolean numeroOrdenError) {
        this.numeroOrdenError = numeroOrdenError;
    }

    public boolean isNumeroFacturaError() {
        return numeroFacturaError;
    }

    public void setNumeroFacturaError(boolean numeroFacturaError) {
        this.numeroFacturaError = numeroFacturaError;
    }

    public boolean isContenedorError() {
        return contenedorError;
    }

    public void setContenedorError(boolean contenedorError) {
        this.contenedorError = contenedorError;
    }

    public boolean isEtdError() {
        return etdError;
    }

    public void setEtdError(boolean etdError) {
        this.etdError = etdError;
    }

    public boolean isEtaError() {
        return etaError;
    }

    public void setEtaError(boolean etaError) {
        this.etaError = etaError;
    }

    public boolean isErrorListado() {
        return errorListado;
    }

    public void setErrorListado(boolean errorListado) {
        this.errorListado = errorListado;
    }
    
    public String getNumeroOrdenErrorTxt() {
        return numeroOrdenErrorTxt;
    }

    public void setNumeroOrdenErrorTxt(String numeroOrdenErrorTxt) {
        this.numeroOrdenErrorTxt = numeroOrdenErrorTxt;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }
    
}
