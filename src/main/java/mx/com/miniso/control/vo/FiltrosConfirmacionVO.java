package mx.com.miniso.control.vo;

import java.io.Serializable;

public class FiltrosConfirmacionVO implements Serializable {

    private String sku;
    private String fecha;
    private String numeroOrden;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

}
