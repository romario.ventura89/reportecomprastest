package mx.com.miniso.control.vo;

import java.io.Serializable;

public class ConfirmacionesVO implements Serializable {

    private String idConfirmacion;
    private String numeroOrden;
    private String tipoOrden;
    private String fecha;
    private String productos;
    private String entregadas;
    private String solicitadas;
    private String total;
    private int idTipoOrden;

    public String getIdConfirmacion() {
        return idConfirmacion;
    }

    public void setIdConfirmacion(String idConfirmacion) {
        this.idConfirmacion = idConfirmacion;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getTipoOrden() {
        return tipoOrden;
    }

    public void setTipoOrden(String tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getProductos() {
        return productos;
    }

    public void setProductos(String productos) {
        this.productos = productos;
    }

    public String getEntregadas() {
        return entregadas;
    }

    public void setEntregadas(String entregadas) {
        this.entregadas = entregadas;
    }

    public String getSolicitadas() {
        return solicitadas;
    }

    public void setSolicitadas(String solicitadas) {
        this.solicitadas = solicitadas;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public int getIdTipoOrden() {
        return idTipoOrden;
    }

    public void setIdTipoOrden(int idTipoOrden) {
        this.idTipoOrden = idTipoOrden;
    }

}
