/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miniso.control.vo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KRIOSOFT 2
 */
public class TabCargaConfirmacionVO {
    private List<CargaConfirmacionVO> cargaConfirmacionVOListError;
    private List<CargaConfirmacionVO> cargaConfirmacionVOListExitoso;
    
    public TabCargaConfirmacionVO() {
        this.cargaConfirmacionVOListError = new ArrayList<>(0);
        this.cargaConfirmacionVOListExitoso = new ArrayList<>(0);
    }

    public List<CargaConfirmacionVO> getCargaConfirmacionVOListError() {
        return cargaConfirmacionVOListError;
    }

    public void setCargaConfirmacionVOListError(List<CargaConfirmacionVO> cargaConfirmacionVOListError) {
        this.cargaConfirmacionVOListError = cargaConfirmacionVOListError;
    }

    public List<CargaConfirmacionVO> getCargaConfirmacionVOListExitoso() {
        return cargaConfirmacionVOListExitoso;
    }

    public void setCargaConfirmacionVOListExitoso(List<CargaConfirmacionVO> cargaConfirmacionVOListExitoso) {
        this.cargaConfirmacionVOListExitoso = cargaConfirmacionVOListExitoso;
    }

    @Override
    public String toString() {
        return "TabCargaConfirmacionVO{" + "cargaConfirmacionVOListError=" + cargaConfirmacionVOListError + ", cargaConfirmacionVOListExitoso=" + cargaConfirmacionVOListExitoso + '}';
    }

    
    
}
