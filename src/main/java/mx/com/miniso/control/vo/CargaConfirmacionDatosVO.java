package mx.com.miniso.control.vo;

import java.io.Serializable;

public class CargaConfirmacionDatosVO implements Serializable {

    private String sku;
    private String nombre;
    private String piezasSolicitadas;
    private String piezasEntregadas;
    private String precio;
    private String precioTotal;
    private String cbm;

    private boolean skuError;
    private boolean nombreError;
    private boolean piezasSolicitadasError;
    private boolean piezasEntregadasError;
    private boolean precioError;

    private String skuErrorTXT;

    private boolean notOrder;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPiezasSolicitadas() {
        return piezasSolicitadas;
    }

    public void setPiezasSolicitadas(String piezasSolicitadas) {
        this.piezasSolicitadas = piezasSolicitadas;
    }

    public String getPiezasEntregadas() {
        return piezasEntregadas;
    }

    public void setPiezasEntregadas(String piezasEntregadas) {
        this.piezasEntregadas = piezasEntregadas;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(String precioTotal) {
        this.precioTotal = precioTotal;
    }

    public boolean isNotOrder() {
        return notOrder;
    }

    public void setNotOrder(boolean notOrder) {
        this.notOrder = notOrder;
    }

    public boolean isSkuError() {
        return skuError;
    }

    public void setSkuError(boolean skuError) {
        this.skuError = skuError;
    }

    public boolean isNombreError() {
        return nombreError;
    }

    public void setNombreError(boolean nombreError) {
        this.nombreError = nombreError;
    }

    public boolean isPiezasSolicitadasError() {
        return piezasSolicitadasError;
    }

    public void setPiezasSolicitadasError(boolean piezasSolicitadasError) {
        this.piezasSolicitadasError = piezasSolicitadasError;
    }

    public boolean isPiezasEntregadasError() {
        return piezasEntregadasError;
    }

    public void setPiezasEntregadasError(boolean piezasEntregadasError) {
        this.piezasEntregadasError = piezasEntregadasError;
    }

    public boolean isPrecioError() {
        return precioError;
    }

    public void setPrecioError(boolean precioError) {
        this.precioError = precioError;
    }

    public String getSkuErrorTXT() {
        return skuErrorTXT;
    }

    public void setSkuErrorTXT(String skuErrorTXT) {
        this.skuErrorTXT = skuErrorTXT;
    }

    public String getCbm() {
        return cbm;
    }

    public void setCbm(String cbm) {
        this.cbm = cbm;
    }
}
