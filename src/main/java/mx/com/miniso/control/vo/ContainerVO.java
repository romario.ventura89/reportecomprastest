package mx.com.miniso.control.vo;

import java.io.Serializable;

public class ContainerVO implements Serializable {

    private String idFactura;
    private String nombreContenedor;

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public String getNombreContenedor() {
        return nombreContenedor;
    }

    public void setNombreContenedor(String nombreContenedor) {
        this.nombreContenedor = nombreContenedor;
    }

}
