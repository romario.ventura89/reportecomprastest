package mx.com.miniso.control.vo;

import java.io.Serializable;

public class FacturaComodatoVO implements Serializable {

    private String piezas;
    private String sku;
    private String idFactura;

    public String getPiezas() {
        return piezas;
    }

    public void setPiezas(String piezas) {
        this.piezas = piezas;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

}
