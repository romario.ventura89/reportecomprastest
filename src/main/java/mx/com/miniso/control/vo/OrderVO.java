package mx.com.miniso.control.vo;

import java.io.Serializable;

public class OrderVO implements Serializable {

    private String idComfirmacion;
    private String noOrden;

    public String getIdComfirmacion() {
        return idComfirmacion;
    }

    public void setIdComfirmacion(String idComfirmacion) {
        this.idComfirmacion = idComfirmacion;
    }

    public String getNoOrden() {
        return noOrden;
    }

    public void setNoOrden(String noOrden) {
        this.noOrden = noOrden;
    }

}
