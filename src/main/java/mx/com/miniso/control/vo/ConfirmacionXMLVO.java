package mx.com.miniso.control.vo;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ConfirmacionXMLVO {

    private String numeroOrden;
    private String fecha;
    private List<ConfirmacionDetalleVO> detalles;

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<ConfirmacionDetalleVO> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<ConfirmacionDetalleVO> detalles) {
        this.detalles = detalles;
    }
}
