package mx.com.miniso.control.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ComodatoVO implements Serializable {

    private String sku;
    private String productName;
    private String numeroOrden;
    private String piezasConfirmadas;
    private String totalFacturas;
    private String noEnviados;

    private List<ContenedorVO> contenedores;

    public ComodatoVO() {
        this.contenedores = new ArrayList<>(0);
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getPiezasConfirmadas() {
        return piezasConfirmadas;
    }

    public void setPiezasConfirmadas(String piezasConfirmadas) {
        this.piezasConfirmadas = piezasConfirmadas;
    }

    public String getTotalFacturas() {
        return totalFacturas;
    }

    public void setTotalFacturas(String totalFacturas) {
        this.totalFacturas = totalFacturas;
    }

    public String getNoEnviados() {
        return noEnviados;
    }

    public void setNoEnviados(String noEnviados) {
        this.noEnviados = noEnviados;
    }

    public List<ContenedorVO> getContenedores() {
        return contenedores;
    }

    public void setContenedores(List<ContenedorVO> contenedores) {
        this.contenedores = contenedores;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    
}
