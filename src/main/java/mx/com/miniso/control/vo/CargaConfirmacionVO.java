package mx.com.miniso.control.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CargaConfirmacionVO implements Serializable {

    private String fechaConfirmacion;
    private String numeroOrden;

    /*private List<CargaConfirmacionDatosVO> general;
    private List<CargaConfirmacionDatosVO> qianhai;
    private List<CargaConfirmacionDatosVO> pingshan;*/
    
    private Map<String , List<CargaConfirmacionDatosVO>> cargaInformacion;

    private boolean fechaConfirmacionError;
    private boolean numeroOrdenError;

    private String fechaConfirmacionErrorTxt;
    private String numeroOrdenErrorTxt;

    private boolean error;
    private boolean remplazar;

    private boolean errorListado;

    public CargaConfirmacionVO() {
        /*
        this.general = new ArrayList<>(0);
        this.qianhai = new ArrayList<>(0);
        this.pingshan = new ArrayList<>(0);*/
        this.cargaInformacion = new HashMap<>();
    }

    public String getFechaConfirmacion() {
        return fechaConfirmacion;
    }

    public void setFechaConfirmacion(String fechaConfirmacion) {
        this.fechaConfirmacion = fechaConfirmacion;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }
    
    public Map<String , List<CargaConfirmacionDatosVO>> getCargaInformacion() {
        return cargaInformacion;
    }
    
    public void setCargaInformacion(Map<String , List<CargaConfirmacionDatosVO>> cargaInformacion) {
        this.cargaInformacion = cargaInformacion;
    }
    
    /*
    public List<CargaConfirmacionDatosVO> getGeneral() {
        return general;
    }

    public void setGeneral(List<CargaConfirmacionDatosVO> general) {
        this.general = general;
    }

    public List<CargaConfirmacionDatosVO> getQianhai() {
        return qianhai;
    }

    public void setQianhai(List<CargaConfirmacionDatosVO> qianhai) {
        this.qianhai = qianhai;
    }

    public List<CargaConfirmacionDatosVO> getPingshan() {
        return pingshan;
    }

    public void setPingshan(List<CargaConfirmacionDatosVO> pingshan) {
        this.pingshan = pingshan;
    }*/

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isRemplazar() {
        return remplazar;
    }

    public void setRemplazar(boolean remplazar) {
        this.remplazar = remplazar;
    }

    public boolean isFechaConfirmacionError() {
        return fechaConfirmacionError;
    }

    public void setFechaConfirmacionError(boolean fechaConfirmacionError) {
        this.fechaConfirmacionError = fechaConfirmacionError;
    }

    public boolean isNumeroOrdenError() {
        return numeroOrdenError;
    }

    public void setNumeroOrdenError(boolean numeroOrdenError) {
        this.numeroOrdenError = numeroOrdenError;
    }

    public String getFechaConfirmacionErrorTxt() {
        return fechaConfirmacionErrorTxt;
    }

    public void setFechaConfirmacionErrorTxt(String fechaConfirmacionErrorTxt) {
        this.fechaConfirmacionErrorTxt = fechaConfirmacionErrorTxt;
    }

    public String getNumeroOrdenErrorTxt() {
        return numeroOrdenErrorTxt;
    }

    public void setNumeroOrdenErrorTxt(String numeroOrdenErrorTxt) {
        this.numeroOrdenErrorTxt = numeroOrdenErrorTxt;
    }

    public boolean isErrorListado() {
        return errorListado;
    }

    public void setErrorListado(boolean errorListado) {
        this.errorListado = errorListado;
    }
    
}
