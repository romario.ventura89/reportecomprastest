package mx.com.miniso.control.vo;

import java.io.Serializable;
import java.util.List;

public class ReportePedidosVO implements Serializable {

    private List<PedidosVO> pedidos;
    private List<OrderVO> ordenes;


    public List<PedidosVO> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<PedidosVO> pedidos) {
        this.pedidos = pedidos;
    }

    public List<OrderVO> getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(List<OrderVO> ordenes) {
        this.ordenes = ordenes;
    }

}
