package mx.com.miniso.control.vo;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FacturaXMLVO {

    private String numeroOrden;
    private String contenedor;
    private String fecha;
    private String invoice;
    private String eta;
    private String etd;
    private List<FacturaDetalleVO> detalles;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getContenedor() {
        return contenedor;
    }

    public void setContenedor(String contenedor) {
        this.contenedor = contenedor;
    }

    public List<FacturaDetalleVO> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<FacturaDetalleVO> detalles) {
        this.detalles = detalles;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

}
