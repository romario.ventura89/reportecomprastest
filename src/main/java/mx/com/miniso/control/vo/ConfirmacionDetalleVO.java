package mx.com.miniso.control.vo;

import java.io.Serializable;

public class ConfirmacionDetalleVO implements Serializable {

    private boolean general;
    private boolean qianhai;
    private boolean pingshan;

    private String sku;
    private String descripcion;
    private String piezasEntregadas;
    private String piezasSolicitadas;
    private String cartoncbm;
    private String precioUnitario;
    private String precioTotal;
    private boolean notOrder;

    public boolean isGeneral() {
        return general;
    }

    public void setGeneral(boolean general) {
        this.general = general;
    }

    public boolean isQianhai() {
        return qianhai;
    }

    public void setQianhai(boolean qianhai) {
        this.qianhai = qianhai;
    }

    public boolean isPingshan() {
        return pingshan;
    }

    public void setPingshan(boolean pingshan) {
        this.pingshan = pingshan;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPiezasEntregadas() {
        return piezasEntregadas;
    }

    public void setPiezasEntregadas(String piezasEntregadas) {
        this.piezasEntregadas = piezasEntregadas;
    }

    public String getPiezasSolicitadas() {
        return piezasSolicitadas;
    }

    public void setPiezasSolicitadas(String piezasSolicitadas) {
        this.piezasSolicitadas = piezasSolicitadas;
    }

    public String getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(String precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public String getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(String precioTotal) {
        this.precioTotal = precioTotal;
    }

    public boolean isNotOrder() {
        return notOrder;
    }

    public void setNotOrder(boolean notOrder) {
        this.notOrder = notOrder;
    }

    public String getCartoncbm() {
        return cartoncbm;
    }

    public void setCartoncbm(String cartoncbm) {
        this.cartoncbm = cartoncbm;
    }

}
