/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miniso.control.vo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KRIOSOFT 2
 */
public class TabCargaFacturaVO {

    private List<CargaFacturaVO> cargaFacturaVOListExitoso;
    private List<CargaFacturaVO> cargaFacturaVOListError;

    public TabCargaFacturaVO() {
        this.cargaFacturaVOListError = new ArrayList<>(0);
        this.cargaFacturaVOListExitoso = new ArrayList<>(0);
    }

    public List<CargaFacturaVO> getCargaFacturaVOListExitoso() {
        return cargaFacturaVOListExitoso;
    }

    public void setCargaFacturaVOListExitoso(List<CargaFacturaVO> cargaFacturaVOListExitoso) {
        this.cargaFacturaVOListExitoso = cargaFacturaVOListExitoso;
    }

    public List<CargaFacturaVO> getCargaFacturaVOListError() {
        return cargaFacturaVOListError;
    }

    public void setCargaFacturaVOListError(List<CargaFacturaVO> cargaFacturaVOListError) {
        this.cargaFacturaVOListError = cargaFacturaVOListError;
    }

    @Override
    public String toString() {
        return "TabCargaFacturaVO{" + "cargaFacturaVOListExitoso=" + cargaFacturaVOListExitoso + ", cargaFacturaVOListError=" + cargaFacturaVOListError + '}';
    }
    
    
    
    

}
