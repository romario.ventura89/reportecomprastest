package mx.com.miniso.control.dao;

import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.dto.ConfirmacionProducto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class ConfirmacionProductoDao extends BaseDao<ConfirmacionProducto> {

    private final static Log LOG = LogFactory.getLog(ConfirmacionProductoDao.class);

    @Autowired
    @Qualifier(value="sessionFactory")
    private SessionFactory sessionFactory;

    public ConfirmacionProducto guardar(ConfirmacionProducto confirmacionProducto) {
        guardar(sessionFactory, confirmacionProducto);
        return confirmacionProducto;
    }

    public void eliminaXConfirmacion(Confirmacion confirmacion) {
        Transaction tx = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Query q = session.createQuery("delete from confirmacionProducto cp where cp.idConfirmacion = :idConfirmacion");
            q.setParameter("idConfirmacion", confirmacion.getIdConfirmacion());
            
            q.executeUpdate();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.error("Error al eliminar... ", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
     public void eliminaXConfirmacionTipo(Confirmacion confirmacion, String tipoOrden) {
        Transaction tx = null;
        Session session = null;
        //Long idTOrden = new Long(idTipoOrden);
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Query q = session.createQuery("delete from confirmacionProducto cp where cp.idConfirmacion = :idConfirmacion and cp.tipoOrden = :tipoOrden");
            q.setParameter("idConfirmacion", confirmacion.getIdConfirmacion());
             q.setParameter("tipoOrden", tipoOrden);
            
            q.executeUpdate();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.error("Error al eliminar... ", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public int cuentaProductos(Confirmacion confirmacion) {
         Session session = null;
         Long numeroProductos = new Long(0);
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("select count(idConfirmacionProducto) from confirmacionProducto c where c.idConfirmacion = :idConfirmacion");
            query.setParameter("idConfirmacion", confirmacion.getIdConfirmacion());
            numeroProductos = (Long) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return numeroProductos.intValue();
    }
}
