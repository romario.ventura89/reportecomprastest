package mx.com.miniso.control.dao.colombia;

import mx.com.miniso.control.dao.*;
import java.util.List;
import mx.com.miniso.control.dto.Factura;
import mx.com.miniso.control.dto.FacturaProducto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class FacturaProductoColombiaDao extends BaseDao<FacturaProducto> {

    private final static Log LOG = LogFactory.getLog(FacturaProductoColombiaDao.class);

    @Autowired
    @Qualifier("sessionFactoryColombia")
    private SessionFactory sessionFactory;

    public FacturaProducto guardar(FacturaProducto faturaProducto) {
        guardar(sessionFactory, faturaProducto);
        return faturaProducto;
    }

    public void eliminaXFactura(Factura factura) {
        Transaction tx = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Query q = session.createQuery("delete from facturaProducto fp where fp.idFactura = :idFactura");
            q.setParameter("idFactura", factura.getIdFactura());
            q.executeUpdate();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.error("Error al eliminar... ", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public List<FacturaProducto> obtenerProductosIdFactura(long idFactura) {
        Session session = null;
        List<FacturaProducto> facturasProducto = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from facturaProducto f where f.idFactura = :idFactura");
            query.setParameter("idFactura", idFactura);
            facturasProducto = query.list();

        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return facturasProducto;
    }

}
