package mx.com.miniso.control.dao.colombia;

import mx.com.miniso.control.dao.*;
import java.sql.Date;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Map;
import mx.com.miniso.control.vo.ConfirmacionesVO;
import mx.com.miniso.control.vo.FiltrosConfirmacionVO;

import java.util.List;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.util.FechaUtil;
import mx.com.miniso.control.vo.ConfirmacionDetalleVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
public class ConfirmacionColombiaDao extends BaseDao<Confirmacion> {

    private final static Log LOG = LogFactory.getLog(ConfirmacionColombiaDao.class);

    @Autowired
    @Qualifier("sessionFactoryColombia")
    private SessionFactory sessionFactory;

    public List<ConfirmacionesVO> obtenerTodos() throws Exception {
        Session session = null;
        List<ConfirmacionesVO> confirmacionList = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from confirmacion c ");
            confirmacionList = query.list();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return confirmacionList;
    }

    public List<ConfirmacionesVO> obtenerXParametros(FiltrosConfirmacionVO filtrosConfirmacionVO) throws Exception {

        Session session = null;
        List<ConfirmacionesVO> confirmacionList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append("select DISTINCT c.idConfirmacion,c.noOrden, c.fechaConfirmacion, p.tipoOrden ");
            sb.append("from confirmacion c, confirmacionProducto p LEFT join productoCompra cp on p.idProductoCompra = cp.idProductoCompra ");
            sb.append("where c.idConfirmacion = p.idConfirmacion ");
            if (!StringUtils.isEmpty(filtrosConfirmacionVO.getNumeroOrden())) {
                sb.append(" and c.noOrden like '%");
                sb.append(filtrosConfirmacionVO.getNumeroOrden().toLowerCase().trim());
                sb.append("%' ");
            }
            if (!StringUtils.isEmpty(filtrosConfirmacionVO.getFecha())) {
                sb.append(" and c.fechaConfirmacion = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosConfirmacionVO.getFecha()));
                sb.append("' ");
            }
            if (!StringUtils.isEmpty(filtrosConfirmacionVO.getSku())) {
                sb.append(" and cp.sku like '%");
                sb.append(filtrosConfirmacionVO.getSku());
                sb.append("%'");
            }
            sb.append(" order by c.fechaConfirmacion, c.noOrden");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                ConfirmacionesVO confirmacionesVO = new ConfirmacionesVO();
                confirmacionesVO.setIdConfirmacion(String.valueOf(mapa.get("idConfirmacion")));
                confirmacionesVO.setNumeroOrden(String.valueOf(mapa.get("noOrden")));
                confirmacionesVO.setFecha(FechaUtil.formateaFecha((Date) mapa.get("fechaConfirmacion")));
                //confirmacionesVO.setIdTipoOrden(Integer.parseInt(String.valueOf(mapa.get("idTipoOrden"))));
                
                confirmacionesVO.setTipoOrden(String.valueOf(mapa.get("tipoOrden")));
                /*
                if (confirmacionesVO.getIdTipoOrden() == 1) {
                    confirmacionesVO.setTipoOrden("GENERAL");
                }
                if (2 == confirmacionesVO.getIdTipoOrden()) {
                    confirmacionesVO.setTipoOrden("PINGSHAN");
                }
                if (3 == confirmacionesVO.getIdTipoOrden()) {
                    confirmacionesVO.setTipoOrden("QIANHAI");
                }*/
                confirmacionList.add(confirmacionesVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return confirmacionList;
    }

    public Confirmacion guardar(Confirmacion confirmacion) {
        guardar(sessionFactory, confirmacion);
        return confirmacion;
    }

    public ConfirmacionesVO obtenTotalesMisConfirmaciones(ConfirmacionesVO confirmacionVO) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery("select count(idConfirmacionProducto) productos, sum(piezasEntregadas) entregadas, sum(piezasSolicitadas) solicitadas, sum(precioTotal) total from confirmacionProducto where idConfirmacion = " + confirmacionVO.getIdConfirmacion() + " and tipoOrden = '" + confirmacionVO.getTipoOrden()+"'");
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {

                confirmacionVO.setProductos(NumberFormat.getInstance().format(mapa.get("productos")));
                confirmacionVO.setSolicitadas(NumberFormat.getInstance().format(mapa.get("solicitadas")));
                confirmacionVO.setEntregadas(NumberFormat.getInstance().format(mapa.get("entregadas")));
                confirmacionVO.setTotal("¥ " + NumberFormat.getInstance().format(mapa.get("total")));
                break;
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return confirmacionVO;
    }

    public ConfirmacionesVO obtenTipoOrden(ConfirmacionesVO confirmacionVO) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery("select DISTINCT(tipoOrden) from confirmacionProducto where idConfirmacion = " + confirmacionVO.getIdConfirmacion());
            List<String> resulSet = query.list();
            String res = "";

            for (String valor : resulSet) {
                
                res += valor+", ";
                
                /*
                if (valor == 1) {
                    res += "GENERAL, ";
                }
                if (2 == valor) {
                    res += "PINGSHAN, ";
                }
                if (3 == valor) {
                    res += "QIANHAI, ";
                }*/

            }
            res = res.substring(0, res.length() - 2);
            confirmacionVO.setTipoOrden(res);

        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return confirmacionVO;
    }

    public List<ConfirmacionDetalleVO> obtenDetalleMisConfirmaciones(ConfirmacionesVO confirmacionVO) {
        Session session = null;
        List<ConfirmacionDetalleVO> detalles = new ArrayList<>(0);
        StringBuilder sb = new StringBuilder();
        sb.append("select cp.sku, cp.descripcion, p.piezasEntregadas, p.piezasSolicitadas,p.cbm ,p.precioUnitario, p.precioTotal, p.notOrder, p.tipoOrden ");
        sb.append("from  confirmacionProducto p LEFT join productoCompra cp on p.idProductoCompra = cp.idProductoCompra ");
        sb.append("where p.idConfirmacion = ");
        sb.append(confirmacionVO.getIdConfirmacion());
        sb.append(" and tipoOrden = '");
        sb.append(confirmacionVO.getTipoOrden());
        sb.append("'");
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                ConfirmacionDetalleVO detalle = new ConfirmacionDetalleVO();
                detalle.setSku(String.valueOf(mapa.get("sku")));
                detalle.setDescripcion(String.valueOf(mapa.get("descripcion")));
                detalle.setPiezasEntregadas(NumberFormat.getInstance().format(mapa.get("piezasEntregadas")));
                detalle.setPiezasSolicitadas(NumberFormat.getInstance().format(mapa.get("piezasSolicitadas")));
                detalle.setCartoncbm(String.valueOf(mapa.get("cbm")));
                detalle.setPrecioUnitario("¥ " + NumberFormat.getInstance().format(mapa.get("precioUnitario")));
                detalle.setPrecioTotal("¥ " + NumberFormat.getInstance().format(mapa.get("precioTotal")));
                boolean notOrder = (Boolean) mapa.get("notOrder");
                if (notOrder) {
                    detalle.setNotOrder(notOrder);
                }
                detalle.setGeneral(true);
                
                
                /*int idTipoOrder = (Integer) mapa.get("tipoOrden");
                
                detalle.setPingshan(2 == idTipoOrder);
                detalle.setQianhai(3 == idTipoOrder);*/
                detalles.add(detalle);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return detalles;
    }

    public Confirmacion obtenerXID(Confirmacion confirmacion) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from confirmacion c where c.idConfirmacion = :idConfirmacion");
            query.setParameter("idConfirmacion", confirmacion.getIdConfirmacion());
            confirmacion = (Confirmacion) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return confirmacion;
    }

    public Confirmacion obtenerXNumeroOrden(Confirmacion confirmacion) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from confirmacion c where c.noOrden = :noOrden");
            query.setParameter("noOrden", confirmacion.getNoOrden());
            confirmacion = (Confirmacion) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return confirmacion;
    }

    public void borrar(Confirmacion confirmacion) {
        delete(sessionFactory, confirmacion);
    }

    public void actualizar(Confirmacion confirmacion) {
        update(sessionFactory, confirmacion);
    }

}
