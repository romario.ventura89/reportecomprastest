package mx.com.miniso.control.dao.colombia;

import mx.com.miniso.control.dao.*;
import java.sql.Date;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mx.com.miniso.control.dto.HistoricoFactura;
import mx.com.miniso.control.util.FechaUtil;
import mx.com.miniso.control.vo.FacturaDetalleVO;
import mx.com.miniso.control.vo.FacturaVO;
import mx.com.miniso.control.vo.FiltrosFacturaVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
public class HistoricoFacturaColombiaDao extends BaseDao<HistoricoFactura> {

    private final static Log LOG = LogFactory.getLog(HistoricoFacturaColombiaDao.class);

    @Autowired
    @Qualifier("sessionFactoryColombia")
    private SessionFactory sessionFactory;

    public HistoricoFactura guardar(HistoricoFactura historicoFactura) {
        LOG.info("Guardando el historico " + historicoFactura);
        guardar(sessionFactory, historicoFactura);
        return historicoFactura;
    }

    public List<FacturaVO> obtenerXParametros(FiltrosFacturaVO filtrosFacturaVO) {
        Session session = null;
        List<FacturaVO> facturas = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append("select distinct(f.idFactura), f.fechaFactura, f.contenedor, f.noFactura, f.eta,f.etd ");
            sb.append("from  historicoFactura f, historicoFacturaProducto p LEFT join productoCompra cp on p.idProductoCompra = cp.idProductoCompra ");
            sb.append("where f.idFactura = p.idFactura ");

            if (!StringUtils.isEmpty(filtrosFacturaVO.getFecha())) {
                sb.append(" and f.fechaFactura = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosFacturaVO.getFecha()));
                sb.append("' ");
            }
            if (!StringUtils.isEmpty(filtrosFacturaVO.getSku())) {
                sb.append(" and cp.sku like '%");
                sb.append(filtrosFacturaVO.getSku());
                sb.append("%'");
            }

            if (!StringUtils.isEmpty(filtrosFacturaVO.getContenedor())) {
                sb.append(" and f.contenedor like '%");
                sb.append(filtrosFacturaVO.getContenedor().trim().toLowerCase());
                sb.append("%'");
            }

            if (!StringUtils.isEmpty(filtrosFacturaVO.getFactura())) {
                sb.append(" and f.noFactura like '%");
                sb.append(filtrosFacturaVO.getFactura().trim().toLowerCase());
                sb.append("%'");
            }

            if (!StringUtils.isEmpty(filtrosFacturaVO.getEta())) {
                sb.append(" and f.eta = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosFacturaVO.getEta()));
                sb.append("'");
            }

            if (!StringUtils.isEmpty(filtrosFacturaVO.getEtd())) {
                sb.append(" and f.etd = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosFacturaVO.getEtd()));
                sb.append("'");
            }

            sb.append(" order by f.fechaFactura");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                FacturaVO facturaVO = new FacturaVO();
                facturaVO.setIdFactura(String.valueOf(mapa.get("idFactura")));
                facturaVO.setFecha(FechaUtil.formateaFecha((Date) mapa.get("fechaFactura")));
                facturaVO.setContenedor(String.valueOf(mapa.get("contenedor")));
                facturaVO.setNoFactura(String.valueOf(mapa.get("noFactura")));
                facturaVO.setEtd(FechaUtil.formateaFecha((Date) mapa.get("etd")));
                facturaVO.setEta(FechaUtil.formateaFecha((Date) mapa.get("eta")));
                facturas.add(facturaVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return facturas;
    }

    public FacturaVO obtenTotalesMisFacturas(FacturaVO factura) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery("select count(idFacturaProducto) productos, sum(piezas) piezas, sum(precioTotal) total from historicoFacturaProducto where idFactura = " + factura.getIdFactura());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {

                factura.setProductos(NumberFormat.getInstance().format(mapa.get("productos")));
                factura.setPiezas(NumberFormat.getInstance().format(mapa.get("piezas")));
                factura.setTotal("¥ " + NumberFormat.getInstance().format(mapa.get("total")));
                break;
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return factura;
    }

    public List<FacturaDetalleVO> obtenDetalleMisFacturas(FacturaVO facturaVO) {
        Session session = null;
        List<FacturaDetalleVO> detalles = new ArrayList<>(0);
        StringBuilder sb = new StringBuilder();
        sb.append("select cp.sku, cp.descripcion, p.piezas, p.precioUnitario, p.precioTotal ");
        sb.append("from  historicoFacturaProducto p LEFT join productoCompra cp on p.idProductoCompra = cp.idProductoCompra  ");
        sb.append("where p.idFactura = ");
        sb.append(facturaVO.getIdFactura());
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                FacturaDetalleVO detalle = new FacturaDetalleVO();
                detalle.setSku(String.valueOf(mapa.get("sku")));
                detalle.setDescripcion(String.valueOf(mapa.get("descripcion")));
                detalle.setPiezas(NumberFormat.getInstance().format(mapa.get("piezas")));
                detalle.setPrecioUnitario("¥ " + NumberFormat.getInstance().format(mapa.get("precioUnitario")));
                detalle.setPrecioTotal("¥ " + NumberFormat.getInstance().format(mapa.get("precioTotal")));

                detalles.add(detalle);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return detalles;
    }

    public HistoricoFactura buscarXId(String id) {

        Session session = null;
        if (id == null) {
            LOG.error("Id de histórico es nulo");
            return null;
        }
        long idFactura = Long.parseLong(id);
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from historicoFactura f where f.idFactura = :idFactura");
            query.setParameter("idFactura", idFactura);
            return (HistoricoFactura) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

    public void elimina(HistoricoFactura historicoFactura) {
        delete(sessionFactory, historicoFactura);
    }

}
