package mx.com.miniso.control.dao.colombia;

import mx.com.miniso.control.dao.*;
import java.util.List;
import mx.com.miniso.control.dto.Sociedad;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class SociedadColombiaDao extends BaseDao<Sociedad> {

    private final static Log LOG = LogFactory.getLog(SociedadColombiaDao.class);

    @Autowired
    @Qualifier("sessionFactoryColombia")
    private SessionFactory sessionFactory;

    public Sociedad obtenerXID(Sociedad sociedad) {
        Session session = null;
        if (sociedad == null) {
            LOG.error("No se puede buscar el sociedad por que es nulo");
            return null;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from sociedad s where s.idSociedad = :idSociedad");
            query.setParameter("idSociedad", sociedad.getIdSociedad());
            sociedad = (Sociedad) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return sociedad;
    }

    public List<Sociedad> buscarTodos() {
        Session session = null;
        List<Sociedad> listSociedad = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from sociedad s order by s.descripcion");
            listSociedad = query.list();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return listSociedad;
    }

}
