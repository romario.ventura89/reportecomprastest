package mx.com.miniso.control.dao.colombia;

import mx.com.miniso.control.dao.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mx.com.miniso.control.dto.TipoOrden;
import mx.com.miniso.control.util.FechaUtil;
import mx.com.miniso.control.vo.ConfirmacionesVO;
import mx.com.miniso.control.vo.ReporteConfirmacionVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class ReportesColombiaDao extends BaseDao<TipoOrden> {

    private final static Log LOG = LogFactory.getLog(ReportesColombiaDao.class);

    @Autowired
    @Qualifier("sessionFactoryColombia")
    private SessionFactory sessionFactory;

    public List<ReporteConfirmacionVO> buscarTodos() {
        Session session = null;
        List<ReporteConfirmacionVO> reporteConfirmacionList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT r.sku,r.descripcion, IsNull(sum(r.piezasEntregadas),0) confirmacion, IsNull(sum(r.facturas),0)fact ");
            sb.append("from (select cp.sku, cp.descripcion, conf.piezasEntregadas, ");
            sb.append("(select sum(piezas) from facturaproducto f where f.idProductoCompra = cp.idProductoCompra group by f.idProductoCompra) as facturas ");
            sb.append("from productoCompra cp ");
            sb.append("LEFT join confirmacionProducto conf on cp.idProductoCompra = conf.idProductoCompra) r ");
            sb.append("group by(r.sku),r.descripcion ");

            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                ReporteConfirmacionVO reporteConfirmacionVO = new ReporteConfirmacionVO();
                reporteConfirmacionVO.setSku(String.valueOf(mapa.get("sku")));
                reporteConfirmacionVO.setProductName(String.valueOf(mapa.get("descripcion")));
                reporteConfirmacionVO.setConfirmacion(String.valueOf(mapa.get("confirmacion")));
                reporteConfirmacionVO.setFacturas(String.valueOf(mapa.get("facturas")));
                reporteConfirmacionList.add(reporteConfirmacionVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return reporteConfirmacionList;
    }
}
