package mx.com.miniso.control.dao.colombia;

import mx.com.miniso.control.dao.*;
import mx.com.miniso.control.dto.ProductoCompra;
import mx.com.miniso.control.dto.Usuario;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class ProductoCompraColombiaDao extends BaseDao<ProductoCompra> {

    private final static Log LOG = LogFactory.getLog(ProductoCompraColombiaDao.class);

    @Autowired
    @Qualifier("sessionFactoryColombia")
    private SessionFactory sessionFactory;

    public ProductoCompra obtenerProductoXSKU(ProductoCompra productoCompra) throws Exception {

        Session session = null;
        if (productoCompra == null) {
            LOG.error("No se puede buscar el usuario por que es nulo");
            return productoCompra;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from productoCompra p where p.sku = :sku");
            query.setParameter("sku", productoCompra.getSku());
            productoCompra = (ProductoCompra) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return productoCompra;

    }

    public ProductoCompra guardar(ProductoCompra productoCompra) throws Exception {
        LOG.info("Guardando desperfecto: " + productoCompra);
        return guardar(sessionFactory, productoCompra);
    }

    public Usuario obtenerUsuarioXuserName(Usuario usuario) throws Exception {

        Session session = null;
        if (usuario == null) {
            LOG.error("No se puede buscar el usuario por que es nulo");
            return usuario;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from usuario u where u.userName = :userName");
            query.setParameter("userName", usuario.getUserName());
            usuario = (Usuario) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return usuario;

    }

    public ProductoCompra buscarXID(ProductoCompra productoCompra) {
        Session session = null;
        if (productoCompra == null) {
            LOG.error("No se puede buscar el skupor que es nulo");
            return null;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from productoCompra p where p.idProductoCompra = :idProductoCompra");
            query.setParameter("idProductoCompra", productoCompra.getIdProductoCompra());
            productoCompra = (ProductoCompra) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return productoCompra;
    }

    public ProductoCompra buscarXSKU(ProductoCompra productoCompra) {
        Session session = null;
        if (productoCompra == null) {
            LOG.error("No se puede buscar el skupor que es nulo");
            return null;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from productoCompra p where p.sku = :sku");
            query.setParameter("sku", productoCompra.getSku());
            productoCompra = (ProductoCompra) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return productoCompra;
    }

}
