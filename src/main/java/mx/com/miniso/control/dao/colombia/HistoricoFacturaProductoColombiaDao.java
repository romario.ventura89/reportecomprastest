package mx.com.miniso.control.dao.colombia;

import mx.com.miniso.control.dao.*;
import java.util.List;
import mx.com.miniso.control.dto.HistoricoFactura;
import mx.com.miniso.control.dto.HistoricoFacturaProducto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class HistoricoFacturaProductoColombiaDao extends BaseDao<HistoricoFacturaProducto> {

    private final static Log LOG = LogFactory.getLog(HistoricoFacturaProductoColombiaDao.class);

    @Autowired
    @Qualifier("sessionFactoryColombia")
    private SessionFactory sessionFactory;

    public HistoricoFacturaProducto guardar(HistoricoFacturaProducto historicoFactura) {
        LOG.info("Guardando el historico " + historicoFactura);
        guardar(sessionFactory, historicoFactura);
        return historicoFactura;
    }

    public List<HistoricoFacturaProducto> buscarXIdFactura(long idFactura) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from historicoFacturaProducto f where f.idFactura = :idFactura");
            query.setParameter("idFactura", idFactura);
            return query.list();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void elimina(HistoricoFacturaProducto historicoProducto) {
        delete(sessionFactory, historicoProducto);
    }

}
