package mx.com.miniso.control.dao;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Map;

import java.util.List;
import mx.com.miniso.control.dto.Confirmacion;
import mx.com.miniso.control.vo.ContainerVO;
import mx.com.miniso.control.vo.ContenedorVO;
import mx.com.miniso.control.vo.FaltanteOrderVO;
import mx.com.miniso.control.vo.OrderVO;
import mx.com.miniso.control.vo.PedidoOrdenVO;
import mx.com.miniso.control.vo.PedidosVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class ReportePedidosDao extends BaseDao<Confirmacion> {

    private final static Log LOG = LogFactory.getLog(ReportePedidosDao.class);

    @Autowired
    @Qualifier(value = "sessionFactory")
    private SessionFactory sessionFactory;

    public List<PedidosVO> obtenerTotales() {
        List<PedidosVO> reporte = new ArrayList<>(0);
        Session session = null;
        StringBuilder sb = new StringBuilder();

        sb.append("select conf.sku, fact.descripcion, conf.confirmacion, fact.facturas, ");
        sb.append("(ISNULL(sum(fact.facturas),0) - ISNULL(sum(conf.confirmacion),0)) notSend ");
        sb.append("from (select DISTINCT(cp.sku), ");
        sb.append("ISNULL(sum(conf.piezasEntregadas),0) confirmacion ");
        sb.append("from confirmacionProducto conf ");

        sb.append("right join productoCompra cp  on cp.idProductoCompra = conf.idProductoCompra ");
//        sb.append("inner join facturaProducto fact on cp.idProductoCompra = fact.idProductoCompra ");
        sb.append("group by (cp.sku)) conf, (select cp.sku, cp.descripcion, ");
        sb.append("ISNULL(sum(fact.piezas),0) facturas ");
        sb.append("from productoCompra cp   ");
        sb.append("LEFT join facturaProducto fact on cp.idProductoCompra = fact.idProductoCompra  ");

        sb.append("group by cp.sku, cp.descripcion) fact  where conf.sku = fact.sku group by conf.sku, fact.descripcion, conf.confirmacion,fact.facturas order by conf.sku ");

        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                PedidosVO pedidoVO = new PedidosVO();
                pedidoVO.setSku(String.valueOf(mapa.get("sku")));
                pedidoVO.setProductName(String.valueOf(mapa.get("descripcion")));
                pedidoVO.setPiezasConfirmadas(NumberFormat.getInstance().format(mapa.get("confirmacion")));
                pedidoVO.setTotalFacturas(NumberFormat.getInstance().format(mapa.get("facturas")));
                pedidoVO.setNoEnviados(NumberFormat.getInstance().format(mapa.get("notSend")));
                reporte.add(pedidoVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return reporte;
    }

    public List<String> obtnerNumeroOrdenes(String sku) {
        List<String> nombres = new ArrayList<>(0);
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("select c.noOrden ");
        sb.append("from confirmacion c, confirmacionProducto p LEFT join productoCompra cp on p.idProductoCompra = cp.idProductoCompra ");
        sb.append("where c.idConfirmacion = p.idConfirmacion and cp.sku = '");
        sb.append(sku);
        sb.append("'");
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            nombres = query.list();

        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return nombres;
    }

    public List<ContainerVO> obtenerContenedores() {
        List<ContainerVO> contenedores = new ArrayList<>(0);
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("select idFactura, contenedor from factura order by contenedor");
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                ContainerVO contenedor = new ContainerVO();
                contenedor.setIdFactura(String.valueOf(mapa.get("idFactura")));
                contenedor.setNombreContenedor(String.valueOf(mapa.get("contenedor")));
                contenedores.add(contenedor);
            }

        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return contenedores;
    }

    public List<ContenedorVO> obtenPiezas(String idFactura) {
        List<ContenedorVO> contenedores = new ArrayList<>(0);
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("select p.idFactura, p.piezas,  cp.sku ");
        sb.append("from  facturaProducto p LEFT join productoCompra cp on p.idProductoCompra = cp.idProductoCompra ");
        sb.append("where p.idFactura = ");
        sb.append(idFactura);
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                ContenedorVO contenedor = new ContenedorVO();
                contenedor.setIdFactura(String.valueOf(mapa.get("idFactura")));
                contenedor.setPiezas(NumberFormat.getInstance().format(mapa.get("piezas")));
                contenedor.setSku(String.valueOf(mapa.get("sku")));
                contenedores.add(contenedor);
            }

        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return contenedores;
    }

    public List<OrderVO> obtenerOrdenes() {
        List<OrderVO> ordenes = new ArrayList<>(0);
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("select idConfirmacion, noOrden from confirmacion order by noOrden");
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                OrderVO ordenVO = new OrderVO();
                ordenVO.setIdComfirmacion(String.valueOf(mapa.get("idConfirmacion")));
                ordenVO.setNoOrden(String.valueOf(mapa.get("noOrden")));
                ordenes.add(ordenVO);
            }

        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return ordenes;
    }

    public List<PedidoOrdenVO> obtenPiezasOrden(String idConfirmacion) {
        List<PedidoOrdenVO> list = new ArrayList<>(0);
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("select p.idConfirmacion, p.piezasEntregadas,  cp.sku  ");
        sb.append("from  ConfirmacionProducto p LEFT join productoCompra cp on p.idProductoCompra = cp.idProductoCompra  ");
        sb.append("where p.idConfirmacion = ");
        sb.append(idConfirmacion);
        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                PedidoOrdenVO pedidoOrden = new PedidoOrdenVO();
                pedidoOrden.setIdConfirmacion(String.valueOf(mapa.get("idConfirmacion")));
                pedidoOrden.setPiezas(NumberFormat.getInstance().format(mapa.get("piezasEntregadas")));
                pedidoOrden.setSku(String.valueOf(mapa.get("sku")));
                list.add(pedidoOrden);
            }

        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return list;
    }

    public List<FaltanteOrderVO> obtenerFaltantes() {
        List<FaltanteOrderVO> reporte = new ArrayList<>(0);
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append(" select cp.sku,c.idConfirmacion, c.noOrden, c.fechaConfirmacion, ISNULL(sum(conf.piezasEntregadas),0) confirmaciones from ");
        sb.append(" confirmacion c  LEFT JOIN");
        sb.append(" confirmacionProducto conf on c.idConfirmacion = conf.idConfirmacion LEFT JOIN");
        sb.append(" productoCompra cp on cp.idProductoCompra = conf.idProductoCompra");
        sb.append(" GROUP by cp.sku, c.idConfirmacion ,c.noOrden, c.fechaConfirmacion  order by cp.sku,c.fechaConfirmacion");

        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                FaltanteOrderVO faltanteVO = new FaltanteOrderVO();
                faltanteVO.setSku(String.valueOf(mapa.get("sku")));
                faltanteVO.setNoOrden(String.valueOf(mapa.get("noOrden")));
                faltanteVO.setConfirmaciones(Integer.parseInt(String.valueOf((mapa.get("confirmaciones")))));
                reporte.add(faltanteVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return reporte;
    }

}
