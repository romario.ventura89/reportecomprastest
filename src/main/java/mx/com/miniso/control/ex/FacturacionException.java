package mx.com.miniso.control.ex;

public class FacturacionException extends Exception {

    public FacturacionException(String mensaje) {
        super(mensaje);
    }

}
