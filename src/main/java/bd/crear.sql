CREATE TABLE tipoOrden (idTipoOrden int NOT NULL, descripcion varchar(30) NOT NULL, PRIMARY KEY (idTipoOrden));
CREATE TABLE sociedad (idSociedad int NOT NULL, descripcion varchar(30) NOT NULL, PRIMARY KEY (idSociedad));
CREATE TABLE navegacion (idNavegacion int IDENTITY NOT NULL, path varchar(100) NOT NULL, fechaCreacion datetime NOT NULL, objeto varbinary(2500) NULL, estatus bit NOT NULL, PRIMARY KEY (idNavegacion));
CREATE TABLE sesion (idSesion int IDENTITY NOT NULL, idUsuario int NOT NULL UNIQUE, idNavegacion int NOT NULL, PRIMARY KEY (idSesion));
CREATE TABLE facturaProducto (idFacturaProducto int IDENTITY NOT NULL, piezas int NOT NULL, precioUnitario numeric(10, 2) NOT NULL, precioTotal numeric(10, 2) NOT NULL, idFactura int NOT NULL, idProductoCompra int NOT NULL, PRIMARY KEY (idFacturaProducto));
CREATE TABLE factura (idFactura int IDENTITY NOT NULL, fechaRegistro datetime NOT NULL, estatus char(1) NOT NULL, contenedor varchar(20) NOT NULL, fechaFactura date NOT NULL, noFactura varchar(15) NOT NULL UNIQUE, etd date NOT NULL, eta date NOT NULL, idConfirmacion int NOT NULL, PRIMARY KEY (idFactura));
CREATE TABLE productoCompra (idProductoCompra int IDENTITY NOT NULL, fechaRegistro datetime NOT NULL, sku varchar(30) NOT NULL, codigoBarras varchar(30) NULL, descripcion varchar(255) NULL, descripcionIngles varchar(255) NULL, familia varchar(50) NULL, subFamilia varchar(50) NULL, subSubFamilia varchar(50) NULL, estatus bit NOT NULL, PRIMARY KEY (idProductoCompra));
CREATE TABLE confirmacionProducto (idConfirmacionProducto int IDENTITY NOT NULL, piezasEntregadas int NOT NULL, piezasSolicitadas int NOT NULL, precioUnitario numeric(10, 2) NOT NULL, precioTotal numeric(10, 2) NOT NULL, notOrder bit NOT NULL, idConfirmacion int NOT NULL, idProductoCompra int NOT NULL, idTipoOrden int NOT NULL, PRIMARY KEY (idConfirmacionProducto));
EXEC sp_addextendedproperty @NAME = N'MS_Description', @VALUE = 'tabla que contiene el numero de piezas que tiene cada producto en un pedido', @LEVEL0TYPE = N'Schema', @LEVEL0NAME = 'dbo', @LEVEL1TYPE = N'Table', @LEVEL1NAME = 'confirmacionProducto';
EXEC sp_addextendedproperty @NAME = N'MS_Description', @VALUE = 'Identificador del pedido compra', @LEVEL0TYPE = N'Schema', @LEVEL0NAME = 'dbo', @LEVEL1TYPE = N'Table', @LEVEL1NAME = 'confirmacionProducto', @LEVEL2TYPE = N'Column', @LEVEL2NAME = 'idConfirmacionProducto';
CREATE TABLE confirmacion (idConfirmacion int IDENTITY NOT NULL, fechaRegistro datetime NOT NULL, estatus char(1) NOT NULL, noOrden varchar(15) NOT NULL UNIQUE, fechaConfirmacion date NOT NULL, idSociedad int NOT NULL, PRIMARY KEY (idConfirmacion));
EXEC sp_addextendedproperty @NAME = N'MS_Description', @VALUE = 'Contiene los pedidos realizados', @LEVEL0TYPE = N'Schema', @LEVEL0NAME = 'dbo', @LEVEL1TYPE = N'Table', @LEVEL1NAME = 'confirmacion';
EXEC sp_addextendedproperty @NAME = N'MS_Description', @VALUE = 'Identificador de la tabla', @LEVEL0TYPE = N'Schema', @LEVEL0NAME = 'dbo', @LEVEL1TYPE = N'Table', @LEVEL1NAME = 'confirmacion', @LEVEL2TYPE = N'Column', @LEVEL2NAME = 'idConfirmacion';
EXEC sp_addextendedproperty @NAME = N'MS_Description', @VALUE = 'Fecha en que se crea el registro', @LEVEL0TYPE = N'Schema', @LEVEL0NAME = 'dbo', @LEVEL1TYPE = N'Table', @LEVEL1NAME = 'confirmacion', @LEVEL2TYPE = N'Column', @LEVEL2NAME = 'fechaRegistro';
EXEC sp_addextendedproperty @NAME = N'MS_Description', @VALUE = 'estatus en que se encuentra el pedido', @LEVEL0TYPE = N'Schema', @LEVEL0NAME = 'dbo', @LEVEL1TYPE = N'Table', @LEVEL1NAME = 'confirmacion', @LEVEL2TYPE = N'Column', @LEVEL2NAME = 'estatus';
EXEC sp_addextendedproperty @NAME = N'MS_Description', @VALUE = 'Fecha en que enviará el pedido', @LEVEL0TYPE = N'Schema', @LEVEL0NAME = 'dbo', @LEVEL1TYPE = N'Table', @LEVEL1NAME = 'confirmacion', @LEVEL2TYPE = N'Column', @LEVEL2NAME = 'noOrden';
CREATE TABLE rol (idRol int NOT NULL, descripcion varchar(60) NOT NULL, PRIMARY KEY (idRol));
CREATE TABLE usuario (idUsuario int IDENTITY NOT NULL, userName varchar(60) NOT NULL, password varchar(32) NOT NULL, estatus bit NOT NULL, fechaRegistro datetime NOT NULL, email varchar(100) NULL, nombre varchar(30) NULL, apellidos varchar(100) NULL, titulo varchar(10) NULL, idRol int NOT NULL, PRIMARY KEY (idUsuario));
ALTER TABLE sesion ADD CONSTRAINT FKsesion106662 FOREIGN KEY (idNavegacion) REFERENCES navegacion (idNavegacion);
ALTER TABLE confirmacion ADD CONSTRAINT FKconfirmaci552917 FOREIGN KEY (idSociedad) REFERENCES sociedad (idSociedad);
ALTER TABLE confirmacionProducto ADD CONSTRAINT FKconfirmaci634976 FOREIGN KEY (idTipoOrden) REFERENCES tipoOrden (idTipoOrden);
ALTER TABLE confirmacionProducto ADD CONSTRAINT FKconfirmaci563753 FOREIGN KEY (idProductoCompra) REFERENCES productoCompra (idProductoCompra);
ALTER TABLE confirmacionProducto ADD CONSTRAINT FKconfirmaci575389 FOREIGN KEY (idConfirmacion) REFERENCES confirmacion (idConfirmacion);
ALTER TABLE facturaProducto ADD CONSTRAINT FKfacturaPro351579 FOREIGN KEY (idProductoCompra) REFERENCES productoCompra (idProductoCompra);
ALTER TABLE facturaProducto ADD CONSTRAINT FKfacturaPro598065 FOREIGN KEY (idFactura) REFERENCES factura (idFactura);
ALTER TABLE factura ADD CONSTRAINT FKfactura416945 FOREIGN KEY (idConfirmacion) REFERENCES confirmacion (idConfirmacion);
ALTER TABLE usuario ADD CONSTRAINT FKusuario330728 FOREIGN KEY (idRol) REFERENCES rol (idRol);
CREATE UNIQUE INDEX productoCompra_sku ON productoCompra (sku);
