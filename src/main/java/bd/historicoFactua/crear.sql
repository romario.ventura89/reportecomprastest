CREATE TABLE HistoricoFacturaProducto (idFacturaProducto int IDENTITY NOT NULL, piezas int NOT NULL, precioUnitario numeric(10, 2) NOT NULL, precioTotal numeric(10, 2) NOT NULL, idFactura int NOT NULL, idProductoCompra int NOT NULL, PRIMARY KEY (idFacturaProducto));
CREATE TABLE HistoricoFactura (idFactura int IDENTITY NOT NULL, fechaRegistro datetime NOT NULL, estatus char(1) NOT NULL, contenedor varchar(20) NOT NULL, fechaFactura date NOT NULL, noFactura varchar(15) NOT NULL, etd date NOT NULL, eta date NOT NULL, PRIMARY KEY (idFactura));
ALTER TABLE HistoricoFacturaProducto ADD CONSTRAINT FKHistoricoF409432 FOREIGN KEY (idFactura) REFERENCES HistoricoFactura (idFactura);

