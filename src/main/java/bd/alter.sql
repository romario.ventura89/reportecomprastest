--ALTER TABLE [dbo].[confirmacionProducto] DROP CONSTRAINT [FKconfirmaci634976]

ALTER TABLE [dbo].[confirmacionProducto] ALTER COLUMN [idTipoOrden] varchar(80);

EXEC sp_rename N'[dbo].[confirmacionProducto].[idTipoOrden]', N'tipoOrden', 'COLUMN'
GO
