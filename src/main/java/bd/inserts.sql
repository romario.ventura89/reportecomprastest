INSERT INTO sociedad
(idSociedad, descripcion)
VALUES(1, 'MÉXICO - CHINA');

INSERT INTO sociedad
(idSociedad, descripcion)
VALUES(2, 'COLOMBIA - CHINA');

INSERT INTO tipoOrden
(idTipoOrden, descripcion)
VALUES(1, 'GENERAL');

INSERT INTO tipoOrden
(idTipoOrden, descripcion)
VALUES(3, 'PINGSHAN');

INSERT INTO tipoOrden
(idTipoOrden, descripcion)
VALUES(2, 'QIANHAI');

INSERT INTO rol (idRol,descripcion) VALUES (
1,'ADMINISTRADOR');

INSERT INTO navegacion ("path",fechaCreacion,objeto,estatus) VALUES (
'redirect:/sitio/privado/actualizacion/carga_excel.jsp','2018-10-18',NULL,1);

INSERT INTO usuario (userName,password,estatus,fechaRegistro,email,nombre,apellidos,titulo,idRol) VALUES (
'admin','123',1,'2018-04-09','sudhunter@gmail.com','Arturo','De León Campos','Ing.',1);

INSERT INTO sesion (idUsuario,idNavegacion) VALUES (
1,1);